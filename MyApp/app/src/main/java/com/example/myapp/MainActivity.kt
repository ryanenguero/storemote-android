package com.example.myapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.GridView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val gridView = findViewById<GridView>(R.id.gridView) as GridView

        /*Create and ArrayList of Integer Type To Store Images From drawable.Here we add Images to ArrayList.
        We have Images of Android Icons of Different versions.
        */

        val arrayListImage = ArrayList<Int>()

        arrayListImage.add(R.drawable.cupcake)



        // Here We take and Array of Android OS names in Same Sequence as we take Images.

        val name = arrayOf("Cupcake")


        //We Have Created Custom Adapter Class in that we pass Context,ArrayList of Image and Array Of name

        val customAdapter = CustomAdapter(this@MainActivity, arrayListImage, name)

        //Set Adapter to ArrayList

        gridView.adapter = customAdapter

        //On Click for GridView Item

        gridView.setOnItemClickListener { adapterView, parent, position, l ->
            Toast.makeText(this@MainActivity, "Click on : " + name[position], Toast.LENGTH_LONG).show()
        }

    }


}

