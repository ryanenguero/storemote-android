package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class CategoryDB extends RealmObject {

    private String category_id;
    private String name;


    public void setCategoryId(String categoryId) {

        this.category_id = categoryId;
    }

    public String getCategoryId() {

        return category_id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }
}
