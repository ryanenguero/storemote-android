package com.storemote.teafarmscafe

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import com.storemote.teafarmscafe.Model_JSON.PDFReceipt
import com.storemote.teafarmscafe.Model_JSON.PurchasedHistory
import com.storemote.teafarmscafe.Model_RealmDatabase.PurchasedHistoryDB
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap
import android.webkit.WebChromeClient



class PurchaseDetailsView : AppCompatActivity() {

    private lateinit var apiInterface: APIInterface
    private lateinit var realm: Realm

    private lateinit var pdf: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchased_history_details)


        supportActionBar?.title = "Purchased History"


        apiInterface = APIClientPDF.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()
       map["client_id"] = intent.getStringExtra("pdfClientId")
        map["username"] = intent.getStringExtra("pdfUsername")
        map["sale_id"] = intent.getStringExtra("pdfURL")


        val call3 = apiInterface.doDownloadPDFReceipt(map)
         call3.enqueue(
                object : Callback<PDFReceipt> {



                    override fun onResponse(call: Call<PDFReceipt>, response: Response<PDFReceipt>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val pdfData = response.body()

                        val url = pdfData!!.data

                        if (url != null) {

                            val googleDocs = "https://docs.google.com/viewer?url="
                            val ur = "http://storemote.net/pdfs/${url["pdf"].toString()}"

                            val mywebview = findViewById<WebView>(R.id.receipt_view) as WebView
                            mywebview!!.webViewClient = object : WebViewClient() {
                                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                                    view?.loadUrl(url)
                                    return true
                                }
                            }

                            mywebview.settings.javaScriptEnabled = true
                            mywebview.settings.allowFileAccessFromFileURLs = true
                            mywebview.settings.allowUniversalAccessFromFileURLs = true
                            mywebview.settings.builtInZoomControls = true
                            mywebview.webChromeClient = WebChromeClient()
//            mywebview.setWebChromeClient(WebChromeClient())

                            mywebview!!.loadUrl(googleDocs + ur)

//
//
//                            pdf = ur
//
//                            NotificationCenter.defaultCenter().postNotification("loadPDF")
                        }
                    }

                    override fun onFailure(call: Call<PDFReceipt>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })

//        NotificationCenter.defaultCenter().addFucntionForNotification("loadPDF") {
//
//            val mywebview = findViewById<WebView>(R.id.receipt_view) as WebView
//            mywebview!!.webViewClient = object : WebViewClient() {
//                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                    view?.loadUrl(url)
//                    return true
//                }
//            }
//
//            mywebview.settings.javaScriptEnabled = true
//            mywebview.settings.allowFileAccessFromFileURLs = true
//            mywebview.settings.allowUniversalAccessFromFileURLs = true
//            mywebview.settings.builtInZoomControls = true
//            mywebview.webChromeClient = WebChromeClient()
////            mywebview.setWebChromeClient(WebChromeClient())
//
//            mywebview!!.loadUrl(pdf)
////                            Log.d("MainActivity", "purchased =  ${data[da]!!.amount_due}")
//
//        }


    }



}