package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Promos {

    @SerializedName("data")
    public List<PromosDatum> data = new ArrayList<>();

    public class PromosDatum {

        @SerializedName("discount_percent")
        public Float discount_percent;

        @SerializedName("from_date")
        public Date from_date;
//
//        @SerializedName("item_free_id")
//        public Double item_free_id;
//
        @SerializedName("item_id")
        public Double item_id;

        @SerializedName("name")
        public String name;

        @SerializedName("no_of_free_items")
        public Double no_of_free_items;

        @SerializedName("promo_id")
        public Double promo_id;

        @SerializedName("promo_type_id")
        public Double promo_type_id;

        @SerializedName("specific_price")
        public Float specific_price;

        @SerializedName("to_date")
        public Date to_date;

    }
}
