package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class FreeItemsDB extends RealmObject {

    private Integer category_id;
    private Integer promo_id;

    public void setCategoryId(Integer categoryId) {

        this.category_id = categoryId;
    }

    public Integer getCategoryId() {

        return category_id;
    }

    public void setPromoId(Integer promoId) {

        this.promo_id = promoId;
    }

    public Integer getPromoId() {

        return promo_id;
    }

}
