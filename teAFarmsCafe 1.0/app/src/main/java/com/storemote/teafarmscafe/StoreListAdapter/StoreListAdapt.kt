package com.storemote.teafarmscafe.StoreListAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.R

class StoreListAdapt(private val context: Context,
                    private val dataSource: ArrayList<StoreListDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.list_store, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.store_list_name) as TextView

// Get subtitle element
        val addressTextView = rowView.findViewById(R.id.store_list_address) as TextView

// Get detail element
        val hoursTextView = rowView.findViewById(R.id.store_list_hours) as TextView

// Get thumbnail element
        val thumbnailImageView = rowView.findViewById(R.id.landing_page_image) as ImageView

        val stores = getItem(position) as StoreListDB

        nameTextView.text = stores.name
        addressTextView.text = stores.storeAddress
        hoursTextView.text = "8:00 am - 10:00 pm on Mon-Sat and 10:00 am - 7:00pm on Sundays"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${stores.storeImage}").into(thumbnailImageView)

        return rowView
    }
}

