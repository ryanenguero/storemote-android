package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FreeItems {

    @SerializedName("data")
    public List<FreeItemsData> data = new ArrayList<>();

    public class FreeItemsData {

        @SerializedName("item_id")
        public Integer category_id;
        @SerializedName("promo_id")
        public Integer promo_id;

    }
}
