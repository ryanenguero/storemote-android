package com.storemote.teafarmscafe



import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.GridView
import android.widget.TextView
import android.widget.Toolbar
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardDB
import io.realm.Realm



class StampView : AppCompatActivity() {


    private lateinit var stampGridView: GridView
    private lateinit var realm: Realm

    private var mTopToolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.stamp_view)

        supportActionBar?.title = "Rewards Card"

        stampGridView = findViewById<GridView>(R.id.gridView) as GridView


        Realm.init(this)

        realm = Realm.getDefaultInstance()


        val preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

        DownloaderClass().doDownloadRewardsCardItems(preferences.getString("storeId", "0").toString() ,
                "6ce2362a6d22d11379067f6bb5e6b1deb69b55098df4b0eca86d041f31dbf2da46e5ae4e418d50c52802ef779ab35e9177ee61b106fa3f93fdab45a4bebf7c7e5mIE4H13Lb3xL4PtFdnr4io8Xu7tLS+dmaStgVpuhc9lO+hxc8AdU3SWzz2kIL9s",
                realm)



        val arrList = ArrayList<RewardsCardDB>(realm.where(RewardsCardDB::class.java).findAll())

        if (arrList[0].pointsToCollect != null) {

            val pointsToCollect = arrList[0].pointsToCollect

            val arr = ArrayList<Int>()

            for (da in 0 until pointsToCollect) {

                arr.add(1)
            }

            Log.d("MainActivity", "ResponseStamps=  $arrList")
            val adapt = GridAdapter(this, arr, arrList[0].cardPoints)
            stampGridView.adapter = adapt

        }




    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        val arrList = ArrayList<RewardsCardDB>(realm.where(RewardsCardDB::class.java).findAll())


        if (arrList[0].cardPoints >= arrList[0].pointsToCollect) {
            menuInflater.inflate(R.menu.claim_rewards, menu)

            val menuItem = menu!!.findItem(R.id.rwrd_btn)

            val actionView = MenuItemCompat.getActionView(menuItem)

            actionView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    onOptionsItemSelected(menuItem)
                }
            })
        }

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {



        when (item?.getItemId()) {

            R.id.rwrd_btn -> {
                // Do something

                val intentt = Intent(this, ClaimRewards::class.java)

                startActivity(intentt)

                return true
            }
        }


        return super.onOptionsItemSelected(item)
    }



}



//
//        } else {
//
//            val builder = AlertDialog.Builder(this)
//
//            builder.setTitle("Stamp")
//            builder.setMessage("You do not have any stamps available right now")
//
//            builder.setPositiveButton("Ok") { dialog, which ->
//
//
//            }
//
//            val dialog: AlertDialog = builder.create()
//            dialog.show()
//
//