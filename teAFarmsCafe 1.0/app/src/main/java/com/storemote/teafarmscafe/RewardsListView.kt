package com.storemote.teafarmscafe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import com.storemote.teafarmscafe.Model_RealmDatabase.*
import com.storemote.teafarmscafe.WeeklySpecialsAdapter.WeeklySpecialsViewAdapter
import io.realm.Realm

class RewardsListView : AppCompatActivity() {


    private lateinit var listview: ListView
    private var arrList = ArrayList<Any>()
    private lateinit var realm: Realm
    private lateinit var adapt: RewardsLisAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rewards_details_list)

//        supportActionBar?.title = intent.getStringExtra("titleProms")

        supportActionBar?.title = intent.getStringExtra("titleRewards")

        listview = findViewById<ListView>(R.id.rewards_details_list_view)

        Realm.init(this)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()
        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
//        arrList = realm.where(Rewards::class.java).findAllAsync()




        val arrList = ArrayList<RewardsDetailsDB>(realm.where(RewardsDetailsDB::class.java).findAll())
        val menu = ArrayList<MenuProductExceptionDB>(realm.where(MenuProductExceptionDB::class.java).findAll())
        //ArrayList<WeeklySpecials>(realm.where(WeeklySpecials::class.java).findAll())

//        val arr = ArrayList<WeeklySpecials>(arrList)
        val adapt = RewardsLisAdapter(this, arrList)
        listview?.adapter = adapt


        listview?.setOnItemClickListener { _, _, position, _ ->


            val intentt = Intent(this, ProductDetailsView::class.java)
//
            for (da in 0 until menu.size) {
                if (menu[da].menuProd.itemId.toString() == arrList[position].itemId.toString()){

                    val selectedRewards = arrList[position]

                    intentt.putExtra("is_promo", "0")
                    intentt.putExtra("is_rewards", "1")
                    intentt.putExtra("name", menu[da].menuProd.name)
                    intentt.putExtra("description", selectedRewards.discountPercent.toString() + "% off \n\n" + menu[da].menuProd.description)
                    intentt.putExtra("image", menu[da].menuProd.picFilename)
                    intentt.putExtra("category", menu[da].menuProd.category)
                    intentt.putExtra("clientId", menu[da].menuProd.client_id)
                    intentt.putExtra("costPrice", menu[da].menuProd.costPrice)
                    intentt.putExtra("createdBy", menu[da].menuProd.createdBy)
                    intentt.putExtra("createdDate", menu[da].menuProd.createdDate)
                    intentt.putExtra("itemId", menu[da].menuProd.itemId)
                    intentt.putExtra("itemNumber", menu[da].menuProd.itemNumber)
                    intentt.putExtra("itemType", menu[da].menuProd.itemType)
                    intentt.putExtra("receivingQuantity", menu[da].menuProd.receivingQuantity)
                    intentt.putExtra("reorderLevel", menu[da].menuProd.reorderLevel)
                    intentt.putExtra("stockType", menu[da].menuProd.stockType)
                    intentt.putExtra("storeId", menu[da].menuProd.storeId)
                    intentt.putExtra("supplierId", menu[da].menuProd.supplierId)
                    intentt.putExtra("taxCategoryId", menu[da].menuProd.taxCategoryId)
                    intentt.putExtra("updatedBy", menu[da].menuProd.updatedBy)
                    intentt.putExtra("updatedDate", menu[da].menuProd.updatedDate)
                    intentt.putExtra("versionNo", menu[da].menuProd.versionNo)



                    if (selectedRewards.discountPercent != 0.0f && selectedRewards.nooffreeitems == 0.toDouble() && selectedRewards.specificPrice == 0.0f){

                        val price = (menu[da].menuProd.unitPrice.toDouble()-(menu[da].menuProd.unitPrice.toDouble()*selectedRewards.discountPercent/100)).toString()

                        intentt.putExtra("price", price)


                    }else if (selectedRewards.discountPercent == 0.0f && selectedRewards.nooffreeitems != 0.toDouble() && selectedRewards.specificPrice == 0.0f){

                        intentt.putExtra("price", selectedRewards.specificPrice.toString())

                    }else if (selectedRewards.discountPercent == 0.0f && selectedRewards.nooffreeitems == 0.toDouble() && selectedRewards.specificPrice != 0.0f){

                        intentt.putExtra("price", selectedRewards.specificPrice.toString())

                    }else if (selectedRewards.discountPercent != 0.0f && selectedRewards.nooffreeitems != 0.toDouble() && selectedRewards.specificPrice == 0.0f){

                        val price = (menu[da].menuProd.unitPrice.toDouble()-(menu[da].menuProd.unitPrice.toDouble()*selectedRewards.discountPercent/100)).toString()

                        intentt.putExtra("price", price)

                    }else if (selectedRewards.discountPercent == 0.0f && selectedRewards.nooffreeitems != 0.toDouble() && selectedRewards.specificPrice != 0.0f){

                        intentt.putExtra("price", selectedRewards.specificPrice.toString())

                    }else if (selectedRewards.discountPercent != 0.0f && selectedRewards.nooffreeitems == 0.toDouble() && selectedRewards.specificPrice != 0.0f){

                        val price = (selectedRewards.specificPrice-(selectedRewards.specificPrice*selectedRewards.discountPercent/100)).toString()

                        intentt.putExtra("price", price)

                    }else if (selectedRewards.discountPercent != 0.0f && selectedRewards.nooffreeitems != 0.toDouble() && selectedRewards.specificPrice != 0.0f){


                        val price = (selectedRewards.specificPrice-(selectedRewards.specificPrice*selectedRewards.discountPercent/100)).toString()

                        intentt.putExtra("price", price)

                    }


                }

            }



            //putExtra("data", selectedSpecials.menuProd)

            startActivity(intentt)

            // 1
////
//            val selectedSpecials = arrList[position]
//
//            val prodDetails = ProductDetailsView()
//
//            val intentt = Intent(this, prodDetails::class.java)
//
//            intentt.putExtra("name", selectedSpecials.name)
////            intentt.putExtra("description", selectedSpecials.des)
//            intentt.putExtra("price", selectedSpecials.price)
////            intentt.putExtra("image", selectedSpecials.menuProd.picFilename)
////            intentt.putExtra("category", selectedSpecials.menuProd.category)
////            intentt.putExtra("clientId", selectedSpecials.menuProd.client_id)
////            intentt.putExtra("costPrice", selectedSpecials.menuProd.costPrice)
////            intentt.putExtra("createdBy", selectedSpecials.menuProd.createdBy)
////            intentt.putExtra("createdDate", selectedSpecials.menuProd.createdDate)
////            intentt.putExtra("itemId", selectedSpecials.menuProd.itemId)
////            intentt.putExtra("itemNumber", selectedSpecials.menuProd.itemNumber)
////            intentt.putExtra("itemType", selectedSpecials.menuProd.itemType)
////            intentt.putExtra("receivingQuantity", selectedSpecials.menuProd.receivingQuantity)
////            intentt.putExtra("reorderLevel", selectedSpecials.menuProd.reorderLevel)
////            intentt.putExtra("stockType", selectedSpecials.menuProd.stockType)
////            intentt.putExtra("storeId", selectedSpecials.menuProd.storeId)
////            intentt.putExtra("supplierId", selectedSpecials.menuProd.supplierId)
////            intentt.putExtra("taxCategoryId", selectedSpecials.menuProd.taxCategoryId)
////            intentt.putExtra("updatedBy", selectedSpecials.menuProd.updatedBy)
////            intentt.putExtra("updatedDate", selectedSpecials.menuProd.updatedDate)
////            intentt.putExtra("versionNo", selectedSpecials.menuProd.versionNo)
//
//            startActivity(intentt)

        }



    }



    private fun makeAdapter(list: List<String>): ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
    /**
     *
     *
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

}