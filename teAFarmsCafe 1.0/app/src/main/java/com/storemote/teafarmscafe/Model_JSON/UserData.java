package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserData {

    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {

        @SerializedName("balance")
        public String balance;
        @SerializedName("created_dtm")
        public String created_dtm;
        @SerializedName("customer_id")
        public String customer_id;
        @SerializedName("email")
        public String email;
        @SerializedName("full_name")
        public String full_name;
        @SerializedName("is_delete")
        public Boolean is_delete;
        @SerializedName("is_logged")
        public Boolean is_logged;
        @SerializedName("mobile_no")
        public String mobile_no;
        @SerializedName("password")
        public String password;
        @SerializedName("paypal_id")
        public String paypal_id;
        @SerializedName("points")
        public String points;
        @SerializedName("token")
        public String token;
        @SerializedName("username")
        public String username;
        @SerializedName("version_no")
        public String version_no;

    }
}
