package com.storemote.teafarmscafe.Model_RealmDatabase;

import java.util.Date;

import io.realm.RealmObject;

public class MenuProductDB extends RealmObject {

    private String category;
    private String client_id;
    private String cost_price;
    private String created_by;
    private String created_date;
    private String item_id;
    private String item_number;
    private String item_type;
    private String name;
    private String pic_filename;
    private String description;
    private String receiving_quantity;
    private String reorder_level;
    private String stock_type;
    private String store_id;
    private String supplier_id;
    private String tax_category_id;
    private String unit_price;
    private String updated_by;
    private String updated_date;
    private String version_no;

    private WeeklySpecials weekly_specials;
    private MenuProductExceptionDB menu_prod_exception;


    public void setCategory(String category) {

        this.category = category;
    }

    public String getCategory() {

        return category;
    }

    public void setClient_id(String client_id) {

        this.client_id = client_id;
    }

    public String getClient_id() {

        return client_id;
    }
    public void setCostPrice(String cost_price) {

        this.cost_price = cost_price;
    }

    public String getCostPrice() {

        return cost_price;
    }

    public void setCreatedBy(String created_by) {

        this.created_by = created_by;
    }

    public String getCreatedBy() {

        return created_by;
    }
    public void setCreatedDate(String created_date) {

        this.created_date = created_date;
    }

    public String getCreatedDate() {

        return created_date;
    }
    public void setItemId(String item_id) {

        this.item_id = item_id;
    }

    public String getItemId() {

        return item_id;
    }
    public void setItemNumber(String item_number) {

        this.item_number = item_number;
    }

    public String getItemNumber() {

        return item_number;
    }

    public void setItemType(String item_type) {

        this.item_type = item_type;
    }

    public String getItemType() {

        return item_type;
    }
    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setPicFilename(String pic_filename) {

        this.pic_filename = pic_filename;
    }

    public String getPicFilename() {

        return pic_filename;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getDescription() {

        return description;
    }

    public void setReceivingQuantity(String receiving_quantity) {

        this.receiving_quantity = receiving_quantity;
    }

    public String getReceivingQuantity() {

        return receiving_quantity;
    }

    public void setReorderLevel(String reorder_level) {

        this.reorder_level = reorder_level;
    }

    public String getReorderLevel() {

        return reorder_level;
    }
    public void setStockType(String stock_type) {

        this.stock_type = stock_type;
    }

    public String getStockType() {

        return stock_type;
    }

    public void setStoreId(String store_id) {

        this.store_id = store_id;
    }

    public String getStoreId() {

        return store_id;
    }

    public void setSupplierId(String supplier_id) {

        this.supplier_id = supplier_id;
    }

    public String getSupplierId() {

        return supplier_id;
    }

    public void setTaxCategoryId(String tax_category_id) {

        this.tax_category_id = tax_category_id;
    }

    public String getTaxCategoryId() {

        return tax_category_id;
    }

    public void setUnitPrice(String unit_price) {

        this.unit_price = unit_price;
    }

    public String getUnitPrice() {

        return unit_price;
    }

    public void setUpdatedBy(String updated_by) {

        this.updated_by = updated_by;
    }

    public String getUpdatedBy() {

        return updated_by;
    }
    public void setUpdatedDate(String updated_date) {

        this.updated_date = updated_date;
    }

    public String getUpdatedDate() {

        return updated_date;
    }

    public void setVersionNo(String version_no) {

        this.version_no = version_no;
    }

    public String getVersionNo() {

        return version_no;
    }


    public void setWeeklySpecials(WeeklySpecials weekly_specials) {

        this.weekly_specials = weekly_specials;
    }

    public WeeklySpecials getWeeklySpecials() {

        return weekly_specials;
    }



    public void setMenuProductException(MenuProductExceptionDB menu_prod_exception) {

        this.menu_prod_exception = menu_prod_exception;
    }

    public MenuProductExceptionDB getMenuProductException() {

        return menu_prod_exception;
    }

}

//public class MenuProductExceptionDB extends RealmObject{
//
//
//}
