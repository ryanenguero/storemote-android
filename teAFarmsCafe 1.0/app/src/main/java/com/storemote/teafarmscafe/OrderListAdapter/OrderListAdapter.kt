package com.storemote.teafarmscafe.OrderListAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.OrderListView
import com.storemote.teafarmscafe.R

class OrderListAdapter (private val context: Context,
                       private val dataSource: ArrayList<OrderListDB>) : BaseAdapter()  {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.order_list_adapter, parent, false)

        // Get title element
        val lblQty = rowView.findViewById(R.id.lblQty) as TextView
        val lblDesc = rowView.findViewById(R.id.lblDescription) as TextView
        val lblAmount = rowView.findViewById(R.id.lblAmount) as TextView
//
//        val thumbnailImageView = rowView.findViewById(R.id.rewards_page_image) as ImageView
//
        val orderList = getItem(position) as OrderListDB

        lblQty.text = orderList.quantity
        lblDesc.text = orderList.name

        if (orderList.isReward == "0") {
            lblAmount.text = "₱" + orderList.payment
        }else{
            lblAmount.text = "₱" + orderList.totalPointSpent
        }


//        OrderListView().getTotal()
//
//        nameTextView.text = rewards.name
////        addressTextView.text = weekly.menuProd.unitPrice as String
////        hoursTextView.text = "8am - 10pm"
//
////        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)
//
//        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${rewards.rewardBanner}").into(thumbnailImageView)

        return rowView
    }
}