package com.storemote.teafarmscafe

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AlertDialog
//import android.support3.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.facebook.login.LoginManager
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.*
import com.storemote.teafarmscafe.WeeklySpecialsAdapter.WeeklySpecialsViewAdapter
import io.realm.Realm
import io.realm.RealmResults
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ProfileVIew : Fragment() {

    private var listener: ProfileVIew.OnFragmentInteractionListener? = null
        private lateinit var apiInterface: APIInterface
        private lateinit var realm: Realm

    var textCartItemCount: TextView? = null
    var mCartItemCount = 10


    //.OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

//        val inflate = inflater.inflate(R.layout.weekly_specials, container, false)


        (activity as AppCompatActivity).supportActionBar?.title = "Profile"

        Realm.init(this.context)
        // Example of a call to a native method

        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        val arrList = ArrayList<UserDataDB>(realm.where(UserDataDB::class.java).findAll())

        val view = inflater.inflate(R.layout.profile_view, container, false)

        val profileImg = view.findViewById(R.id.profileImg) as ImageButton
        val txtProfileName = view.findViewById(R.id.txtProfileName) as TextView
        val txtCredit = view.findViewById(R.id.txtCredits) as TextView
        val txtPoints = view.findViewById(R.id.txtPoints) as TextView
        val rewardsBtn = view.findViewById(R.id.rewardsBtn) as TextView
        val purchasedHistoryBtn = view.findViewById(R.id.purchaseHistoryBtn) as TextView
        val logOutBtn = view.findViewById(R.id.logOutBtn) as TextView


        setHasOptionsMenu(true)


        val userData = arrList[0]

        txtProfileName.text = userData.fullname
        txtCredit.text = "₱ " + userData.balance + " credit/s"
        txtPoints.text = userData.points + " point/s"

        val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        val userPreferences = context!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

        DownloaderClass().downloadRewardsStamp(preferences.getString("storeId", "0").toString(),
                preferences.getString("clientId", "0").toString(),
                userData.username,
                userData.password, "0", "0", this.context!!, realm)

        DownloaderClass().doDownloadPurchasedHistory(preferences.getString("clientId", "0").toString(),
                userPreferences.getString("username", "0").toString(),
                userPreferences.getString("password", "0").toString(),
                "0",
                "0",
                realm)

//        apiInterface = APIClient.getClient().create(APIInterface::class.java)


        rewardsBtn.setOnClickListener {


            val arrList = ArrayList<RewardsCardDB>(realm.where(RewardsCardDB::class.java).findAll())

            if (arrList.size > 0){
            if (arrList[0].pointsToCollect != null){

            val intentt = Intent(this.context, StampView::class.java)



            view.context.startActivity(intentt)


            }else{

                val builder = AlertDialog.Builder(context!!)

                builder.setTitle("Stamp")
                builder.setMessage("You do not have any stamps available right now")

                builder.setPositiveButton("Ok") { dialog, which ->


                }

                val dialog: AlertDialog = builder.create()
                dialog.show()


            }

            }else{

                val builder = AlertDialog.Builder(context!!)

                builder.setTitle("Stamp")
                builder.setMessage("You do not have any stamps available right now")

                builder.setPositiveButton("Ok") { dialog, which ->


                }

                val dialog: AlertDialog = builder.create()
                dialog.show()
            }



        }

        purchasedHistoryBtn.setOnClickListener{


            val intentt = Intent(this.context, PurchasedHistoryView::class.java)
            view.context.startActivity(intentt)
        }



        logOutBtn.setOnClickListener {
            val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

            val editor = preferences.edit()
            editor.putBoolean("isLoggedIn", false)
            editor.putBoolean("isFB", false)
            editor.putBoolean("isGoogle", false)
            editor.commit()

            realm.beginTransaction()
            realm.delete(UserDataDB::class.java)

            realm.commitTransaction();

            LoginManager.getInstance().logOut()

            var heroActivity : TabActivity = activity as TabActivity

            heroActivity.logOutNow()

        }





//        Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${userData.}")
//                .transform(CropCircleTransformation())
//                .into(thumbnailImageView)



        return view

//        return super.onCreateView(inflater, container, savedInstanceState)

    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

//        inflater!!.inflate(R.menu.cart_menu, menu);


        inflater!!.inflate(R.menu.cart_menu, menu)

        val menuItem = menu!!.findItem(R.id.cart_btn)

        val actionView = MenuItemCompat.getActionView(menuItem)
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView

//        setupBadge()

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size


        textCartItemCount!!.text = mCartItemCount.toString()

        actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItem)
            }
        })

    }

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

//        TabActivity().createProfileFragment()

    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

//        TabActivity().createProfileFragment()

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

//        val id = item?.getItemId()
//
//        //noinspection SimplifiableIfStatement
//        return if (id == R.id.cart_btn) {
//            true
//        } else super.onOptionsItemSelected(item)

        when (item?.getItemId()) {

            R.id.cart_btn -> {
                // Do something

                val intentt = Intent(this.context, OrderListView::class.java)

                startActivity(intentt)

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        super.onCreateOptionsMenu(view, inflater)
//
////        inflater!!.inflate(R.menu.cart_menu, menu);
//
//
//        inflater!!.inflate(R.menu.cart_menu, menu)
//
//        val menuItem = menu!!.findItem(R.id.cart_btn)
//
//        val actionView = MenuItemCompat.getActionView(menuItem)
//        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView
//
////        setupBadge()
//
//        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
//        mCartItemCount = orderListCount.size
//
//
//        textCartItemCount!!.text = mCartItemCount.toString()
//
//        actionView.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View) {
//                onOptionsItemSelected(menuItem)
//            }
//        })
//
//    }

    private fun setupBadge() {

//        if (textCartItemCount != null) {
//            if (mCartItemCount === 0) {
//                if (textCartItemCount!!.getVisibility() !== View.GONE) {
//                    textCartItemCount!!.setVisibility(View.GONE)
//                }
//            } else {
//                textCartItemCount!!.text = (mCartItemCount) as String
//
//                        //.setText(String.valueOf(Math.min(mCartItemCount, 99)))
//                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
//                    textCartItemCount!!.setVisibility(View.VISIBLE)
//                }
//            }
//        }
    }



    fun onButtonPressed(uri: Uri) {

        listener?.onFragmentInteraction(uri)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        fun newInstance() : Fragment
        {

            var fb = ProfileVIew()
            return fb


        }

    }


}