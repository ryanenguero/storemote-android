package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RewardsCardItems {

    @SerializedName("data")
    public List<Items> data = new ArrayList<>();

    public class Items {

        @SerializedName("item_id")
        public String item_id;

        @SerializedName("name")
        public String name;

        @SerializedName("description")
        public String description;

        @SerializedName("pic_filename")
        public String pic_filename;

        @SerializedName("store_id")
        public String store_id;

    }
}
