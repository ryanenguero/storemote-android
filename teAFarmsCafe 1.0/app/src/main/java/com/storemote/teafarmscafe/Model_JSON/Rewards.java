package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Rewards {

    @SerializedName("data")
    public List<RewardsData> data = new ArrayList<>();

    public class RewardsData {

        @SerializedName("discount_percent")
        public String discount_percent;

        @SerializedName("from_date")
        public String from_date;

        @SerializedName("name")
        public String name;

        @SerializedName("no_of_free_items")
        public String no_of_free_items;

        @SerializedName("price")
        public String price;

        @SerializedName("reward_banner")
        public String reward_banner;

        @SerializedName("reward_id")
        public String reward_id;

        @SerializedName("specific_price")
        public String specific_price;

        @SerializedName("to_date")
        public String to_date;

    }
}
