package com.storemote.teafarmscafe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ListView
import com.storemote.teafarmscafe.Model_JSON.PurchasedHistory
import com.storemote.teafarmscafe.Model_JSON.RewardsCard
import com.storemote.teafarmscafe.Model_RealmDatabase.PromoListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PurchasedHistoryDB
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardDB
import com.storemote.teafarmscafe.PromoViewAdapter.PromoViewAdapter
import io.realm.Realm
import io.realm.RealmResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class PurchasedHistoryView : AppCompatActivity() {

    private lateinit var listview: ListView
    private var arrList = ArrayList<Any>()
    private lateinit var realm: Realm
    private lateinit var adapt: PurchasedHistoryAdapter
    private lateinit var purchasedHistory: RealmResults<PurchasedHistoryDB>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.purchased_history)

        supportActionBar?.title = "Purchased History"

        listview = findViewById<ListView>(R.id.purchased_history_view)
                //findViewById(R.layout.purchased_history)

        Realm.init(this)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
//        promos = realm.where(PromoListDB::class.java).findAllAsync()



//        val arr = ArrayList<WeeklySpecials>(arrList)

//        realm.beginTransaction()
//        realm.delete(PurchasedHistoryDB::class.java)
//        realm.commitTransaction()

       val apiInterface = APIClient.getClient().create(APIInterface::class.java)


        purchasedHistory = realm.where(PurchasedHistoryDB::class.java).findAllAsync()
//        NotificationCenter.defaultCenter().addFucntionForNotification("refreshPurchasedHistory") {

            val arrList = ArrayList<PurchasedHistoryDB>(realm.where(PurchasedHistoryDB::class.java).findAll())

            val adapt = PurchasedHistoryAdapter(this, arrList)
            listview.adapter = adapt

//        }



        listview?.setOnItemClickListener { _, _, position, _ ->

            val data = arrList[position]

            val intentt = Intent(this, PurchaseDetailsView::class.java)

            val preferences = baseContext!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            val userPreferences = baseContext!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

            intentt.putExtra("pdfURL", data.saleId)
            intentt.putExtra("pdfUsername", userPreferences.getString("username", "0").toString())
            intentt.putExtra("pdfClientId", preferences.getString("clientId", "0").toString())

            startActivity(intentt)
//
        }



    }
}