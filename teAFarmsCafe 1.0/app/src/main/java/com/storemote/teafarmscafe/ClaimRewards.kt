package com.storemote.teafarmscafe

import android.content.Context
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.GridView
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.storemote.teafarmscafe.Model_JSON.ClaimRewardsMessage
import com.storemote.teafarmscafe.Model_JSON.RewardsCardItems
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardDB
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardItemsDB
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ClaimRewards : AppCompatActivity(){


private lateinit var realm: Realm

    private var listview: ListView? = null


override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.claim_rewards_item_list)

    supportActionBar?.title = "Claim Reward"

//    claimStampView = findViewById<ImageView>(R.layout.claim_reward_item_layout) as ImageView

    listview  = findViewById(R.id.rewardsItemsListView)


    Realm.init(this)

    realm = Realm.getDefaultInstance()

    val arrList = ArrayList<RewardsCardItemsDB>(realm.where(RewardsCardItemsDB::class.java).findAll())

    val adapt = ClaimRewardsAdapter(this, arrList)

    listview!!.adapter = adapt

    listview!!.setOnItemClickListener { _, _, position, _ ->


        val builderClaim = AlertDialog.Builder(this)

        builderClaim.setTitle("Rewards")
        builderClaim.setMessage("Do you want to claim this reward?")
        builderClaim.setPositiveButton("Yes") { dialog, which ->


        val apiInterface = APIClient.getClient().create(APIInterface::class.java)
        val map = HashMap<String, String>()
        val preferences = baseContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        val userPreferences = baseContext!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

        val username = userPreferences.getString("username", "").toString()
        val password = userPreferences.getString("password", "").toString()

        val androidId = Settings.Secure.getString(contentResolver,
                Settings.Secure.ANDROID_ID)

        map["item_id"] = arrList[position].itemId
        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["store_id"] =  preferences.getString("storeId", "0").toString()
        map["client_id"] =  preferences.getString("clientId", "0").toString()
        map["username"] = username
        map["password"] = password
        map["is_fb"] = preferences.getString("isFB", "0").toString()
        map["is_google"] = preferences.getString("isGoogle", "0").toString()
        map["device_token"] = androidId


        val call3 = apiInterface.claimRewards(map)
        call3.enqueue(
                object : Callback<ClaimRewardsMessage> {

                    override fun onResponse(call: Call<ClaimRewardsMessage>, response: Response<ClaimRewardsMessage>) {
                        val rewardsStampData = response.body()
                        val data = rewardsStampData!!.status_message

                        val builder = AlertDialog.Builder(this@ClaimRewards)

                        builder.setTitle("Status")
                        builder.setMessage(data)
                        builder.setPositiveButton("OK") { dialog, which ->

                            finish()
                        }

                        val dialog1: AlertDialog = builder.create()
                        dialog1.show()


                    }

                    override fun onFailure(call: Call<ClaimRewardsMessage>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })


        }

        builderClaim.setNegativeButton("No") { dialog, which ->


        }

        val dialogClaim: AlertDialog = builderClaim.create()
        dialogClaim.show()


    }




    }

}

