package com.storemote.teafarmscafe.Model_RealmDatabase;

import java.util.Date;

import io.realm.RealmObject;

public class
Rewards extends RealmObject {

//    private Float discount_percent;
//    private Date from_date;
//    private String name;
//    private Integer no_of_free_items;
//    private Float price;
//    private String reward_banner;
//    private Integer reward_id;
//    private Float specific_price;
//    private Date to_date;
//
//    public void setDiscountPercent(Float discount_percent) {
//
//        this.discount_percent = discount_percent;
//    }
//
//    public Float getDiscountPercent() {
//
//        return discount_percent;
//    }
//
//    public void setFromDate(Date from_date) {
//
//        this.from_date = from_date;
//    }
//
//    public Date getFromDate() {
//
//        return from_date;
//    }
//
//    public void setName(String name) {
//
//        this.name = name;
//    }
//
//    public String getName() {
//
//        return name;
//    }
//
//    public void setNooffreeitems(Integer no_of_free_items) {
//
//        this.no_of_free_items = no_of_free_items;
//    }
//
//    public Integer getNooffreeitems() {
//
//        return no_of_free_items;
//    }
//
//    public void setPrice(Float price) {
//
//        this.price = price;
//    }
//
//    public Float getPrice() {
//
//        return price;
//    }
//
//    public void setRewardBanner(String reward_banner) {
//
//        this.reward_banner = reward_banner;
//    }
//
//    public String getRewardBanner() {
//
//        return reward_banner;
//    }
//
//    public void setRewardId(Integer reward_id) {
//
//        this.reward_id = reward_id;
//    }
//
//    public Integer getRewardId() {
//
//        return reward_id;
//    }
//
//    public void setSpecificPrice(Float specific_price) {
//
//        this.specific_price = specific_price;
//    }
//
//    public Float getSpecificPrice() {
//
//        return specific_price;
//    }
//
//    public void setToDate(Date to_date) {
//
//        this.to_date = to_date;
//    }
//
//    public Date getToDate() {
//
//        return to_date;
//    }


    private String discount_percent;
    private String from_date;
    private String name;
    private String no_of_free_items;
    private String price;
    private String reward_banner;
    private String reward_id;
    private String specific_price;
    private String to_date;

    public void setDiscountPercent(String discount_percent) {

        this.discount_percent = discount_percent;
    }

    public String getDiscountPercent() {

        return discount_percent;
    }

    public void setFromDate(String from_date) {

        this.from_date = from_date;
    }

    public String getFromDate() {

        return from_date;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setNooffreeitems(String no_of_free_items) {

        this.no_of_free_items = no_of_free_items;
    }

    public String getNooffreeitems() {

        return no_of_free_items;
    }

    public void setPrice(String price) {

        this.price = price;
    }

    public String getPrice() {

        return price;
    }

    public void setRewardBanner(String reward_banner) {

        this.reward_banner = reward_banner;
    }

    public String getRewardBanner() {

        return reward_banner;
    }

    public void setRewardId(String reward_id) {

        this.reward_id = reward_id;
    }

    public String getRewardId() {

        return reward_id;
    }

    public void setSpecificPrice(String specific_price) {

        this.specific_price = specific_price;
    }

    public String getSpecificPrice() {

        return specific_price;
    }

    public void setToDate(String to_date) {

        this.to_date = to_date;
    }

    public String getToDate() {

        return to_date;
    }
}
