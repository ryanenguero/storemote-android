package com.storemote.teafarmscafe


import android.app.Activity
import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso



class GridAdapter (private val context: Context,
                   private val dataSource: ArrayList<Int>,
                   private val totalStamp: Int) : BaseAdapter() {




    //1
    override fun getCount(): Int {

        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        // Get view for row item
//

        var myView = convertView
        var holder: ViewHolder


        if (myView == null) {

            //If our View is Null than we Inflater view using Layout Inflater

            val mInflater = (context as Activity).layoutInflater

            //Inflating our grid_item.
            myView = mInflater.inflate(R.layout.stamp_view_adapter, parent, false)

            //Create Object of ViewHolder Class and set our View to it

            holder = ViewHolder()


            //Find view By Id For all our Widget taken in grid_item.

            /*Here !! are use for non-null asserted two prevent From null.
             you can also use Only Safe (?.)

            */


            holder.mImageView = myView!!.findViewById<ImageView>(R.id.imageView) as ImageView


            //Set A Tag to Identify our view.
            myView.setTag(holder)


        } else {

            //If Our View in not Null than Just get View using Tag and pass to holder Object.

            holder = myView.getTag() as ViewHolder

        }

        //Setting Image to ImageView by position

        holder.mImageView!!.setImageResource(R.drawable.graycup)

        for (i in 0 until totalStamp) {

            if (i == position){
                holder.mImageView!!.setImageResource(R.drawable.bluecup)

            }

        }


        return myView

    }

    class ViewHolder {

        var mImageView: ImageView? = null
    }
}










