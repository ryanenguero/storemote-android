package com.storemote.teafarmscafe

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import com.storemote.teafarmscafe.WeeklySpecialsAdapter.WeeklySpecialsViewAdapter
import io.realm.Realm
import io.realm.RealmResults


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [WeeklySpecialsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [WeeklySpecialsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class WeeklySpecialsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private val PREFS_TASKS = "prefs_tasks"
    private val KEY_TASKS_LIST = "tasks_list"

    private lateinit var realm: Realm
    private lateinit var weeklyspecialsData: RealmResults<WeeklySpecials>


    private lateinit var listview: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentManager!!.beginTransaction()
                .replace(android.R.id.content, WeeklySpecialsFragment.newInstance())
                .commit()

//        setupActionBar()

//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }

//        view!!.setContentView(R.layout.tab_activity)

//       fragmentManager!!.beginTransaction().add(R.id.fragment_weekly_specials, WeeklySpecialsFragment.newInstance()).commit()


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        listview = view!!.findViewById(R.id.weeklylistview)


        Realm.init(view!!.context)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
        weeklyspecialsData = realm.where(WeeklySpecials::class.java).findAllAsync()



        val arrList = ArrayList<WeeklySpecials>(realm.where(WeeklySpecials::class.java).findAll())

//        val arr = ArrayList<WeeklySpecials>(arrList)

        val adapt = WeeklySpecialsViewAdapter(view!!.context, arrList)
        listview.adapter = adapt

        return inflater.inflate(R.layout.fragment_weekly_specials, container, false)

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WeeklySpecialsFragment.
         */
        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//                WeeklySpecialsFragment().apply {
//                    arguments = Bundle().apply {
//                        putString(ARG_PARAM1, param1)
//                        putString(ARG_PARAM2, param2)
//                    }
//                }

        fun newInstance() : Fragment
        {

            var fb = WeeklySpecialsFragment()
            return fb

        }
    }
}
