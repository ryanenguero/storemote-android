package com.storemote.teafarmscafe

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.ListView
import android.widget.TextView
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PromoListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PromosDB
import com.storemote.teafarmscafe.Model_RealmDatabase.Rewards
import com.storemote.teafarmscafe.PromoViewAdapter.PromoViewAdapter
import com.storemote.teafarmscafe.RewardsViewAdapter.RewardsViewAdapter
import io.realm.Realm
import io.realm.RealmResults

class PromoView : Fragment() {

    private val PREFS_TASKS = "prefs_tasks"
    private val KEY_TASKS_LIST = "tasks_list"

    private lateinit var realm: Realm
    private lateinit var promos: RealmResults<PromoListDB>

    private lateinit var listview: ListView

    private lateinit var listItems: ArrayList<String>
    private var listener: PromoView.OnFragmentInteractionListener? = null

    var textCartItemCount: TextView? = null
    var mCartItemCount = 10

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//            super.onCreate(savedInstanceState)
//            setContentView(R.layout.rewards_list)
//
//
//            listview = findViewById<ListView>(R.id.rewardsListView)

        (activity as AppCompatActivity).supportActionBar?.title = "Promo"

        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.promo_list, container, false)

        listview = view.findViewById(R.id.promoListView)

        Realm.init(this.context!!)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
        promos = realm.where(PromoListDB::class.java).findAllAsync()


        val arrList = ArrayList<PromoListDB>(realm.where(PromoListDB::class.java).findAll())

//        val arr = ArrayList<WeeklySpecials>(arrList)

        val adapt = PromoViewAdapter(this.context!!, arrList)
        listview.adapter = adapt


        listview?.setOnItemClickListener { _, _, position, _ ->
            // 1
//
            val selectedSpecials = arrList[position]

//            val prodDetails = ProductDetailsView()

            if (arrList.size > 0){
              val intentt = Intent(this.context, PromoListView::class.java)
              intentt.putExtra("titleProms", arrList[position].name)
                startActivity(intentt)
            }else{

                val builder = AlertDialog.Builder(context!!)


                builder.setTitle("Failed")
                builder.setMessage("There are no promo")
                builder.setPositiveButton("Ok") { dialog, which ->

                }

                val dialog: AlertDialog = builder.create()
                dialog.show()

            }

        }

        return view


    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

//        inflater!!.inflate(R.menu.cart_menu, menu);


        inflater!!.inflate(R.menu.cart_menu, menu)

        val menuItem = menu!!.findItem(R.id.cart_btn)

        val actionView = MenuItemCompat.getActionView(menuItem)
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView

//        setupBadge()

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size


        textCartItemCount!!.text = mCartItemCount.toString()

        actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItem)
            }
        })

    }

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

//        val id = item?.getItemId()
//
//        //noinspection SimplifiableIfStatement
//        return if (id == R.id.cart_btn) {
//            true
//        } else super.onOptionsItemSelected(item)

        when (item?.getItemId()) {

            R.id.cart_btn -> {
                // Do something

                val intentt = Intent(this.context, OrderListView::class.java)

                startActivity(intentt)

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBadge() {

//        if (textCartItemCount != null) {
//            if (mCartItemCount === 0) {
//                if (textCartItemCount!!.getVisibility() !== View.GONE) {
//                    textCartItemCount!!.setVisibility(View.GONE)
//                }
//            } else {
//                textCartItemCount!!.text = (mCartItemCount) as String
//
//                        //.setText(String.valueOf(Math.min(mCartItemCount, 99)))
//                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
//                    textCartItemCount!!.setVisibility(View.VISIBLE)
//                }
//            }
//        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        fun newInstance(): Fragment {

            var fb = PromoView()
            return fb

        }
    }

}