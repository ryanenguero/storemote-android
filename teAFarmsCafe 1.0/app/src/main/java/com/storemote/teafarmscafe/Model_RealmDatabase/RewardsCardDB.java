package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class RewardsCardDB extends RealmObject {

    private Integer card_points;
    private Integer points_to_collect;
    private Integer reward_card_id;

    public void setCardPoints(Integer card_points) {

        this.card_points = card_points;
    }

    public Integer getCardPoints() {

        return card_points;
    }

    public void setPointsToCollect(Integer points_to_collect) {

        this.points_to_collect = points_to_collect;
    }

    public Integer getPointsToCollect() {

        return points_to_collect;
    }

    public void setRewardCardId(Integer reward_card_id) {

        this.reward_card_id = reward_card_id;
    }

    public Integer getRewardCardId() {

        return reward_card_id;
    }
}
