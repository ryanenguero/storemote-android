package com.storemote.teafarmscafe.MenuViewAdapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.R
import com.storemote.teafarmscafe.MenuView
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductExceptionDB
import de.hdodenhof.circleimageview.CircleImageView
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import java.util.*
import kotlin.collections.ArrayList
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class MenuViewAdapter (private val context: Context) : BaseAdapter() {

//(private val context: Context,
//                        private val dataSource: ArrayList<Any>) : BaseAdapter() {


    private val TYPE_ITEM = 0
    private val TYPE_SEPARATOR = 1

    public var mData = ArrayList<Any>()
    private val sectionHeader = TreeSet<Int>()
    private val filteredData = ArrayList<Any>()
    private val filt = ArrayList<MenuProductExceptionDB>()
    private var isItFiltered : Boolean? = null

//    private var mInflater: LayoutInflater? = null

//    fun MenuViewAdapter(context: Context) {
//        mInflater = context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//    }

    private val mInflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    fun addItem(item: MenuProductExceptionDB, isFiltered: Boolean) {
        if (isFiltered == true){
//            filteredData.removeAll(filteredData)
            filteredData.add(item)
            isItFiltered = true
        }else {
//            filteredData.removeAll(filteredData)
            mData.add(item)
            isItFiltered = false
        }
        notifyDataSetChanged()
    }

    fun addSectionHeaderItem(item: String, isFiltered: Boolean) {

        if (isFiltered == true){
//            filteredData.removeAll(filteredData)
            filteredData.add(item)
            sectionHeader.add(filteredData.size - 1)
            isItFiltered = true
        }else {
//            filteredData.removeAll(filteredData)
            mData.add(item)
            sectionHeader.add(mData.size - 1)
            isItFiltered = false
        }


        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (sectionHeader.contains(position)) TYPE_SEPARATOR else TYPE_ITEM
    }

    override fun getViewTypeCount(): Int {
        return 2
    }

    override fun getCount(): Int {
        if (isItFiltered == true){
            return filteredData.size
        }else {
            return mData.size
        }
    }

    override fun getItem(position: Int): Any {
        if (isItFiltered == true){
            return filteredData[position]
        }else {
            return mData[position]
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        var holder: ViewHolder? = null
        val rowType = getItemViewType(position)

//        Log.d("MainActivity", "ResponseAdapter =  $mData");

        if (convertView == null) {
            holder = ViewHolder()
            when (rowType) {
                TYPE_ITEM -> {
                    convertView = mInflater!!.inflate(R.layout.menu_list_row, null)
                    holder.textView = convertView!!.findViewById<View>(R.id.textrow) as TextView
                    holder.priceView = convertView!!.findViewById<View>(R.id.pricerow) as TextView
                    holder.imageView = convertView!!.findViewById<View>(R.id.imagerow) as ImageView

                }
                TYPE_SEPARATOR -> {
                    convertView = mInflater!!.inflate(R.layout.menu_list_header, null)
                    holder.textView = convertView!!.findViewById<View>(R.id.textSeparator) as TextView


                }
            }

//            if ((mData[position] as String) != true)
            convertView!!.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        if (isItFiltered == true){

            if (filteredData[position] is String) {

                holder.textView!!.text = filteredData[position] as String


            } else {


                val d = filteredData[position] as MenuProductExceptionDB
                holder.textView!!.text = d.menuProd.name
                holder.priceView!!.text = "₱" + d.menuProd.unitPrice


//            Picasso.get().load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}").into(holder.imageView)

//            Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")

                Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")
                        .transform(CropCircleTransformation())
                        .into(holder.imageView)
            }

        }else {

            if (mData[position] is String) {

                holder.textView!!.text = mData[position] as String


            } else {


                val d = mData[position] as MenuProductExceptionDB
                holder.textView!!.text = d.menuProd.name
                holder.priceView!!.text = "₱" + d.menuProd.unitPrice


//            Picasso.get().load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}").into(holder.imageView)

//            Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")

                Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")
                        .transform(CropCircleTransformation())
                        .into(holder.imageView)
            }
        }



        return convertView
    }

    // Filter Class
    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())

        Log.d("MainActivity", "search menu menubbb =  $mData");

        if (charText.length == 0) {

            isItFiltered = false

//            filteredData.removeAll(filteredData)
//            for (da in 0 until mData.size) {
//                if (mData[da] is String){
//
//                    val header = mData[da] as String
//
//                    addSectionHeaderItem(header, false)
//
//                }else{
//
//                    val prod = mData[da] as MenuProductExceptionDB
//                    addItem(prod, false)
//
//                }
//            }
//            MenuView.newInstance()
        } else {

            val dat = ArrayList<Any>()

            for (da in 0 until mData.size) {


                if (mData[da] is String){
                    val header = mData[da] as String

                    addSectionHeaderItem(header, true)
                }else {

                        val prod = mData[da] as MenuProductExceptionDB

                        if (prod.menuProd.name.toString().toLowerCase().contains(charText)){

                            addItem(prod, true)
                        }

                }
            }

        }

        Log.d("MainActivity", "search menu =  $mData");
        Log.d("MainActivity", "search menu filtered =  $filteredData");


        notifyDataSetChanged()
    }

    class ViewHolder {
        var textView: TextView? = null
        var imageView: ImageView? = null
        var priceView: TextView? = null
    }



}

