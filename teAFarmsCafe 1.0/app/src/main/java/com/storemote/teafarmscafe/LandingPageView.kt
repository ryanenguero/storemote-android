package com.storemote.teafarmscafe

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ListView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.LandingPageViewAdapter.LandingPageViewAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.LandingPageDB
import com.storemote.teafarmscafe.StoreListAdapter.StoreListAdapt
import io.realm.Realm
import kotlinx.android.synthetic.main.landing_page.*
import android.text.method.Touch.onTouchEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.ImageView


class LandingPageView : AppCompatActivity() {

    private lateinit var realm: Realm
    private lateinit var listview: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.landing_page)

        val preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)


        supportActionBar!!.title =  preferences.getString("storeName", "0").toString()
//        supportActionBar!!.setDefaultDisplayHomeAsUpEnabled(true)



            Realm.init(this)

            realm = Realm.getDefaultInstance()


//        val arrList = ArrayList<LandingPageDB>(realm.where(LandingPageDB::class.java).findAll())

        val landingData = realm.where(LandingPageDB::class.java).findAll()

        Log.d("MainActivity", "Response Landing =  $landingData")

        landing_page_weekly_caption.text = "Weekly Specials"
        landing_page_menu_caption.text = "See Our Menu"
        landing_page_promo_caption.text = "Promo"
        landing_page_rewards_caption.text = "Rewards"

        val weekly = findViewById(R.id.landing_page_weeklyspecials_image) as ImageView
        val menu = findViewById(R.id.landing_page_menu_image) as ImageView
        val promo = findViewById(R.id.landing_page_promo_image) as ImageView
        val rewards = findViewById(R.id.landing_page_rewards_image) as ImageView


        Picasso.with(this).load("http://storemote.net/assets/uploads/item_pics/${landingData[0]!!.weeklySpecial}").into(weekly)
        Picasso.with(this).load("http://storemote.net/assets/uploads/item_pics/${landingData[0]!!.menu}").into(menu)
        Picasso.with(this).load("http://storemote.net/assets/uploads/item_pics/${landingData[0]!!.promo}").into(promo)
        Picasso.with(this).load("http://storemote.net/assets/uploads/item_pics/${landingData[0]!!.rewards}").into(rewards)


        weekly.setOnClickListener {

            Log.d("MainActivity", "Weekly Tapped")

            val intentt = Intent(this, TabActivity::class.java)

            intentt.putExtra("weeklyBanner", landingData[0]!!.weeklySpecial)
            intentt.putExtra("index", 0)


            startActivity(intentt)
        }

        menu.setOnClickListener {

            Log.d("MainActivity", "Menu Tapped")

            val intentt = Intent(this, TabActivity::class.java)
//            TabActivity.tab

            intentt.putExtra("weeklyBanner", landingData[0]!!.weeklySpecial)
            intentt.putExtra("index", 1)

            startActivity(intentt)


        }

        promo.setOnClickListener {

            Log.d("MainActivity", "Promo Tapped")


            val intentt = Intent(this, TabActivity::class.java)

            intentt.putExtra("weeklyBanner", landingData[0]!!.weeklySpecial)
            intentt.putExtra("index", 3)

            startActivity(intentt)

        }

        rewards.setOnClickListener {

            Log.d("MainActivity", "Rewards Tapped")

            val intentt = Intent(this, TabActivity::class.java)

            intentt.putExtra("weeklyBanner", landingData[0]!!.weeklySpecial)
            intentt.putExtra("index", 4)

            startActivity(intentt)
        }



    }




}