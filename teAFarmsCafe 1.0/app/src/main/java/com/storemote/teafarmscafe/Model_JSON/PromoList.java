package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PromoList {

    @SerializedName("data")
    public List<PromoListData> data = new ArrayList<>();

    public class PromoListData {

        @SerializedName("from_date")
        public Date from_date;
        @SerializedName("name")
        public String name;
        @SerializedName("promo_banner")
        public String promo_banner;
        @SerializedName("promo_id")
        public Integer promo_id;
        @SerializedName("to_date")
        public Date to_date;

    }
}
