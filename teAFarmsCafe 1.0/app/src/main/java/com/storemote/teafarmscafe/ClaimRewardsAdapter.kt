package com.storemote.teafarmscafe

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardItemsDB
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class ClaimRewardsAdapter (private val context: Context,
                           private val dataSource: ArrayList<RewardsCardItemsDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getItem(position: Int): Any {
        return dataSource[position]

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.weekly_specials_adapter, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.weekly_name) as TextView

// Get subtitle element
        val addressTextView = rowView.findViewById(R.id.weekly_price) as TextView

// Get detail element
//        val hoursTextView = rowView.findViewById(R.id.store_list_hours) as TextView

// Get thumbnail element
        val thumbnailImageView = rowView.findViewById(R.id.weekly_image) as ImageView

        val weekly = getItem(position) as RewardsCardItemsDB

        nameTextView.text = weekly.name
        addressTextView.text ="Free"
//        hoursTextView.text = "8am - 10pm"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

//        Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${weekly.menuProd.picFilename}").into(thumbnailImageView)

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${weekly.picFilename}")
                .transform(CropCircleTransformation())
                .into(thumbnailImageView)

        return rowView
    }

}
