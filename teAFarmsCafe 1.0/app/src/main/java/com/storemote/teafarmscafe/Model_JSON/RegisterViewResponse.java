package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

public class RegisterViewResponse {

    @SerializedName("success_message")
    public String message;
}
