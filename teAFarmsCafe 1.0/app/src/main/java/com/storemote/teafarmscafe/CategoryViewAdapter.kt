package com.storemote.teafarmscafe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_JSON.Category
import com.storemote.teafarmscafe.Model_RealmDatabase.CategoryDB
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class CategoryViewAdapter  (private val context: Context,
                            private val dataSource: ArrayList<CategoryDB>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.category_view_adapter, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.category_name) as TextView

        val category = getItem(position) as CategoryDB

        nameTextView.text = category.name

        return rowView
    }

}