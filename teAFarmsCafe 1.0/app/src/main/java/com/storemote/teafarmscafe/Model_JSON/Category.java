package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Category {

    @SerializedName("data")
    public List<CategoryData> data = new ArrayList<>();

    public class CategoryData {

        @SerializedName("category_id")
        public String category_id;
        @SerializedName("name")
        public String name;

    }
}
