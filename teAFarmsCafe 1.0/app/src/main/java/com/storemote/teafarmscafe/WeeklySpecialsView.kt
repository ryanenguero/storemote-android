package com.storemote.teafarmscafe

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.Layout
import android.util.Log
import android.widget.ListView
import com.squareup.picasso.Picasso

import com.storemote.teafarmscafe.LandingPageViewAdapter.LandingPageViewAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import io.realm.Realm
import kotlinx.android.synthetic.main.landing_page.*
import android.text.method.Touch.onTouchEvent
import android.view.*
import android.view.View.OnTouchListener
import android.widget.ImageView
import android.widget.TextView
import com.storemote.teafarmscafe.Model_JSON.MenuProduct
import com.storemote.teafarmscafe.Model_JSON.WeeklySpecialsExceptionParser
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductDB
import com.storemote.teafarmscafe.StoreListAdapter.StoreListAdapt
import com.storemote.teafarmscafe.WeeklySpecialsAdapter.WeeklySpecialsViewAdapter

import io.realm.RealmResults
import android.support.v4.view.MenuItemCompat
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB


class WeeklySpecialsView : Fragment(){

    internal lateinit var apiInterface: APIInterface

//    private val taskList: MutableList<String> = mutableListOf()
//    private val adapter by lazy { makeAdapter(taskList) }

    private val PREFS_TASKS = "prefs_tasks"
    private val KEY_TASKS_LIST = "tasks_list"

    private lateinit var realm: Realm
    private lateinit var weeklyspecials: RealmResults<WeeklySpecials>


    private var listview: ListView? = null
    private var banner: ImageView? = null
//    private var intent : Intent? = null

    private lateinit var listItems: ArrayList<String>
    private var listener: WeeklySpecialsView.OnFragmentInteractionListener? = null


    var textCartItemCount: TextView? = null
    var mCartItemCount = 10

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

//        val inflate = inflater.inflate(R.layout.weekly_specials, container, false)

        (activity as AppCompatActivity).supportActionBar?.title = "Weekly Specials"


        setHasOptionsMenu(true)

        val view = inflater.inflate(R.layout.weekly_specials, container, false)

        banner = view.findViewById(R.id.landing_page_weeklyspecials_image2)
        listview  = view.findViewById(R.id.weeklylistview)

//        this.intent


        val str = activity!!.intent!!.getStringExtra("weeklyBanner")

               // intent!!.getStringExtra("weeklyBanner")

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/$str").into(banner)

        Realm.init(view.context)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()
        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
        weeklyspecials = realm.where(WeeklySpecials::class.java).findAllAsync()




        val arrList = ArrayList<WeeklySpecials>(realm.where(WeeklySpecials::class.java).findAll())
                //ArrayList<WeeklySpecials>(realm.where(WeeklySpecials::class.java).findAll())

//        val arr = ArrayList<WeeklySpecials>(arrList)

        val adapt = WeeklySpecialsViewAdapter(view.context, arrList)
        listview?.adapter = adapt


        listview?.setOnItemClickListener { _, _, position, _ ->
            // 1
//
            val selectedSpecials = arrList[position]

            val prodDetails = ProductDetailsView()

            val intentt = Intent(this.context, prodDetails::class.java)

            intentt.putExtra("is_promo", "0")
            intentt.putExtra("is_rewards", "0")
            intentt.putExtra("name", selectedSpecials.menuProd.name)
            intentt.putExtra("description", selectedSpecials.menuProd.description)
            intentt.putExtra("price", selectedSpecials.menuProd.unitPrice)
            intentt.putExtra("image", selectedSpecials.menuProd.picFilename)
            intentt.putExtra("category", selectedSpecials.menuProd.category)
            intentt.putExtra("clientId", selectedSpecials.menuProd.client_id)
            intentt.putExtra("costPrice", selectedSpecials.menuProd.costPrice)
            intentt.putExtra("createdBy", selectedSpecials.menuProd.createdBy)
            intentt.putExtra("createdDate", selectedSpecials.menuProd.createdDate)
            intentt.putExtra("itemId", selectedSpecials.menuProd.itemId)
            intentt.putExtra("itemNumber", selectedSpecials.menuProd.itemNumber)
            intentt.putExtra("itemType", selectedSpecials.menuProd.itemType)
            intentt.putExtra("receivingQuantity", selectedSpecials.menuProd.receivingQuantity)
            intentt.putExtra("reorderLevel", selectedSpecials.menuProd.reorderLevel)
            intentt.putExtra("stockType", selectedSpecials.menuProd.stockType)
            intentt.putExtra("storeId", selectedSpecials.menuProd.storeId)
            intentt.putExtra("supplierId", selectedSpecials.menuProd.supplierId)
            intentt.putExtra("taxCategoryId", selectedSpecials.menuProd.taxCategoryId)
            intentt.putExtra("updatedBy", selectedSpecials.menuProd.updatedBy)
            intentt.putExtra("updatedDate", selectedSpecials.menuProd.updatedDate)
            intentt.putExtra("versionNo", selectedSpecials.menuProd.versionNo)

            startActivity(intentt)

        }

        return view

//        return super.onCreateView(inflater, container, savedInstanceState)

    }



    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        setHasOptionsMenu(true)
    }




    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

//        inflater!!.inflate(R.menu.cart_menu, menu);


        inflater!!.inflate(R.menu.cart_menu, menu)

        val menuItem = menu!!.findItem(R.id.cart_btn)

        val actionView = MenuItemCompat.getActionView(menuItem)
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView

//        setupBadge()

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size


        textCartItemCount!!.text = mCartItemCount.toString()

        actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItem)
            }
        })

    }

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

//        val id = item?.getItemId()
//
//        //noinspection SimplifiableIfStatement
//        return if (id == R.id.cart_btn) {
//            true
//        } else super.onOptionsItemSelected(item)

        when (item?.getItemId()) {

            R.id.cart_btn -> {
                // Do something

                val intentt = Intent(this.context, OrderListView::class.java)

                startActivity(intentt)

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBadge() {

//        if (textCartItemCount != null) {
//            if (mCartItemCount === 0) {
//                if (textCartItemCount!!.getVisibility() !== View.GONE) {
//                    textCartItemCount!!.setVisibility(View.GONE)
//                }
//            } else {
//                textCartItemCount!!.text = (mCartItemCount) as String
//
//                        //.setText(String.valueOf(Math.min(mCartItemCount, 99)))
//                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
//                    textCartItemCount!!.setVisibility(View.VISIBLE)
//                }
//            }
//        }
    }


    fun onButtonPressed(uri: Uri) {

        listener?.onFragmentInteraction(uri)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        fun newInstance() : Fragment
        {

            var fb = WeeklySpecialsView()
            return fb


        }
    }


}