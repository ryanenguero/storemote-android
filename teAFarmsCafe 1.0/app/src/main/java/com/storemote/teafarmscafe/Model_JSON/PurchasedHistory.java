package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PurchasedHistory {

    @SerializedName("data")
    public List<PurchasedHistoryData> data = new ArrayList<>();

    public class PurchasedHistoryData {

        @SerializedName("amount_due")
        public String amount_due;

        @SerializedName("comment")
        public String comment;

        @SerializedName("comments")
        public String comments;

        @SerializedName("customer_name")
        public String customer_name;

        @SerializedName("email")
        public String email;

        @SerializedName("invoice_number")
        public String invoice_number;

        @SerializedName("qoute_number")
        public String qoute_number;

        @SerializedName("sale_date")
        public String sale_date;

        @SerializedName("sale_id")
        public String sale_id;

        @SerializedName("sale_status")
        public String sale_status;

        @SerializedName("sale_time")
        public String sale_time;

        @SerializedName("user_id")
        public String user_id;


    }
}
