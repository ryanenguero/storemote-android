package com.storemote.teafarmscafe

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.Image
import android.net.Uri
import android.nfc.Tag
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.telecom.Call
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button

import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import bolts.Task
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.signin.SignIn
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_JSON.ForgotPassword
import com.storemote.teafarmscafe.Model_JSON.UserData
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.UserDataDB
import io.realm.Realm
import kotlinx.android.synthetic.main.login_view.*
import org.json.JSONObject
import org.w3c.dom.Text
import retrofit2.Response
import java.util.*

class LoginView : Fragment() {

    private var listener: LoginView.OnFragmentInteractionListener? = null
    //.OnFragmentInteractionListener? = null

    private lateinit var apiInterface: APIInterface
    private lateinit var realm: Realm

    val callbackManager = CallbackManager.Factory.create()

//    private final val EMAIL: String = "email"

//    private static final String EMAIL = "email";

    private val m_Text = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

//        val inflate = inflater.inflate(R.layout.weekly_specials, container, false)

        (activity as AppCompatActivity).supportActionBar?.title = "Login"

        val view = inflater.inflate(R.layout.login_view, container, false)
        val logoImg = view.findViewById(R.id.imgLogo) as ImageView
        val loginNowBtn = view.findViewById(R.id.loginBtn) as Button
        val fbBtn = view.findViewById(R.id.loginFBbtn) as Button
//        val googleBtn = view.findViewById(R.id.googleBtn) as Button
        val forgotPass = view.findViewById(R.id.forgotPassBtn) as TextView

        forgotPass.setOnClickListener {

            val builder = AlertDialog.Builder(context!!)

            builder.setTitle("Forgot password")
            builder.setMessage("Please enter your E-mail or Username")

//            final EditText input = new EditText(this);
            val input = EditText(context)

            input.inputType  = InputType.TYPE_CLASS_TEXT

            builder.setView(input);


            builder.setPositiveButton("OK") { dialog, which ->

                val map = HashMap<String, String>()

                val androidId = Settings.Secure.getString((activity as Activity).contentResolver,
                        Settings.Secure.ANDROID_ID)

                map["username_email"] = input.text.toString()
                map["device_token"] = androidId

//            map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
//            map["client_id"] = "1"

                val call3 = apiInterface.forgotPassword(map)
                call3.enqueue(object : retrofit2.Callback<ForgotPassword> {

                    override fun onResponse(call: retrofit2.Call<ForgotPassword>, response: Response<ForgotPassword>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val userData = response.body()

                        val datumList = userData?.status_message

                        val builder1 = AlertDialog.Builder(context!!)

                        builder1.setTitle("Status")
                        builder1.setMessage(datumList)

                        builder1.setPositiveButton("OK") { dialog, which ->


                        }

                        val dialog2: AlertDialog = builder1.create()
                        dialog2.show()


                    }

                    override fun onFailure(call: retrofit2.Call<ForgotPassword>, t: Throwable) {
                        call.cancel()
                    }




                })

            }


            builder.setNegativeButton("Cancel") { dialog, which ->

                //                finish()
            }

            val dialog1: AlertDialog = builder.create()
            dialog1.show()


        }

        val registerBtn = view.findViewById(R.id.registerBtn) as TextView

        registerBtn.setOnClickListener {

//            val register = RegisterView()
            val intentt = Intent(this.context, RegisterView::class.java)
            startActivity(intentt)
        }


        val txtUserName = view.findViewById(R.id.txtUsername) as EditText
        val txtPassword = view.findViewById(R.id.txtPassword) as EditText

        val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

        Realm.init(this.context)
        // Example of a call to a native method

        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        loginNowBtn.setOnClickListener {

            val map = HashMap<String, String>()

            val username = txtUserName.text
            val password = txtPassword.text

            val androidId = Settings.Secure.getString((activity as Activity).contentResolver,
                    Settings.Secure.ANDROID_ID)

            map["muid"] = androidId
            map["username"] = username.toString()
            map["password"] = password.toString()
            map["device_token"] = androidId
            map["client_id"] = preferences.getString("clientId", "0").toString()

//            map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
//            map["client_id"] = "1"

            val call3 = apiInterface.doLoginFromApp(map)
            call3.enqueue(object : retrofit2.Callback<UserData> {

                override fun onResponse(call: retrofit2.Call<UserData>, response: Response<UserData>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                    val userData = response.body()

                    val datumList = userData?.data

                    if (datumList != null) {

                        for (da in 0 until datumList.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");

                            var datum = datumList[da]

                            Log.d("MainActivity", "ResponseDataForLogin =  ${datum.full_name}");

                            val userpreferences = context!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

                            val editor1 = userpreferences.edit()
                            editor1.putString("username", username.toString())
                            editor1.putString("password", password.toString())
                            editor1.commit()

                            realm.executeTransaction { r ->
                                // Add a person.
                                // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.

                                val userData = r.createObject(UserDataDB::class.java)

                                val editor = preferences.edit()
                                editor.putBoolean("isLoggedIn", true)
                                editor.commit()

                                userData.isLogged = true
                                userData.balance = datum.balance
                                userData.createdDtm = datum.created_dtm
                                userData.customerId = datum.customer_id
                                userData.email = datum.email
                                userData.fullname = datum.full_name
                                userData.isDelete = datum.is_delete
                                userData.mobileNo = datum.mobile_no
                                userData.password = datum.password
                                userData.points = datum.points
                                userData.paypalId = datum.paypal_id
                                userData.token = datum.token
                                userData.username = datum.username
                                userData.versionNo = datum.version_no

                                var heroActivity : TabActivity = activity as TabActivity

                                heroActivity.logInNow()


                            }
                        }

                    }
                }

                override fun onFailure(call: retrofit2.Call<UserData>, t: Throwable) {
                    call.cancel()
                }




            })

        }

        fbBtn.setOnClickListener {

            setUpFB()
        }



//        Picasso.with(this.context).load(R.drawable.tea_farms_logo_signature).into(logoImg)
//        logoImg.setImageResource(R.drawable.tea_farms_logo_signature)

//        val callbackManager = CallbackManager.Factory.create()
//        val callbackManager = CallbackManager.Factory.create()
//        var loginBtn: LoginButton = view!!.findViewById(R.id.login_button) as LoginButton
//        loginBtn.setOnClickListener {
//
//            setUpFB()
//        }
//        loginBtn.setReadPermissions(Arrays.asList("public_profile","email"))
//        loginBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
//            override fun onCancel() {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//
//
//            override fun onError(error: FacebookException) {
//                print("error login fb" +error.message)
//            }
//
//            override fun onSuccess(result: LoginResult) {
//                GraphRequest.newMeRequest(result.accessToken, GraphRequest.GraphJSONObjectCallback {
//                    obj: JSONObject, response: GraphResponse ->
//                    println(obj.toString())
//                    println(obj.getString("email"))})
//                println("successful access token" + result.accessToken)
//            }
//        })


//        FacebookSdk.sdkInitialize(this.context)
//
//        var loginBtn: LoginButton = view!!.findViewById(R.id.login_button) as LoginButton
//
//        loginBtn.setReadPermissions(Arrays.asList("public_profile","email"))
//        loginBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
//                //.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"))
//            override fun onSuccess(loginResult: LoginResult) {
//                Log.d("FBLOGIN", loginResult.accessToken.token.toString())
//                Log.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
//                Log.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())
//
//
//                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
//                    try {
//                        //here is the data that you want
//                        Log.d("FBLOGIN_JSON_RES", `object`.toString())
//
//                        if (`object`.has("id")) {
//
//                            Log.e("FBLOGIN_SUCC", `object`.toString())
//                            handleFacebookSignInResult(`object`)
////                            handleSignInResultFacebook(`object`)
//                        } else {
//                            Log.e("FBLOGIN_FAILD", `object`.toString())
//                        }
//
//                    } catch (e: Exception) {
//                        e.printStackTrace()
////                        dismissDialogLogin()
//                    }
//                }
//
//                val parameters = Bundle()
//                parameters.putString("fields", "name,email,id,picture.type(large)")
//                request.parameters = parameters
//                request.executeAsync()
//
//            }
//
//            override fun onCancel() {
//                Log.e("FBLOGIN_FAILD", "Cancel")
//            }
//
//            override fun onError(error: FacebookException) {
//                Log.e("FBLOGIN_FAILD", "ERROR", error)
//            }
//        })




//        view!!.findViewById(R.id.sign_in_button).setOnClickListener(this)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(this.context!!, gso)
//        setcli(this.context, gso);

        val Button = view!!.findViewById(R.id.sign_in_button) as Button
//        val signInButton = view!!.findViewById(R.id.sign_in_button) as SignInButton
//        signInButton.setSize(SignInButton.SIZE_STANDARD)
//        signInButton.setOnClickListener {
        Button.setOnClickListener {
                        val signInIntent = mGoogleSignInClient.getSignInIntent()

///            startActivity(signInIntent)
            startActivityForResult(signInIntent, 1)



        }




        return view

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)



        FacebookSdk.sdkInitialize(this.context)
//        setUpFB()

    }

    fun setUpFB() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("FBLOGIN", loginResult.accessToken.token.toString())
                Log.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
                Log.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())


                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    try {
                        //here is the data that you want
                        Log.d("FBLOGIN_JSON_RES", `object`.toString())

                        if (`object`.has("id")) {

                            Log.e("FBLOGIN_SUCC", `object`.toString())
                            handleFacebookSignInResult(`object`)
//                            handleSignInResultFacebook(`object`)
                        } else {
                            Log.e("FBLOGIN_FAILD", `object`.toString())
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
//                        dismissDialogLogin()
                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "name,email,id,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

            }

            override fun onCancel() {
                Log.e("FBLOGIN_FAILD", "Cancel")
            }

            override fun onError(error: FacebookException) {
                Log.e("FBLOGIN_FAILD", "ERROR", error)
            }
        })

    }

    override fun onStart() {
        super.onStart()

        val account = GoogleSignIn.getLastSignedInAccount(this.context!!)

//        updateUI(account)


//updateUI(account);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent ) {

        super.onActivityResult(requestCode, resultCode, data)

        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.

            val task = GoogleSignIn.getSignedInAccountFromIntent(data) as Task<GoogleSignInAccount>

            handleSignInResult(task)
        }
    }

    fun handleFacebookSignInResult(jsonObject: JSONObject){


        val map = HashMap<String, String>()

//        val fbData = jsonObject["name"]


//        let params: [String:Any]? = [
//        "muid" : UIDevice.current.identifierForVendor!.uuidString,
//        "username" : dict.object(forKey: "id")!,
//        "full_name" : dict.object(forKey: "name")!,
//        "mobile_no" : "",
//        "email" :  email,
//        "client_id" :  UserDefaults.standard.object(forKey: "clientID")!,
//        "is_fb" : 1,
//        "is_google" : 0,
//        "device_token" : appDelegate.deviceToken,
//        ]

        val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

        val androidId = Settings.Secure.getString((activity as Activity).contentResolver,
                Settings.Secure.ANDROID_ID)

        map["muid"] = androidId
        map["username"] = jsonObject["id"].toString()
        map["full_name"] = jsonObject["name"].toString()
        map["mobile_no"] = ""
        map["email"] = jsonObject["email"].toString()
        map["client_id"] = preferences.getString("clientId", "0").toString()
        map["is_fb"] = "1"
        map["is_google"] = "0"
        map["device_token"] = androidId


        val call3 = apiInterface.doLoginFromFB(map)
        call3.enqueue(object : retrofit2.Callback<UserData> {

            override fun onResponse(call: retrofit2.Call<UserData>, response: Response<UserData>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                val userData = response.body()

                val datumList = userData?.data

                if (datumList != null) {

                    for (da in 0 until datumList.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");

                        var datum = datumList[da]

                        Log.d("MainActivity", "ResponseDataForLogin =  ${datum.full_name}");

                        realm.executeTransaction { r ->
                            // Add a person.
                            // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.

                            val userData = r.createObject(UserDataDB::class.java)

                            val editor = preferences.edit()
                            editor.putBoolean("isLoggedIn", true)
                            editor.putBoolean("isFB", true)
                            editor.commit()

                            userData.isLogged = true
                            userData.balance = datum.balance
                            userData.createdDtm = datum.created_dtm
                            userData.customerId = datum.customer_id
                            userData.email = datum.email
                            userData.fullname = datum.full_name
                            userData.isDelete = datum.is_delete
                            userData.mobileNo = datum.mobile_no
                            userData.password = datum.password
                            userData.points = datum.points
                            userData.paypalId = datum.paypal_id
                            userData.token = datum.token
                            userData.username = datum.username
                            userData.versionNo = datum.version_no

                            var heroActivity : TabActivity = activity as TabActivity

                            heroActivity.logInNow()

                        }
                    }

                }
            }

            override fun onFailure(call: retrofit2.Call<UserData>, t: Throwable) {
                call.cancel()
            }




        })

    }

fun handleSignInResult(completedTask: Task<GoogleSignInAccount> ) {
    try {
        val account = completedTask.result
                //.getResult(ApiException)

        Log.d("MainActivity", "Google Sign In result =  $account");

        // Signed in successfully, show authenticated UI.
//        update(account);
    } catch (e: ApiException) {
        // The ApiException status code indicates the detailed failure reason.
//        // Please refer to the GoogleSignInStatusCodes class reference for more information.
//        Log.w(Tag, "signInResult:failed code=" + e
//                .getStatusCode());
//        updateUI(null);
    }
}

    fun onButtonPressed(uri: Uri) {

            listener?.onFragmentInteraction(uri)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        fun newInstance() : Fragment
        {

            var fb = LoginView()
            return fb


        }
    }


}