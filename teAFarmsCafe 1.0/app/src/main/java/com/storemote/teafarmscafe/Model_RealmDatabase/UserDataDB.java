package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class UserDataDB extends RealmObject {

    public String balance;
    public String created_dtm;
    public String customer_id;
    public String email;
    public String full_name;
    public Boolean is_delete;
    public Boolean is_logged;
    public String mobile_no;
    public String password;
    public String paypal_id;
    public String points;
    public String token;
    public String username;
    public String version_no;

    public void setBalance(String balance) {

        this.balance = balance;
    }

    public String getBalance() {

        return balance;
    }

    public void setCreatedDtm(String created_dtm) {

        this.created_dtm = created_dtm;
    }

    public String getCreatedDtm() {

        return created_dtm;
    }

    public void setCustomerId(String customer_id) {

        this.customer_id = customer_id;
    }

    public String getCustomerId() {

        return customer_id;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getEmail() {

        return email;
    }

    public void setFullname(String full_name) {

        this.full_name = full_name;
    }

    public String getFullname() {

        return full_name;
    }

    public void setIsDelete(Boolean is_delete) {

        this.is_delete = is_delete;
    }

    public Boolean getIsDelete() {

        return is_delete;
    }

    public void setIsLogged(Boolean is_logged) {

        this.is_logged = is_logged;
    }

    public Boolean getIsLogged() {

        return is_logged;
    }

    public void setMobileNo(String mobile_no) {

        this.mobile_no = mobile_no;
    }

    public String getMobileNo() {

        return mobile_no;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getPassword() {

        return password;
    }

    public void setPaypalId(String paypal_id) {

        this.paypal_id = paypal_id;
    }

    public String getPaypalId() {

        return paypal_id;
    }

    public void setPoints(String points) {

        this.points = points;
    }

    public String getPoints() {

        return points;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getToken() {

        return token;
    }

    public void setUsername(String username ) {

        this.username = username;
    }

    public String getUsername() {

        return username;
    }

    public void setVersionNo(String version_no) {

        this.version_no = version_no;
    }

    public String getVersionNo() {

        return version_no;
    }
}
