package com.storemote.teafarmscafe.Model_RealmDatabase;

import java.util.Date;

import io.realm.RealmObject;

public class PromoListDB extends RealmObject {

    private Date from_date;
    private String name;
    private String promo_banner;
    private Integer promo_id;
    private Date to_date;

    public void setFromDate(Date from_date) {

        this.from_date = from_date;
    }

    public Date getFromDate() {

        return from_date;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setPromoBanner(String promo_banner) {

        this.promo_banner = promo_banner;
    }

    public String getPromoBanner() {

        return promo_banner;
    }

    public void setPromoId(Integer promo_id) {

        this.promo_id = promo_id;
    }

    public Integer getPromoId() {

        return promo_id;
    }

    public void setToDate(Date to_date) {

        this.to_date = to_date;
    }

    public Date getToDate() {

        return to_date;
    }
}
