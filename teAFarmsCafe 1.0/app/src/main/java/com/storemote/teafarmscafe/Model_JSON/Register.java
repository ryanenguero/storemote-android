package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Register {

    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {

        @SerializedName("balance")
        public String balance;
        @SerializedName("customer_id")
        public String customer_id;
        @SerializedName("email")
        public String email;
        @SerializedName("full_name")
        public String full_name;
        @SerializedName("mobile_no")
        public String mobile_no;
        @SerializedName("password")
        public String password;
        @SerializedName("paypal_id")
        public String paypal_id;
        @SerializedName("points")
        public String points;
        @SerializedName("token")
        public String token;
        @SerializedName("username")
        public String username;
        @SerializedName("client_id")
        public String client_id;

    }
}
