package com.storemote.teafarmscafe.PromoViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.PromoListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PromosDB
import com.storemote.teafarmscafe.Model_RealmDatabase.Rewards
import com.storemote.teafarmscafe.R

class PromoViewAdapter (private val context: Context,
                        private val dataSource: ArrayList<PromoListDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.promo_list_adapter, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.promos_caption) as TextView

        val thumbnailImageView = rowView.findViewById(R.id.promo_page_image) as ImageView

        val promos = getItem(position) as PromoListDB

        nameTextView.text = promos.name
//        addressTextView.text = weekly.menuProd.unitPrice as String
//        hoursTextView.text = "8am - 10pm"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${promos.promoBanner}").into(thumbnailImageView)

        return rowView
    }
}