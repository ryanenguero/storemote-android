package com.storemote.teafarmscafe

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import android.graphics.Movie
import android.os.Parcelable
import android.support.v4.view.MenuItemCompat
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_JSON.WeeklySpecialsExceptionParser
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductDB
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.landing_page.*


class ProductDetailsView : AppCompatActivity() {

    var productDetails: Any? = null

    var textCartItemCount: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        this.set
//        setHasOptionsMenu(true)

        supportActionBar!!.title = intent.getStringExtra("name")
        setContentView(R.layout.product_details_view)

        val imageDetails = findViewById(R.id.imageDetails) as ImageView
        val nameDetails = findViewById(R.id.textDetailsName) as TextView
        val priceDetails = findViewById(R.id.textDetailsPrice) as TextView
//        val descriptions = findViewById(R.id.) as TextView

        val addToOrderBtn = findViewById(R.id.addToOrderButton) as Button


        nameDetails.text = intent.getStringExtra("name")
        priceDetails.text = "₱" + intent.getStringExtra("price") + "\n\n" + intent.getStringExtra("description")

//
        Picasso.with(this).load("http://storemote.net/assets/uploads/item_pics/${intent.getStringExtra("image")}").transform(CropCircleTransformation())
                .into(imageDetails)


        addToOrderBtn.setOnClickListener {

            Log.d("MainActivity", "Add to order");


            val intentt = Intent(this, AddToOrderView::class.java)


            intentt.putExtra("name", intent.getStringExtra("name"))
            intentt.putExtra("description", intent.getStringExtra("description"))
            intentt.putExtra("price", intent.getStringExtra("price"))
            intentt.putExtra("image", intent.getStringExtra("image"))
            intentt.putExtra("category", intent.getStringExtra("category"))
            intentt.putExtra("clientId", intent.getStringExtra("clientId"))
            intentt.putExtra("costPrice", intent.getStringExtra("costPrice"))
            intentt.putExtra("createdBy", intent.getStringExtra("createdBy"))
            intentt.putExtra("createdDate", intent.getStringExtra("createdDate"))
            intentt.putExtra("itemId", intent.getStringExtra("itemId"))
            intentt.putExtra("itemNumber", intent.getStringExtra("itemNumber"))
            intentt.putExtra("itemType", intent.getStringExtra("itemType"))
            intentt.putExtra("receivingQuantity", intent.getStringExtra("receivingQuantity"))
            intentt.putExtra("reorderLevel", intent.getStringExtra("reorderLevel"))
            intentt.putExtra("stockType", intent.getStringExtra("stockType"))
            intentt.putExtra("storeId", intent.getStringExtra("storeId"))
            intentt.putExtra("supplierId", intent.getStringExtra("supplierId"))
            intentt.putExtra("taxCategoryId", intent.getStringExtra("taxCategoryId"))
            intentt.putExtra("updatedBy", intent.getStringExtra("updatedBy"))
            intentt.putExtra("updatedDate", intent.getStringExtra("updatedDate"))
            intentt.putExtra("versionNo", intent.getStringExtra("versionNo"))
            intentt.putExtra("is_promo", intent.getStringExtra("is_promo"))
            intentt.putExtra("is_rewards", intent.getStringExtra("is_rewards"))

//            intentt.putExtra("image", selectedSpecials.menuProd.picFilename)


            startActivity(intentt)


        }
    }

}
