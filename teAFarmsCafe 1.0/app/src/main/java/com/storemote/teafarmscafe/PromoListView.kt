package com.storemote.teafarmscafe

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import com.storemote.teafarmscafe.MenuViewAdapter.MenuViewAdapter
import com.storemote.teafarmscafe.Model_JSON.StoreListData
import com.storemote.teafarmscafe.Model_RealmDatabase.CategoryDB
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductExceptionDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PromosDB
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import com.storemote.teafarmscafe.StoreListAdapter.StoreListAdapt
import io.realm.Realm

class PromoListView : AppCompatActivity() {

    private lateinit var listview: ListView
    private var arrList = ArrayList<Any>()
    private lateinit var realm: Realm
    private lateinit var adapt: PromoDetailsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.promo_details_list)

        supportActionBar?.title = intent.getStringExtra("titleProms")

        listview = findViewById<ListView>(R.id.promo_details_list_view)

        Realm.init(this)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        val menuList = ArrayList<MenuProductExceptionDB>(realm.where(MenuProductExceptionDB::class.java).findAll())
        val categoryList = ArrayList<CategoryDB>(realm.where(CategoryDB::class.java).findAll())
        val promosList = ArrayList<PromosDB>(realm.where(PromosDB::class.java).findAll())
//        val arr = ArrayList<WeeklySpecials>(arrList)

        for (da in 0 until categoryList.size) {

            val map = HashMap<String, Any>()
            var arrMenuList = ArrayList<MenuProductExceptionDB>()

            for (dat in 0  until menuList.size) {
                if (menuList[dat].menuProd.category == categoryList[da].name){

                    for (d in 0  until promosList.size) {
                        if (promosList[d].itemName.toString() == menuList[dat].menuProd.name){

                            arrMenuList.add(menuList[dat])
                        }
                    }

//                    arrMenuList.add(menuList[dat])

                }


            }

            if (arrMenuList.size != 0) {
                map.set("header", categoryList[da].name)
                map.set("data", arrMenuList)


                arrList.add(map)



                adapt = PromoDetailsListAdapter(this)
                adapt.promosData = promosList

//        listview.adapter = adapt


                for (da in 0 until arrList.size) {

                    val dict = arrList[da] as HashMap<String, Any>

                    val catName = dict["header"] as String
                    adapt!!.addSectionHeaderItem(catName, false)

//                adapt.addItem(menuList[0])

                    val men = dict["data"] as ArrayList<MenuProductExceptionDB>



                    for (dat in 0 until men.size) {
                        adapt!!.addItem(men[dat], false)

                        Log.d("MainActivity", "menmen =  ${men[dat].menuProd.name}");
                    }

                }
            }

        }

        Log.d("MainActivity", "promodetailslist =  $arrList");



        if (arrList.count() > 0) {
            listview.adapter = adapt
//        }

            listview.setOnItemClickListener { _, _, position, _ ->

                val selectedMenu = adapt!!.mData[position] as MenuProductExceptionDB
                val intentt = Intent(this, ProductDetailsView::class.java)

                val promosListd = ArrayList<PromosDB>(realm.where(PromosDB::class.java).findAll())
//
                for (da in 0 until promosListd.size) {
                    if (promosListd[da].itemName == selectedMenu.menuProd.name) {

                        val promo = promosListd[da]

                        intentt.putExtra("is_promo", "1")
                        intentt.putExtra("is_rewards", "0")
                        intentt.putExtra("name", promo.itemName)
                        intentt.putExtra("description", promo.discountPercent.toString() + "% off \n\n" + selectedMenu.menuProd.description)
                        intentt.putExtra("image", selectedMenu.menuProd.picFilename)
                        intentt.putExtra("category", selectedMenu.menuProd.category)
                        intentt.putExtra("clientId", selectedMenu.menuProd.client_id)
                        intentt.putExtra("costPrice", selectedMenu.menuProd.costPrice)
                        intentt.putExtra("createdBy", selectedMenu.menuProd.createdBy)
                        intentt.putExtra("createdDate", selectedMenu.menuProd.createdDate)
                        intentt.putExtra("itemId", selectedMenu.menuProd.itemId)
                        intentt.putExtra("itemNumber", selectedMenu.menuProd.itemNumber)
                        intentt.putExtra("itemType", selectedMenu.menuProd.itemType)
                        intentt.putExtra("receivingQuantity", selectedMenu.menuProd.receivingQuantity)
                        intentt.putExtra("reorderLevel", selectedMenu.menuProd.reorderLevel)
                        intentt.putExtra("stockType", selectedMenu.menuProd.stockType)
                        intentt.putExtra("storeId", selectedMenu.menuProd.storeId)
                        intentt.putExtra("supplierId", selectedMenu.menuProd.supplierId)
                        intentt.putExtra("taxCategoryId", selectedMenu.menuProd.taxCategoryId)
                        intentt.putExtra("updatedBy", selectedMenu.menuProd.updatedBy)
                        intentt.putExtra("updatedDate", selectedMenu.menuProd.updatedDate)
                        intentt.putExtra("versionNo", selectedMenu.menuProd.versionNo)




                        if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice == 0.0f) {

                            val price = (selectedMenu.menuProd.unitPrice.toDouble() - (selectedMenu.menuProd.unitPrice.toDouble() * promo.discountPercent / 100)).toString()

                            intentt.putExtra("price", price)


                        } else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f) {

                            intentt.putExtra("price", promo.specificPrice.toString())

                        } else if (promo.discountPercent == 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f) {

                            intentt.putExtra("price", promo.specificPrice.toString())

                        } else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f) {

                            val price = (selectedMenu.menuProd.unitPrice.toDouble() - (selectedMenu.menuProd.unitPrice.toDouble() * promo.discountPercent / 100)).toString()

                            intentt.putExtra("price", price)

                        } else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f) {

                            intentt.putExtra("price", promo.specificPrice.toString())

                        } else if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f) {

                            val price = (promo.specificPrice - (promo.specificPrice * promo.discountPercent / 100)).toString()

                            intentt.putExtra("price", price)

                        } else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f) {


                            val price = (promo.specificPrice - (promo.specificPrice * promo.discountPercent / 100)).toString()

                            intentt.putExtra("price", price)

                        }


                    }

                }


                //putExtra("data", selectedSpecials.menuProd)

                startActivity(intentt)

                Log.d("MainActivity", "SelectedMenu =  $selectedMenu");


            }

        }


    }



    private fun makeAdapter(list: List<String>): ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
    /**
     *
     *
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

}