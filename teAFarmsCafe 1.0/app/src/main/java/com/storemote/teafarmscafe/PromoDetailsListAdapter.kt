package com.storemote.teafarmscafe

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.MenuViewAdapter.MenuViewAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductExceptionDB
import com.storemote.teafarmscafe.Model_RealmDatabase.PromosDB
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import java.util.*

class PromoDetailsListAdapter (private val context: Context) : BaseAdapter() {
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//     }
//
//    override fun getItem(position: Int): Any {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun getItemId(position: Int): Long {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun getCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }

    private val TYPE_ITEM = 0
    private val TYPE_SEPARATOR = 1

    public var mData = ArrayList<Any>()
    var promosData = ArrayList<PromosDB>()
    private val sectionHeader = TreeSet<Int>()
    private val filteredData = ArrayList<Any>()
    private val filt = ArrayList<MenuProductExceptionDB>()
    private var isItFiltered : Boolean? = null

//    private var mInflater: LayoutInflater? = null

//    fun MenuViewAdapter(context: Context) {
//        mInflater = context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//    }

    private val mInflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    fun addItem(item: MenuProductExceptionDB, isFiltered: Boolean) {
        if (isFiltered == true){
//            filteredData.removeAll(filteredData)
            filteredData.add(item)
            isItFiltered = true
        }else {
//            filteredData.removeAll(filteredData)
            mData.add(item)
            isItFiltered = false
        }
        notifyDataSetChanged()
    }

    fun addSectionHeaderItem(item: String, isFiltered: Boolean) {

        if (isFiltered == true){
//            filteredData.removeAll(filteredData)
            filteredData.add(item)
            sectionHeader.add(filteredData.size - 1)
            isItFiltered = true
        }else {
//            filteredData.removeAll(filteredData)
            mData.add(item)
            sectionHeader.add(mData.size - 1)
            isItFiltered = false
        }


        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (sectionHeader.contains(position)) TYPE_SEPARATOR else TYPE_ITEM
    }

    override fun getViewTypeCount(): Int {
        return 2
    }

    override fun getCount(): Int {
        if (isItFiltered == true){
            return filteredData.size
        }else {
            return mData.size
        }
    }

    override fun getItem(position: Int): Any {
        if (isItFiltered == true){
            return filteredData[position]
        }else {
            return mData[position]
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        var holder: ViewHolder? = null
        val rowType = getItemViewType(position)

//        Log.d("MainActivity", "ResponseAdapter =  $mData");

        if (convertView == null) {
            holder = ViewHolder()
            when (rowType) {
                TYPE_ITEM -> {
                    convertView = mInflater!!.inflate(R.layout.menu_list_row, null)
                    holder.textView = convertView!!.findViewById<View>(R.id.textrow) as TextView
                    holder.priceView = convertView!!.findViewById<View>(R.id.pricerow) as TextView
                    holder.imageView = convertView!!.findViewById<View>(R.id.imagerow) as ImageView

                }
                TYPE_SEPARATOR -> {
                    convertView = mInflater!!.inflate(R.layout.menu_list_header, null)
                    holder.textView = convertView!!.findViewById<View>(R.id.textSeparator) as TextView


                }
            }

//            if ((mData[position] as String) != true)
            convertView!!.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        if (isItFiltered == true){

            if (filteredData[position] is String) {

                holder.textView!!.text = filteredData[position] as String


            } else {

//                val promosList = ArrayList<PromosDB>(realm.where(PromosDB::class.java).findAll())

                val d = filteredData[position] as MenuProductExceptionDB

                for (da in 0 until promosData.size) {

                    if (promosData[da].itemName == d.menuProd.name){

                        val promo = promosData[da]


                        holder.textView!!.text = promosData[da].itemName

                        if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice == 0.0f){

                           val price = (d.menuProd.unitPrice.toInt()-(d.menuProd.unitPrice.toInt()*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f){

                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f){


                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f){

                            val price = (d.menuProd.unitPrice.toInt()-(d.menuProd.unitPrice.toInt()*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f){

                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f){

                            val price = (promo.specificPrice-(promo.specificPrice*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f){


                            val price = (promo.specificPrice-(promo.specificPrice*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }




                    }

                }




//            Picasso.get().load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}").into(holder.imageView)

//            Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")

                Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${d}")
                        .transform(CropCircleTransformation())
                        .into(holder.imageView)
            }

        }else {

            if (mData[position] is String) {

                holder.textView!!.text = mData[position] as String


            } else {


                val d = mData[position] as MenuProductExceptionDB

                for (da in 0 until promosData.size) {

                    if (promosData[da].itemName == d.menuProd.name){

//                        holder.textView!!.text = promosData[da].itemName
//                        holder.priceView!!.text = "₱" + promosData[da].specificPrice

                        val promo = promosData[da]


                        holder.textView!!.text = promosData[da].itemName

                        if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice == 0.0f){

                            val price = (d.menuProd.unitPrice.toDouble()-(d.menuProd.unitPrice.toDouble()*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f){

                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f){


                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice == 0.0f){

                            val price = (d.menuProd.unitPrice.toDouble()-(d.menuProd.unitPrice.toDouble()*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent == 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f){

                            holder.priceView!!.text = "₱" + promo.specificPrice.toString()

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems == 0.toDouble() && promo.specificPrice != 0.0f){

                            val price = (promo.specificPrice-(promo.specificPrice*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }else if (promo.discountPercent != 0.0f && promo.nooffreeitems != 0.toDouble() && promo.specificPrice != 0.0f){


                            val price = (promo.specificPrice-(promo.specificPrice*promo.discountPercent/100)).toString()

                            holder.priceView!!.text = "₱" + price

                        }

                    }

                }

//                holder.textView!!.text = d.menuProd.name
//                holder.priceView!!.text = "₱" + d.menuProd.unitPrice


//            Picasso.get().load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}").into(holder.imageView)

//            Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")

                Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${d.menuProd.picFilename}")
                        .transform(CropCircleTransformation())
                        .into(holder.imageView)
            }
        }



        return convertView
    }

    // Filter Class
    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())

        Log.d("MainActivity", "search menu menubbb =  $mData");

        if (charText.length == 0) {

            isItFiltered = false

//            filteredData.removeAll(filteredData)
//            for (da in 0 until mData.size) {
//                if (mData[da] is String){
//
//                    val header = mData[da] as String
//
//                    addSectionHeaderItem(header, false)
//
//                }else{
//
//                    val prod = mData[da] as MenuProductExceptionDB
//                    addItem(prod, false)
//
//                }
//            }
//            MenuView.newInstance()
        } else {

            val dat = ArrayList<Any>()

            for (da in 0 until mData.size) {


                if (mData[da] is String){
                    val header = mData[da] as String

                    addSectionHeaderItem(header, true)
                }else {

                    val prod = mData[da] as MenuProductExceptionDB

                    if (prod.menuProd.name.toString().toLowerCase().contains(charText)){

                        addItem(prod, true)
                    }

                }
            }

        }

        Log.d("MainActivity", "search menu =  $mData");
        Log.d("MainActivity", "search menu filtered =  $filteredData");


        notifyDataSetChanged()
    }

    class ViewHolder {
        var textView: TextView? = null
        var imageView: ImageView? = null
        var priceView: TextView? = null
    }


}


