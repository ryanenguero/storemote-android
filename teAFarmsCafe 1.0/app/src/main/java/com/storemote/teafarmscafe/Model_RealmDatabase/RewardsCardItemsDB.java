package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class RewardsCardItemsDB extends RealmObject {

    private String item_id;
    private String name;
    private String description;
    private String pic_filename;
    private String store_id;

    public void setItemId(String item_id) {

        this.item_id = item_id;
    }

    public String getItemId() {

        return item_id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public String getDescription() {

        return description;
    }


    public void setPicFilename(String pic_filename) {

        this.pic_filename = pic_filename;
    }

    public String getPicFilename() {

        return pic_filename;
    }

    public void setStoreId(String store_id) {

        this.store_id = store_id;
    }

    public String getStoreId() {

        return store_id;
    }

}
