package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RewardsCard {

    @SerializedName("data")
    public List<RewardsCardData> data = new ArrayList<>();

    public class RewardsCardData {

        @SerializedName("card_points")
        public Integer card_points;

        @SerializedName("total_stamp")
        public Integer points_to_collect;

        @SerializedName("reward_card_id")
        public Integer reward_card_id;

    }
}
