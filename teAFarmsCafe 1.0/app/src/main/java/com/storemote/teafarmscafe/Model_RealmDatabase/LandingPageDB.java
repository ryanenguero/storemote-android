package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class LandingPageDB extends RealmObject {

    private String client_id;
    private String menu;
    private String promo;
    private String rewards;
    private String store_id;
    private String weekly_special;

    public String getClientId() {

        return client_id;
    }


    public void setClientId(String clientId) {

        this.client_id = clientId;
    }

    public String getMenu() {

        return menu;
    }


    public void setMenu(String menu) {

        this.menu = menu;
    }

    public String getPromo() {

        return promo;
    }


    public void setPromo(String promo) {

        this.promo = promo;
    }

    public String getRewards() {

        return rewards;
    }


    public void setRewards(String rewards) {

        this.rewards = rewards;
    }

    public String getStoreId() {

        return store_id;
    }


    public void setStoreId(String storeId) {

        this.store_id = storeId;
    }

    public String getWeeklySpecial() {

        return weekly_special;
    }


    public void setWeeklySpecial(String weeklySpecial) {

        this.weekly_special = weeklySpecial;
    }

}


