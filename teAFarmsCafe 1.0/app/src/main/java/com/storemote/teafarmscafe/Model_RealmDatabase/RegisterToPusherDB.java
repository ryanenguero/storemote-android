package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class RegisterToPusherDB extends RealmObject {

    public String channel;
    private String event;
    private String key;

    public void setChannel(String channel) {

        this.channel = channel;
    }

    public String getChannel() {

        return channel;
    }

    public void setEvent(String event) {

        this.event = event;
    }

    public String getEvent() {

        return event;
    }

    public void setKey(String key) {

        this.key = key;
    }

    public String getKey() {

        return key;
    }

}
