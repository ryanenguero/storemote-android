package com.storemote.teafarmscafe

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.storemote.teafarmscafe.DownloaderClass

import android.util.Log
import android.widget.*

//import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.view.*
import com.storemote.teafarmscafe.Model_JSON.LandingPage
import com.storemote.teafarmscafe.Model_JSON.RewardsDetails
import com.storemote.teafarmscafe.Model_RealmDatabase.*
import com.storemote.teafarmscafe.RewardsViewAdapter.RewardsViewAdapter
import com.storemote.teafarmscafe.WeeklySpecialsAdapter.WeeklySpecialsViewAdapter
//import com.storemote.teafarmscafe.APIInterface

//import android.util.Log
//import java.util.ArrayList

import io.realm.Realm
import io.realm.RealmResults
import java.util.*
import kotlin.collections.ArrayList

class RewardsView : Fragment() {

        private val PREFS_TASKS = "prefs_tasks"
        private val KEY_TASKS_LIST = "tasks_list"

        private lateinit var realm: Realm
        private lateinit var rewards: RealmResults<Rewards>

        private lateinit var listview: ListView

        private lateinit var listItems: ArrayList<String>
        private var listener: RewardsView.OnFragmentInteractionListener? = null

        var textCartItemCount: TextView? = null
        var mCartItemCount = 10


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//            super.onCreate(savedInstanceState)
//            setContentView(R.layout.rewards_list)
//
//
//            listview = findViewById<ListView>(R.id.rewardsListView)


            (activity as AppCompatActivity).supportActionBar?.title = "Rewards"


            setHasOptionsMenu(true)

            val view = inflater.inflate(R.layout.rewards_list, container, false)

            listview  = view.findViewById(R.id.rewardsListView)

            Realm.init(this.context!!)
            // Create the Realm instance
            realm = Realm.getDefaultInstance()

            // Asynchronous queries are evaluated on a background thread,
            // and passed to the registered change listener when it's done.
            // The change listener is also called on any future writes that change the result set.
            rewards = realm.where(Rewards::class.java).findAllAsync()



            val arrList = ArrayList<Rewards>(realm.where(Rewards::class.java).findAll())

//        val arr = ArrayList<WeeklySpecials>(arrList)

            val adapt = RewardsViewAdapter(this.context!!, arrList)
            listview.adapter = adapt



            listview.setOnItemClickListener { _, _, position, _ ->

//                val selectedMenu = adapt!!.mData[position] as MenuProductExceptionDB

                val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                val userPreferences = context!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

//                DownloaderClass().downloadRewardsDetails(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), context!!, realm, arrList[position].rewardId)

                realm.beginTransaction()
                realm.delete(RewardsDetailsDB::class.java)
                realm.commitTransaction()



               val apiInterface = APIClient.getClient().create(APIInterface::class.java)

                val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                map["client_id"] = preferences.getString("clientId", "0").toString()
                map["store_id"] = preferences.getString("storeId", "0").toString()
                map["reward_id"] = arrList[position].rewardId

                val call3 = apiInterface.doDownloadRewardsDetails(map)
                call3.enqueue(
                        object : Callback<RewardsDetails> {



                            override fun onResponse(call: Call<RewardsDetails>, response: Response<RewardsDetails>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                                val rewardsDetailData = response.body()

                                val data = rewardsDetailData!!.data

                                if (data != null) {

                                    for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


//                                        Log.d("MainActivity", "ResponseRewardDetails =  ${data[da]!!.item_id}");

                                        realm.executeTransaction { r ->
                                            // Add a person.
                                            // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                            val rewardsDTB = r.createObject(RewardsDetailsDB::class.java)

                                            rewardsDTB.discountPercent = data[da].discount_percent
                                            rewardsDTB.nooffreeitems = data[da].no_of_free_items
                                            rewardsDTB.rewardId = data[da].reward_id
                                            rewardsDTB.specificPrice = data[da].specific_price
//                                            rewardsDTB.availableQuantity = data[da].available_quantity
                                            rewardsDTB.itemId = data[da].item_id


                                        }
                                    }

                                    val intentt = Intent(context!!, RewardsListView::class.java)

                                    intentt.putExtra("titleRewards", arrList[position].name)
                                    intentt.putExtra("rewards_details_id", arrList[position].rewardId)

                                    startActivity(intentt)

                                }
                            }

                            override fun onFailure(call: Call<RewardsDetails>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                call3.cancel()
                            }

                        })
//


            }

//            NotificationCenter.defaultCenter().addFucntionForNotification("loadRewardDetails") {
//
//
//
//            }



            return view


        }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)

//        inflater!!.inflate(R.menu.cart_menu, menu);


        inflater!!.inflate(R.menu.cart_menu, menu)

        val menuItem = menu!!.findItem(R.id.cart_btn)

        val actionView = MenuItemCompat.getActionView(menuItem)
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView

//        setupBadge()

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size


        textCartItemCount!!.text = mCartItemCount.toString()

        actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItem)
            }
        })

    }

    override fun onStart() {
        super.onStart()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }

    override fun onResume() {
        super.onResume()

        (activity as AppCompatActivity).invalidateOptionsMenu()

        setHasOptionsMenu(true)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

//        val id = item?.getItemId()
//
//        //noinspection SimplifiableIfStatement
//        return if (id == R.id.cart_btn) {
//            true
//        } else super.onOptionsItemSelected(item)

        when (item?.getItemId()) {

            R.id.cart_btn -> {
                // Do something

                val intentt = Intent(this.context, OrderListView::class.java)

                startActivity(intentt)

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupBadge() {

//        if (textCartItemCount != null) {
//            if (mCartItemCount === 0) {
//                if (textCartItemCount!!.getVisibility() !== View.GONE) {
//                    textCartItemCount!!.setVisibility(View.GONE)
//                }
//            } else {
//                textCartItemCount!!.text = (mCartItemCount) as String
//
//                        //.setText(String.valueOf(Math.min(mCartItemCount, 99)))
//                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
//                    textCartItemCount!!.setVisibility(View.VISIBLE)
//                }
//            }
//        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        fun newInstance() : Fragment
        {

            var fb = RewardsView()
            return fb

        }
    }



}