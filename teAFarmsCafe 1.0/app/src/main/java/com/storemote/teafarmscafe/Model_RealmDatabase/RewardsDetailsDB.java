package com.storemote.teafarmscafe.Model_RealmDatabase;

import java.util.Date;

import io.realm.RealmObject;

public class RewardsDetailsDB extends RealmObject {

    private Double available_quantity;
    private Float discount_percent;
    private Date from_date;
    private Integer item_id;
    private Double no_of_free_items;
    private Integer reward_id;
    private Float specific_price;
    private Date to_date;

    public void setAvailableQuantity(Double available_quantity) {

        this.available_quantity = available_quantity;
    }

    public Double getAvailableQuantity() {

        return available_quantity;
    }

    public void setDiscountPercent(Float discount_percent) {

        this.discount_percent = discount_percent;
    }

    public Float getDiscountPercent() {

        return discount_percent;
    }

    public void setFromDate(Date from_date) {

        this.from_date = from_date;
    }

    public Date getFromDate() {

        return from_date;
    }

    public void setItemId(Integer item_id) {

        this.item_id = item_id;
    }

    public Integer getItemId() {

        return item_id;
    }

    public void setNooffreeitems(Double no_of_free_items) {

        this.no_of_free_items = no_of_free_items;
    }

    public Double getNooffreeitems() {

        return no_of_free_items;
    }

    public void setRewardId(Integer reward_id) {

        this.reward_id = reward_id;
    }

    public Integer getRewardId() {

        return reward_id;
    }

    public void setSpecificPrice(Float specific_price) {

        this.specific_price = specific_price;
    }

    public Float getSpecificPrice() {

        return specific_price;
    }

    public void setToDate(Date to_date) {

        this.to_date = to_date;
    }

    public Date getToDate() {

        return to_date;
    }



}
