package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class RewardsDetails {

    @SerializedName("data")
    public List<RewardsDetailsData> data = new ArrayList<>();

//    @SerializedName("data")
//    public HashMap<String, ArrayList> data;

    public class RewardsDetailsData {

//        @SerializedName("available_quantity")
//        public Double available_quantity;

        @SerializedName("discount_percent")
        public Float discount_percent;

        @SerializedName("item_id")
        public Integer item_id;

        @SerializedName("no_of_free_items")
        public Double no_of_free_items;

        @SerializedName("reward_id")
        public Integer reward_id;

        @SerializedName("specific_price")
        public Float specific_price;

    }


}
