package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RegisterToPusher {

    @SerializedName("data")
    public List<Data> data = new ArrayList<>();

    public class Data {

        @SerializedName("channel")
        public String channel;

        @SerializedName("event")
        public String event;

        @SerializedName("key")
        public String key;

    }
}
