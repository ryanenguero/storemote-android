package com.storemote.teafarmscafe


import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.provider.Settings
import com.storemote.teafarmscafe.StoreListAdapter.StoreListAdapt
import io.realm.Realm
import io.realm.RealmResults
import java.util.*
import kotlin.collections.ArrayList
import com.storemote.teafarmscafe.Model_RealmDatabase.*
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.PusherOptions
import android.widget.Toast
import android.provider.Settings.Secure
import com.storemote.teafarmscafe.Model_JSON.*
import com.storemote.teafarmscafe.Model_RealmDatabase.Rewards
import org.json.JSONArray
import org.json.JSONObject


class StoreList : AppCompatActivity() {

    internal lateinit var responseText: TextView
    internal lateinit var apiInterface: APIInterface

//    private Arraylist<String> dogArr = New ArrayList<String>

    private lateinit var dogArr: ArrayList<String>

    private val taskList: MutableList<String> = mutableListOf()
    private val adapter by lazy { makeAdapter(taskList) }

    private val PREFS_TASKS = "prefs_tasks"
    private val KEY_TASKS_LIST = "tasks_list"

    private lateinit var realm: Realm
    private lateinit var realmUpdate: Realm
    private lateinit var storelist: RealmResults<StoreListDB>
    private lateinit var registerToPusher: RealmResults<RegisterToPusherDB>

    private lateinit var listview: ListView

    private lateinit var listItems: ArrayList<String>

//    private var cities: RealmResults<StoreListDB>
//    private val realmChangeListener = { cities -> adapter.setData(cities) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_list)

//        registerApptoPusher


        supportActionBar?.title = "Stores"




        listview = findViewById<ListView>(R.id.taskListView)


//        listItems = null

                //arrayOfNulls<String>(10)

        Realm.init(this)
        // Example of a call to a native method

//        Realm.deleteRealm(Realm.getDefaultConfiguration()!!)

        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        val androidId = Settings.Secure.getString(contentResolver,
                Settings.Secure.ANDROID_ID)

        DownloaderClass().registerApptoPusher(androidId, "1", realm)

        realm.beginTransaction()
        realm.delete(StoreListDB::class.java)
        realm.commitTransaction();


//        realm.where(StoreListDB::class.java).findAll().deleteAllFromRealm()

        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
        storelist = realm.where(StoreListDB::class.java).findAllAsync()
        registerToPusher = realm.where(RegisterToPusherDB::class.java).findAllAsync()

//        storelist.deleteAllFromRealm()


//        responseText = findViewById(R.id.responseText) as TextView
        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = "1"

        val call3 = apiInterface.doCreateUserWithField(map)
        call3.enqueue(object : Callback<StoreListData> {

            override fun onResponse(call: Call<StoreListData>, response: Response<StoreListData>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                val storeListData = response.body()

                val datumList = storeListData?.data

                if (datumList != null) {

                    for (da in 0 until datumList.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");

                        var datum = datumList[da]


                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                        val preferences = baseContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

                        val editor = preferences.edit()
                        editor.putString("clientId", datum.client_id)
//                        editor.putString("storeId", datum.store_id)
                        editor.commit()

                            realm.executeTransaction { r ->
                                // Add a person.
                                // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                val storelistDat = r.createObject(StoreListDB::class.java)

                                storelistDat.clientId = datum.client_id;
                                storelistDat.name = datum.name;
                                storelistDat.storeId = datum.store_id;
                                storelistDat.storeAddress = datum.store_address;
                                storelistDat.storeImage = datum.store_image;

                            }

                    }

                    val arrList = ArrayList<StoreListDB>(realm.where(StoreListDB::class.java).findAll())

                    Log.d("MainActivity", "ResponseTask =  $arrList")

                    reloadListView(arrList)

                }
            }

            override fun onFailure(call: Call<StoreListData>, t: Throwable) {
                call.cancel()
            }




        })


        val intentt = Intent(this, LandingPageView::class.java)



        val context = this
        listview.setOnItemClickListener { _, _, position, _ ->

            val selectedStore = storelist[position]



            val preferences = baseContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

            val editor = preferences.edit()
            editor.putString("clientId", selectedStore!!.clientId)
            editor.putString("storeId", selectedStore!!.storeId)
            editor.putString("storeName", selectedStore!!.name)
            editor.commit()

                DownloaderClass().downloadLandingPagedata(selectedStore!!.clientId, selectedStore!!.storeId, this, realm, intentt)
                DownloaderClass().downloadProductdata(selectedStore!!.clientId, selectedStore!!.storeId, this, realm)
                DownloaderClass().downloadWeeklySpecials(selectedStore!!.clientId, selectedStore!!.storeId, this, realm)
                DownloaderClass().downloadPromos(selectedStore!!.clientId, selectedStore!!.storeId, this, realm)
                DownloaderClass().downloadPromosList(selectedStore!!.clientId, selectedStore!!.storeId, this, realm)
                DownloaderClass().downloadRewards(selectedStore!!.clientId, selectedStore!!.storeId, this, realm)


            Log.d("MainActivity", "Response Click =  ${selectedStore!!.clientId}")

//            startActivity(intentt)

        }

        val preferences = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        val userPreferences = context!!.getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

        NotificationCenter.defaultCenter().addFucntionForNotification("connectToPusher") {

//            Realm.init(this)
//
//            realmUpdate = Realm.getDefaultInstance()

            val registerToPush = ArrayList<RegisterToPusherDB>(realm.where(RegisterToPusherDB::class.java).findAll())

            val options = PusherOptions()
            options.setCluster("ap1")


            val pusher = Pusher(registerToPush[0]!!.key, options)

            val channel = pusher.subscribe(registerToPush[0]!!.getChannel())

//            channel.bind(registerToPush[0]!!.event)
            channel.bind(registerToPush[0]!!.event) { channelName, eventName, data -> val d = ArrayList<String>()

                var value = toMap(JSONObject(data))
                var action = value["action"] as ArrayList<String>

                for (da in 0 until action.size) {

                    if (action[da] == "Update-StoreList"){



                        val map = HashMap<String, String>()
                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = "1"

                        val call3 = apiInterface.doCreateUserWithField(map)
                        call3.enqueue(object : Callback<StoreListData> {
                            override fun onResponse(call: Call<StoreListData>, response: Response<StoreListData>) {
                                val storeListData = response.body()
                                val datumList = storeListData?.data

                                realm.beginTransaction()
                                realm.delete(StoreListDB::class.java)
                                realm.commitTransaction()

                                if (datumList != null) {
                                    for (da in 0 until datumList.size) {
                                        var datum = datumList[da]
                                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                        realm.executeTransaction { r ->
                                            val storelistDat = r.createObject(StoreListDB::class.java)

                                            storelistDat.clientId = datum.client_id;
                                            storelistDat.name = datum.name;
                                            storelistDat.storeId = datum.store_id;
                                            storelistDat.storeAddress = datum.store_address;
                                            storelistDat.storeImage = datum.store_image;
                                        }
                                    }
                                }
                            }

                            override fun onFailure(call: Call<StoreListData>, t: Throwable) {
                                call.cancel()
                            }
                        })



                    }else if (action[da] == "Update-LandingPage"){

//                        realmUpdate.beginTransaction()
//                        realmUpdate.delete(LandingPageDB::class.java)
//                        realmUpdate.commitTransaction()

//                        DownloaderClass().downloadLandingPagedata(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm, intentt)


                        apiInterface = APIClient.getClient().create(APIInterface::class.java)

                        val map = HashMap<String, String>()

                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = preferences.getString("clientId", "0").toString()
                        map["store_id"] = preferences.getString("storeId", "0").toString()

                        val call3 = apiInterface.doDownloadLandingPageData(map)
                        call3.enqueue(
                                object : Callback<LandingPage> {

                                    override fun onResponse(call: Call<LandingPage>, response: Response<LandingPage>) {
                                        val landingPageData = response.body()

                                        val data = landingPageData!!.data

                                        if (data != null) {

                                            realm.beginTransaction()
                                            realm.delete(LandingPageDB::class.java)
                                            realm.commitTransaction();


                                            Log.d("MainActivity", "ResponseLandingPageData =  ${data[0]!!.menu}");

                                            realm.executeTransaction { r ->

                                                val landingDataDb = r.createObject(LandingPageDB::class.java)

                                                landingDataDb.clientId = data[0]!!.client_id
                                                landingDataDb.menu = data[0]!!.menu
                                                landingDataDb.storeId = data[0]!!.store_id
                                                landingDataDb.promo = data[0]!!.promo
                                                landingDataDb.rewards = data[0]!!.rewards
                                                landingDataDb.weeklySpecial = data[0]!!.weekly_special

                                            }

                                        }
                                    }



                                    override fun onFailure(call: Call<LandingPage>, t: Throwable) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        call3.cancel()
                                    }



                                })
                    }else if (action[da] == "Update-WeeklySpecials"){

//                        realmUpdate.beginTransaction()
//                        realmUpdate.delete(WeeklySpecials::class.java)
//                        realmUpdate.commitTransaction()

//                        DownloaderClass().downloadWeeklySpecials(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm)



                        apiInterface = APIClient.getClient().create(APIInterface::class.java)

                        val map = HashMap<String, String>()

                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = preferences.getString("clientId", "0").toString()
                        map["store_id"] = preferences.getString("storeId", "0").toString()

                        val call3 = apiInterface.doDownloadWeeklySpecials(map)
                        call3.enqueue(object : Callback<MenuProduct> {



                            override fun onResponse(call: Call<MenuProduct>, response: Response<MenuProduct>) {
                                Log.d("MainActivity", "ResponseBodyError = " + response.isSuccessful);

                                val menuProdData = response.body()

                                val data = menuProdData!!.data

                                if (data != null) {
                                    realm.beginTransaction()
                                    realm.delete(WeeklySpecials::class.java)
                                    realm.commitTransaction();

                                    for (da in 0 until data.size) {
                                        Log.d("MainActivity", "ResponseWeeklySpecials =  ${data[da]!!.name}");

                                        realm.executeTransaction { r ->
                                            val menuProductDTB = r.createObject(MenuProductDB::class.java)

                                            menuProductDTB.category = data[da].category
                                            menuProductDTB.client_id = data[da].client_id
                                            menuProductDTB.costPrice = data[da].cost_price
                                            menuProductDTB.createdBy = data[da].created_by
                                            menuProductDTB.createdDate = data[da].created_date
                                            menuProductDTB.versionNo= data[da].version_no
                                            menuProductDTB.itemId = data[da].item_id
                                            menuProductDTB.itemNumber = data[da].item_number
                                            menuProductDTB.itemType = data[da].item_type
                                            menuProductDTB.name = data[da].name
                                            menuProductDTB.picFilename = data[da].pic_filename
                                            menuProductDTB.description = data[da].description;
                                            menuProductDTB.receivingQuantity = data[da].receiving_quantity
                                            menuProductDTB.reorderLevel = data[da].reorder_level
                                            menuProductDTB.stockType = data[da].stock_type
                                            menuProductDTB.storeId = data[da].store_id
                                            menuProductDTB.supplierId = data[da].supplier_id
                                            menuProductDTB.taxCategoryId = data[da].tax_category_id
                                            menuProductDTB.unitPrice = data[da].unit_price
                                            menuProductDTB.updatedBy = data[da].updated_by
                                            menuProductDTB.updatedDate = data[da].updated_date

                                            val weeklySpecialsDTB = r.createObject(WeeklySpecials::class.java)

                                            weeklySpecialsDTB.menuProd =  menuProductDTB

                                            menuProductDTB.weeklySpecials = weeklySpecialsDTB
                                            //.setWeeklySpecials(weeklySpecialsDTB)


                                        }
                                    }
                                }
                            }

                            override fun onFailure(call: Call<MenuProduct>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                Log.d("MainActivity", "Weekly Error =  ${t.cause}");

                                call3.cancel()
                            }

                        })


                    }else if (action[da] == "Update-Menu"){

//                        realmUpdate.beginTransaction()
//                        realmUpdate.delete(MenuProductDB::class.java)
//                        realmUpdate.delete(MenuProductExceptionDB::class.java)
//                        realmUpdate.delete(CategoryDB::class.java)
//                        realmUpdate.commitTransaction()

                        DownloaderClass().downloadProductdata(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm)

                        val map = HashMap<String, String>()

                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = preferences.getString("clientId", "0").toString()
                        map["store_id"] = preferences.getString("storeId", "0").toString()

                        val call3 = apiInterface.doDownloadProductData(map)
                        call3.enqueue(
                                object : Callback<MenuProduct> {

                                    override fun onResponse(call: Call<MenuProduct>, response: Response<MenuProduct>) {
                                        val menuProdData = response.body()
                                        val data = menuProdData!!.data
                                        if (data != null) {
                                            realm.beginTransaction()
                                            realm.delete(MenuProductDB::class.java)
                                            realm.delete(MenuProductExceptionDB::class.java)
                                            realm.commitTransaction();
                                            for (da in 0 until data.size) {
                                                Log.d("MainActivity", "ResponseProductData =  ${data[da]!!.name}");



                                                realm.executeTransaction { r ->
                                                    val menuProductDTB = r.createObject(MenuProductDB::class.java)

                                                    menuProductDTB.category = data[da].category
                                                    menuProductDTB.client_id = data[da].client_id
                                                    menuProductDTB.costPrice = data[da].cost_price
                                                    menuProductDTB.createdBy = data[da].created_by
                                                    menuProductDTB.createdDate = data[da].created_date
                                                    menuProductDTB.versionNo= data[da].version_no
                                                    menuProductDTB.itemId = data[da].item_id
                                                    menuProductDTB.itemNumber = data[da].item_number
                                                    menuProductDTB.itemType = data[da].item_type
                                                    menuProductDTB.name = data[da].name
                                                    menuProductDTB.picFilename = data[da].pic_filename
                                                    menuProductDTB.description = data[da].description;
                                                    menuProductDTB.receivingQuantity = data[da].receiving_quantity
                                                    menuProductDTB.reorderLevel = data[da].reorder_level
                                                    menuProductDTB.stockType = data[da].stock_type
                                                    menuProductDTB.storeId = data[da].store_id
                                                    menuProductDTB.supplierId = data[da].supplier_id
                                                    menuProductDTB.taxCategoryId = data[da].tax_category_id
                                                    menuProductDTB.unitPrice = data[da].unit_price
                                                    menuProductDTB.updatedBy = data[da].updated_by
                                                    menuProductDTB.updatedDate = data[da].updated_date

                                                    val menuExceptionDTB = r.createObject(MenuProductExceptionDB::class.java)

                                                    menuExceptionDTB.menuProd =  menuProductDTB

                                                    menuProductDTB.menuProductException = menuExceptionDTB



                                                }
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<MenuProduct>, t: Throwable) {
//                      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        Log.d("MainActivity", "Response Product Error =  $t ");
                                        call3.cancel()
                                    }


                                })

                        val map1 = HashMap<String, String>()

                        map1["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map1["client_id"] = preferences.getString("clientId", "0").toString()
                        map1["store_id"] = preferences.getString("storeId", "0").toString()

                        val call4 = apiInterface.doDownloadCategoryData(map1)
                        call4.enqueue(
                                object : Callback<Category> {

                                    override fun onResponse(call: Call<Category>, response: Response<Category>) {

                                        val categoryDat = response.body()

                                        val data = categoryDat!!.data

                                        if (data != null) {
                                            realm.beginTransaction()
                                            realm.delete(CategoryDB::class.java)
                                            realm.commitTransaction();
                                            for (da in 0 until data.size) {
                                                Log.d("MainActivity", "ResponseCategory = $data");

                                                realm.executeTransaction { r ->

                                                    val categoryDTB = r.createObject(CategoryDB::class.java)

                                                    categoryDTB.categoryId = data[da].category_id
                                                    categoryDTB.name = data[da].name
                                                }
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<Category>, t: Throwable) {
                                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        call4.cancel()
                                    }

                                })



                    }else if (action[da] == "Update-Promo"){

//                        realmUpdate.beginTransaction()
//                        realmUpdate.delete(PromosDB::class.java)
//                        realmUpdate.delete(PromoListDB::class.java)
//                        realmUpdate.commitTransaction()
//
//                        DownloaderClass().downloadPromos(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm)

                        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = preferences.getString("clientId", "0").toString()
                        //clientId
                        map["store_id"] = preferences.getString("storeId", "0").toString()

                        //storeId

                        val call3 = apiInterface.doDownloadPromos(map)
                        call3.enqueue( object : Callback<Promos> {

                            override fun onResponse(call: Call<Promos>, response: Response<Promos>) {
                                val promosData = response.body()

                                Log.d("MainActivity", "promospromos =  ${promosData}");
                                //.body()

                                val data = promosData!!.data

                                if (data != null) {
                                    realm.beginTransaction()
                                    realm.delete(PromosDB::class.java)
                                    realm.commitTransaction();
                                    for (da in 0 until data.size) {
                                        Log.d("MainActivity", "ResponsePromos =  ${data[da]!!.name}");
                                        realm.executeTransaction { r ->
                                            val promosDTB = r.createObject(PromosDB::class.java)

                                            promosDTB.discountPercent = data[da].discount_percent
                                            promosDTB.fromDate = data[da].from_date
//                                    promosDTB.itemFreeId = data[da].item_free_id
                                            promosDTB.itemId = data[da].item_id
                                            promosDTB.itemName = data[da].name
                                            promosDTB.nooffreeitems = data[da].no_of_free_items
                                            promosDTB.promoId = data[da].promo_id
                                            promosDTB.promoTypeId = data[da].promo_type_id
                                            promosDTB.specificPrice = data[da].specific_price
                                            promosDTB.toDate = data[da].to_date

                                        }
                                    }
                                }
                            }

                            override fun onFailure(call: Call<Promos>, t: Throwable) {
//
                                call3.cancel()
                            }

                        })


//                        DownloaderClass().downloadPromosList(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm)


                        val map1 = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map1["client_id"] = preferences.getString("clientId", "0").toString()
                        //clientId
                        map1["store_id"] = preferences.getString("storeId", "0").toString()

                        val call4 = apiInterface.doDownloadPromosList(map1)
                        call4.enqueue(
                                object : Callback<PromoList> {



                                    override fun onResponse(call: Call<PromoList>, response: Response<PromoList>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                                        val promosData = response.body()

                                        val data = promosData!!.data

                                        if (data != null) {
                                            realm.beginTransaction()
                                            realm.delete(PromoListDB::class.java)
                                            realm.commitTransaction();
                                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                                Log.d("MainActivity", "ResponsePromoList =  ${data[da]!!.name}");

                                                realm.executeTransaction { r ->
                                                    // Add a person.
                                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                                    val promosListDTB = r.createObject(PromoListDB::class.java)

                                                    promosListDTB.fromDate = data[da].from_date
                                                    promosListDTB.promoId = data[da].promo_id
                                                    promosListDTB.toDate = data[da].to_date
                                                    promosListDTB.name = data[da].name
                                                    promosListDTB.promoBanner = data[da].promo_banner



                                                }
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<PromoList>, t: Throwable) {
                                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        call4.cancel()
                                    }

                                })

                    }else if (action[da] == "Update-Rewards"){

//                        realmUpdate.beginTransaction()
//                        realmUpdate.delete(Rewards::class.java)
//                        realmUpdate.commitTransaction()

//                        DownloaderClass().downloadRewards(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), this, realm)

                        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["client_id"] = preferences.getString("clientId", "0").toString()
                        map["store_id"] = preferences.getString("storeId", "0").toString()

                        val call3 = apiInterface.doDownloadRewards(map)
                        call3.enqueue(
                                object : Callback<com.storemote.teafarmscafe.Model_JSON.Rewards> {



                                    override fun onResponse(call: Call<com.storemote.teafarmscafe.Model_JSON.Rewards>, response: Response<com.storemote.teafarmscafe.Model_JSON.Rewards>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                                        val rewardsData = response.body()

                                        val data = rewardsData!!.data

                                        if (data != null) {

                                            realm.beginTransaction()
                                            realm.delete(Rewards::class.java)
                                            realm.commitTransaction()

                                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                                Log.d("MainActivity", "ResponseRewards =  ${data[da]!!.name}");

                                                realm.executeTransaction { r ->
                                                    // Add a person.
                                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                                    val rewardsDTB = r.createObject(Rewards::class.java)

                                                    rewardsDTB.fromDate = data[da].from_date
                                                    rewardsDTB.toDate = data[da].to_date
                                                    rewardsDTB.name = data[da].name
                                                    rewardsDTB.discountPercent = data[da].discount_percent
                                                    rewardsDTB.nooffreeitems = data[da].no_of_free_items
                                                    rewardsDTB.price = data[da].price
                                                    rewardsDTB.rewardBanner = data[da].reward_banner
                                                    rewardsDTB.rewardId = data[da].reward_id
                                                    rewardsDTB.specificPrice = data[da].specific_price




                                                }

//                                                DownloaderClass().downloadRewardsDetails(preferences.getString("clientId", "0").toString(), preferences.getString("storeId", "0").toString(), context, realm, data[da].reward_id)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<com.storemote.teafarmscafe.Model_JSON.Rewards>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                        call3.cancel()
                                    }

                                })

                    }else if (action[da] == "Update-Profile"){

//                        val userData = ArrayList<UserDataDB>(realm.where(UserDataDB::class.java).findAll())

//                        realm.beginTransaction()
//                        realm.delete(UserDataDB::class.java)
//                        realm.commitTransaction()

                        val map = HashMap<String, String>()

                        val username = userPreferences.getString("username", "").toString()
                        val password = userPreferences.getString("password", "").toString()


                        val androidId = Settings.Secure.getString(contentResolver,
                                Settings.Secure.ANDROID_ID)

                        map["muid"] = androidId
                        map["username"] = username
                        map["password"] = password
                        map["device_token"] = androidId
                        map["client_id"] = preferences.getString("clientId", "0").toString()

                        val call3 = apiInterface.doLoginFromApp(map)
                        call3.enqueue(object : retrofit2.Callback<UserData> {

                            override fun onResponse(call: retrofit2.Call<UserData>, response: Response<UserData>) {
                                val userData = response.body()

                                val datumList = userData?.data

                                if (datumList != null) {

                                    for (da in 0 until datumList.size) {
                                        var datum = datumList[da]
                                        Log.d("MainActivity", "ResponseDataForLogin =  ${datum.full_name}");

                                        realm.beginTransaction()
                                        realm.delete(UserDataDB::class.java)
                                        realm.commitTransaction()

                                        realm.executeTransaction { r ->
                                            // Add a person.
                                            // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
                                            val userData = r.createObject(UserDataDB::class.java)

                                            val editor = preferences.edit()
                                            editor.putBoolean("isLoggedIn", true)
                                            editor.commit()

                                            userData.isLogged = true
                                            userData.balance = datum.balance
                                            userData.createdDtm = datum.created_dtm
                                            userData.customerId = datum.customer_id
                                            userData.email = datum.email
                                            userData.fullname = datum.full_name
                                            userData.isDelete = datum.is_delete
                                            userData.mobileNo = datum.mobile_no
                                            userData.password = datum.password
                                            userData.points = datum.points
                                            userData.paypalId = datum.paypal_id
                                            userData.token = datum.token
                                            userData.username = datum.username
                                            userData.versionNo = datum.version_no
                                        }
                                    }

                                }
                            }

                            override fun onFailure(call: retrofit2.Call<UserData>, t: Throwable) {
                                call.cancel()
                            }




                        })

                    }else if (action[da] == "Notification"){


                    }

                }

                Log.d("MainActivity", "Pusher Response =  $value")

            }

            pusher.connect()

        }


    }

    fun toMap(`object`: JSONObject): Map<String, Any> {
        val map = HashMap<String, Any>()

        val keysItr = `object`.keys().iterator()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            var value = `object`.get(key)

            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            map.put(key, value)
        }
        return map
    }

    fun toList(array: JSONArray): List<Any> {
        val list = ArrayList<Any>()
        for (i in 0..array.length() - 1) {
            var value = array.get(i)
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            list.add(value)
        }
        return list
    }


//
//    fun startLandingPage(){
//
//        val intentt = Intent(this, LandingPageView::class.java)
//
//
//
//    }

    private  fun reloadListView(arr: ArrayList<StoreListDB>){

//        val sto = realm.where(StoreListDB::class.java).findAll()



//        listItems
//        Log.d("MainActivity", "ResponseTask =  $sto");

//        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arr)

        val adapt = StoreListAdapt(this, arr)
        listview.adapter = adapt

    }



    private fun makeAdapter(list: List<String>): ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, list)
    /**
     *
     *
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}
