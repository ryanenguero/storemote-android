package com.storemote.teafarmscafe

import com.storemote.teafarmscafe.Model_JSON.*
import com.storemote.teafarmscafe.Model_JSON.PurchasedHistory
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.HashMap

interface APIInterface {


    //    @GET("/api/unknown")
//    Call<MultipleResource> doGetListResources();
//
//    @POST("/api/users")
//    Call<User> createUser(@Body User user);
//
//    @GET("/api/users?")
//    Call<UserList> doGetUserList(@Query("page") String page);

    //    @GET("/get_client_store")
    //    Call<StoreListData> doCreateUserWithField(@Field("token") String token, @Field("client_id") String client_id);
    //    @POST("/posts")
    //    @FormUrlEncoded
    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_client_store")
    abstract fun doCreateUserWithField(@Body body: HashMap<String, String>): Call<StoreListData>
    //    Call<StoreListData> doCreateUserWithField(@Field("token") String token, @Field("client_id") String client_id);
//            Call<StoreListData>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_landing_page")
    abstract fun doDownloadLandingPageData(@Body body: HashMap<String, String>): Call<LandingPage>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_items")
    abstract fun doDownloadProductData(@Body body: HashMap<String, String>): Call<MenuProduct>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_weekly_specials")
    abstract fun doDownloadWeeklySpecials(@Body body: HashMap<String, String>): Call<MenuProduct>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_item_promo_list")
    abstract fun doDownloadPromos(@Body body: HashMap<String, String>): Call<Promos>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_promos")
    abstract fun doDownloadPromosList(@Body body: HashMap<String, String>): Call<PromoList>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_rewards")
    abstract fun doDownloadRewards(@Body body: HashMap<String, String>): Call<Rewards>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_reward_items")
    abstract fun doDownloadRewardsDetails(@Body body: HashMap<String, String>): Call<RewardsDetails>

    //    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
    @POST("get_categories")
    abstract fun doDownloadCategoryData(@Body body: HashMap<String, String>): Call<Category>


    @POST("login")
    abstract fun doLoginFromApp(@Body body: HashMap<String, String>): Call<UserData>

    @POST("tp_login")
    abstract fun doLoginFromFB(@Body body: HashMap<String, String>): Call<UserData>

    @POST("pay_at_the_counter")
    abstract fun doOrderListResponse(@Body body: HashMap<String, Any>): Call<OrderListReponse>

    @POST("item_ordered")
    abstract fun doOrderListUseCreditsResponse(@Body body: HashMap<String, Any>): Call<OrderListReponse>

    @POST("registration")
    abstract fun doRegisterUser(@Body body: HashMap<String, String>): Call<UserData>

    @POST("get_card_rewards")
    abstract fun doDownloadRewardsStamp(@Body body: HashMap<String, String>): Call<RewardsCard>

    @POST("app_registration")
    abstract fun doRegisterToPusher(@Body body: HashMap<String, String>): Call<RegisterToPusher>

    @POST("get_sales")
    abstract fun doDownloadPurchasedHistory(@Body body: HashMap<String, String>): Call<PurchasedHistory>

    @POST("get_pdf")
    abstract fun doDownloadPDFReceipt(@Body body: HashMap<String, String>): Call<PDFReceipt>

    @POST("get_card_reward_items")
    abstract fun doDownloadRewardsCardItems(@Body body: HashMap<String, String>): Call<RewardsCardItems>


    @POST("claim_rewards")
    abstract fun claimRewards(@Body body: HashMap<String, String>): Call<ClaimRewardsMessage>

    @POST("forgot_password")
    abstract fun forgotPassword(@Body body: HashMap<String, String>): Call<ForgotPassword>

//    @Headers({"Content-Type: application/json", "Auth: Basic e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="})
//    @POST("get_weekly_specials")
//    Call<WeeklySpecials> doDownl
}