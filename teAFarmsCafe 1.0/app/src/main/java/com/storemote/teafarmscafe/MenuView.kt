package com.storemote.teafarmscafe

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle

import android.widget.*

//import android.util.Log
import android.net.Uri
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import com.storemote.teafarmscafe.MenuViewAdapter.MenuViewAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.CategoryDB
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductExceptionDB
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsCardDB
//import com.storemote.teafarmscafe.APIInterface

//import android.util.Log
//import java.util.ArrayList

import io.realm.Realm
import io.realm.RealmResults


class MenuView : AppCompatActivity() {
        //Fragment(), SearchView.OnQueryTextListener {

    private val PREFS_TASKS = "prefs_tasks"
    private val KEY_TASKS_LIST = "tasks_list"

    private lateinit var realm: Realm
    private lateinit var menuException: RealmResults<MenuProductExceptionDB>
    private lateinit var category: RealmResults<CategoryDB>

    private lateinit var listview: ListView

//    private var listener: MenuView.OnFragmentInteractionListener? = null

    private lateinit var editsearch: SearchView

    private lateinit var adapt: MenuViewAdapter

    private var arrList = ArrayList<Any>()

    var textCartItemCount: TextView? = null
    var mCartItemCount = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        this.set
//        setHasOptionsMenu(true)=

        setContentView(R.layout.menu_list)


        supportActionBar?.title = "Menu"

//        setHasOptionsMenu(true)

//        val view = inflater.inflate(R.layout.menu_list, container, false)

        listview  = findViewById(R.id.menulistview)
//                ..view.f//indViewById<ListView>(R.id.weeklylistview)


        Realm.init(this)
        // Create the Realm instance
        realm = Realm.getDefaultInstance()

        // Asynchronous queries are evaluated on a background thread,
        // and passed to the registered change listener when it's done.
        // The change listener is also called on any future writes that change the result set.
        menuException = realm.where(MenuProductExceptionDB::class.java).findAllAsync()

        category = realm.where(CategoryDB::class.java).findAllAsync()


//
        val menuList = ArrayList<MenuProductExceptionDB>(realm.where(MenuProductExceptionDB::class.java).findAll())
        val categoryList = ArrayList<CategoryDB>(realm.where(CategoryDB::class.java).findAll())

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size
//        val arr = ArrayList<WeeklySpecials>(arrList)







//        for (da in 0 until categoryList.size) {

            val map = HashMap<String, Any>()
//            map["header"] = categoryList[da].name


            var arrMenuList = ArrayList<MenuProductExceptionDB>()


            for (dat in 0  until menuList.size) {

                if (menuList[dat].menuProd.category == intent.getStringExtra("category")){
                        //categoryList[da].name){

                    arrMenuList.add(menuList[dat])

                }

            }




//            map.set("header", categoryList[da].name)
            map.set("data", arrMenuList)

            arrList.add(map)

//        }

        Log.d("MainActivity", "datdat =  $arrList");



        adapt = MenuViewAdapter(this)
//        listview.adapter = adapt


            for (da in 0 until arrList.size) {

                val dict = arrList[da] as HashMap<String, Any>

//                val catName = dict["header"] as String
//                adapt!!.addSectionHeaderItem(catName, false)
//                adapt.isEnabled(da)
//                adapt.addItem(menuList[0])

                val men = dict["data"] as ArrayList<MenuProductExceptionDB>



                for (dat in 0 until men.size) {
                    adapt!!.addItem(men[dat], false)

                    Log.d("MainActivity", "menmen =  ${men[dat].menuProd.name}");
                }

            }



//        editsearch = view.findViewById(R.id.searchview) as SearchView
//        editsearch!!.setOnQueryTextListener(this)
//        editsearch!!.setOnQueryTextListener(this)

                //setOnQueryTextListener(this.view)

        listview.adapter = adapt


        listview?.setOnItemClickListener { _, _, position, _ ->


//            val selectedMenu = arrList[position]  as HashMap<String, Any>
//            val men = selectedMenu["data"] as ArrayList<MenuProductExceptionDB>
//
//            val menu = men[position] as MenuProductExceptionDB

            val selectedMenu = adapt!!.mData[position] as MenuProductExceptionDB
            val intentt = Intent(this, ProductDetailsView::class.java)

//            intentt.putExtra("name", selectedMenu.menuProd.name)
//            intentt.putExtra("description", selectedMenu.menuProd.description)
//            intentt.putExtra("price", selectedMenu.menuProd.unitPrice)
//            intentt.putExtra("image", selectedMenu.menuProd.picFilename)

            intentt.putExtra("is_promo", "0")
            intentt.putExtra("is_rewards", "0")
            intentt.putExtra("name", selectedMenu.menuProd.name)
            intentt.putExtra("description", selectedMenu.menuProd.description)
            intentt.putExtra("price", selectedMenu.menuProd.unitPrice)
            intentt.putExtra("image", selectedMenu.menuProd.picFilename)
            intentt.putExtra("category", selectedMenu.menuProd.category)
            intentt.putExtra("clientId", selectedMenu.menuProd.client_id)
            intentt.putExtra("costPrice", selectedMenu.menuProd.costPrice)
            intentt.putExtra("createdBy", selectedMenu.menuProd.createdBy)
            intentt.putExtra("createdDate", selectedMenu.menuProd.createdDate)
            intentt.putExtra("itemId", selectedMenu.menuProd.itemId)
            intentt.putExtra("itemNumber", selectedMenu.menuProd.itemNumber)
            intentt.putExtra("itemType", selectedMenu.menuProd.itemType)
            intentt.putExtra("receivingQuantity", selectedMenu.menuProd.receivingQuantity)
            intentt.putExtra("reorderLevel", selectedMenu.menuProd.reorderLevel)
            intentt.putExtra("stockType", selectedMenu.menuProd.stockType)
            intentt.putExtra("storeId", selectedMenu.menuProd.storeId)
            intentt.putExtra("supplierId", selectedMenu.menuProd.supplierId)
            intentt.putExtra("taxCategoryId", selectedMenu.menuProd.taxCategoryId)
            intentt.putExtra("updatedBy", selectedMenu.menuProd.updatedBy)
            intentt.putExtra("updatedDate", selectedMenu.menuProd.updatedDate)
            intentt.putExtra("versionNo", selectedMenu.menuProd.versionNo)

            //putExtra("data", selectedSpecials.menuProd)

            startActivity(intentt)

            Log.d("MainActivity", "SelectedMenu =  $selectedMenu");


        }

//        return view
//        return super.onCreateView(inflater, container, savedInstanceState)

    }


    fun onQueryTextSubmit(query: String): Boolean {

        return false
    }

    fun onQueryTextChange(newText: String): Boolean {
//        adapt!!.filter(newText)
        listview.refreshDrawableState()
        return false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        menuInflater.inflate(R.menu.cart_menu, menu)

        val menuItem = menu!!.findItem(R.id.cart_btn)

        val actionView = MenuItemCompat.getActionView(menuItem)
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView

//        setupBadge()

        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        mCartItemCount = orderListCount.size


        textCartItemCount!!.text = mCartItemCount.toString()

        actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItem)
            }
        })


        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {



        when (item?.getItemId()) {

            R.id.cart_btn -> {
                // Do something

                val intentt = Intent(this, OrderListView::class.java)

                startActivity(intentt)

                return true
            }
        }


        return super.onOptionsItemSelected(item)
    }




//    @SuppressLint("ResourceType")
//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        super.onCreateOptionsMenu(menu, inflater)
//
////        inflater!!.inflate(R.menu.cart_menu, menu);
//
//
//        inflater!!.inflate(R.menu.cart_menu, menu)
//
//        val menuItem = menu!!.findItem(R.id.cart_btn)
//
//        val actionView = MenuItemCompat.getActionView(menuItem)
//        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView
//
////        setupBadge()
//
//        val orderListCount = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
//        mCartItemCount = orderListCount.size
//
//
//        textCartItemCount!!.text = mCartItemCount.toString()
//
//        actionView.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View) {
//                onOptionsItemSelected(menuItem)
//            }
//        })
//
////    }

    override fun onStart() {
        super.onStart()

     invalidateOptionsMenu()

//        setHasOptionsMenu(true)

    }

    override fun onResume() {
        super.onResume()

    invalidateOptionsMenu()

//        setHasOptionsMenu(true)

    }

//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//
////        val id = item?.getItemId()
////
////        //noinspection SimplifiableIfStatement
////        return if (id == R.id.cart_btn) {
////            true
////        } else super.onOptionsItemSelected(item)
//
//        when (item?.getItemId()) {
//
//            R.id.cart_btn -> {
//                // Do something
//                val intentt = Intent(this, OrderListView::class.java)
//
//                startActivity(intentt)
//
//                return true
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }
//
//    private fun setupBadge() {
//
//        if (textCartItemCount != null) {
//            if (mCartItemCount === 0) {
//                if (textCartItemCount!!.getVisibility() !== View.GONE) {
//                    textCartItemCount!!.setVisibility(View.GONE)
//                }
//            } else {
//
//
//
////                 .setText(String.valueOf(Math.min(mCartItemCount, 99)))
//                if (textCartItemCount!!.getVisibility() !== View.VISIBLE) {
//                    textCartItemCount!!.setVisibility(View.VISIBLE)
//                }
//            }
//        }
//    }
//    fun filtering(filter: String){
//
//        Log.d("MainActivity", "Filtering Array =  $filter");
//
//        Realm.init(view!!.context)
//        // Create the Realm instance
//        realm = Realm.getDefaultInstance()
//
//        // Asynchronous queries are evaluated on a background thread,
//        // and passed to the registered change listener when it's done.
//        // The change listener is also called on any future writes that change the result set.
//        menuException = realm.where(MenuProductExceptionDB::class.java).findAllAsync()
//
//        category = realm.where(CategoryDB::class.java).findAllAsync()
//
//
//        val menuList = ArrayList<MenuProductExceptionDB>(realm.where(MenuProductExceptionDB::class.java).findAll())
//        val categoryList = ArrayList<CategoryDB>(realm.where(CategoryDB::class.java).findAll())
//
//        val arrList = ArrayList<Any>()
//
//        for (da in 0 until categoryList.size) {
//
//            val map = HashMap<String, Any>()
//            var arrMenuList = ArrayList<MenuProductExceptionDB>()
//            for (dat in 0  until menuList.size) {
//                if (menuList[dat].menuProd.category == categoryList[da].name){
//                    if (filter == menuList[dat].menuProd.name) {
//                        arrMenuList.add(menuList[dat])
//                    }
//                }
//            }
//
//            map.set("header", categoryList[da].name)
//            map.set("data", arrMenuList)
//
//            arrList.add(map)
//
//        }
////
//        Log.d("MainActivity", "datdat =  $arrList");
//
//        adapt = MenuViewAdapter(this.context!!)
//
//        for (da in 0 until arrList.size) {
//
//            val dict = arrList[da] as HashMap<String, Any>
//
//            val catName = dict["header"] as String
//            adapt!!.addSectionHeaderItem(catName)
//
//            val men = dict["data"] as ArrayList<MenuProductExceptionDB>
//
//
//
//            for (dat in 0 until men.size) {
//
//                if (men[dat].menuProd.name.contains(filter)) {
//
//                adapt!!.addItem(men[dat])
//
//                Log.d("MainActivity", "menmen =  ${men[dat].menuProd.name}");
//
//             }
//            }
//
//        }
//
//
//
////        editsearch = view!!.findViewById(R.id.searchview) as SearchView
////        editsearch!!.setOnQueryTextListener(this)
//////        editsearch!!.setOnQueryTextListener(this)
////
////        //setOnQueryTextListener(this.view)
////
//        listview.adapter = adapt
//
//
//        listview?.setOnItemClickListener { _, _, position, _ ->
//
//            val selectedMenu = adapt!!.mData[position] as MenuProductExceptionDB
//            val intentt = Intent(this.context, ProductDetailsView::class.java)
//
//            intentt.putExtra("name", selectedMenu.menuProd.name)
//            intentt.putExtra("description", selectedMenu.menuProd.description)
//            intentt.putExtra("price", selectedMenu.menuProd.unitPrice)
//            intentt.putExtra("image", selectedMenu.menuProd.picFilename)
//
//            //putExtra("data", selectedSpecials.menuProd)
//
//            startActivity(intentt)
//
//            Log.d("MainActivity", "SelectedMenu =  $selectedMenu");
//
//
//        }
//
////        listview.invalidateViews()
//
//    }
//    /**
//     * row item
//     */
//    interface Item {
//        val isSection: Boolean
//        var title: String
//        var menu : MenuProductExceptionDB
//    }
//
//    /**
//     * Section Item
//     */
//    abstract inner class SectionItem(title: String) : Item {
//
//
//        fun SectionItem(title: String) {
//            this.title = title
//        }
//
//        override fun getTitle(): String {
//            return title
//        }
//
//        override fun isSection(): Boolean {
//            return true
//        }
//    }
//
//    /**
//     * Entry Item
//     */
//    abstract inner class EntryItem(menu: MenuProductExceptionDB) : Item {
//
////        val title: String
//
//        fun EntryItem(menu: MenuProductExceptionDB) {
//            this.title = title
//        }
//
//        override fun getTitle(): MenuProductExceptionDB {
//            return menu
//        }
//
//        override fun isSection(): Boolean {
//            return false
//        }
//    }



//    fun onButtonPressed(uri: Uri) {
//        listener?.onFragmentInteraction(uri)
//    }
//
//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
//    }
//
//    override fun onDetach() {
//        super.onDetach()
//        listener = null
//    }
//
//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     *
//     *
//     * See the Android Training lesson [Communicating with Other Fragments]
//     * (http://developer.android.com/training/basics/fragments/communicating.html)
//     * for more information.
//     */
//    interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        fun onFragmentInteraction(uri: Uri)
//    }
//
//    companion object {
//
//        fun newInstance() : Fragment
//        {
//
//            var fb = MenuView()
//            return fb
//
//        }
//    }
}