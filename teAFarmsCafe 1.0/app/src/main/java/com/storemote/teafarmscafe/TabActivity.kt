package com.storemote.teafarmscafe

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.tab_activity.*
import android.app.FragmentTransaction
import android.content.Context
import android.net.Uri
import android.support.v4.app.Fragment
import android.view.Menu
import com.storemote.teafarmscafe.WeeklySpecialsFragment.OnFragmentInteractionListener
import android.content.SharedPreferences



//import android.support.v4.app.FragmentTransaction


public class TabActivity : AppCompatActivity(), WeeklySpecialsView.OnFragmentInteractionListener, ProfileVIew.OnFragmentInteractionListener, LoginView.OnFragmentInteractionListener, RewardsView.OnFragmentInteractionListener, PromoView.OnFragmentInteractionListener, CategoryView.OnFragmentInteractionListener {
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var listItems: ArrayList<String>

//    val manager = fragmentManager

//    private var selectedFragment = Fragment()

    lateinit var bnv : BottomNavigationView


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

//        var selectedFragment : Fragment? = null

        val preferences = baseContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

        val isLoggedIn = preferences.getBoolean("isLoggedIn", false)

        when (item.itemId) {

            R.id.navigation_weeklyspecials -> {
//                message.setText(R.string.title_home)
                Log.d("MainActivity", "Weekly Specials")

                createWeeklyFragment()

//                selectedFragment = WeeklySpecialsFragment.newInstance()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_menu -> {
//                message.setText(R.string.title_dashboard)
                Log.d("MainActivity", "Menu")

//                createMenuFragment()

                createCategoryFragment()


                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
//                message.setText(R.string.title_notifications)
                Log.d("MainActivity", "Profile")

                if (isLoggedIn) {
                    createProfileFragment()
                }else{
                    createLoginFragment()
                }

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_promo -> {
//                message.setText(R.string.title_notifications)
                Log.d("MainActivity", "Promo")

                createPromoFragment()

                return@OnNavigationItemSelectedListener true
            }

            R.id.navigation_rewards -> {
//                message.setText(R.string.title_notifications)
                Log.d("MainActivity", "Rewards")

                createRewardFragment()

                return@OnNavigationItemSelectedListener true




            }


        }


//        if (selectedFragment != null) {
//
//        }

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tab_activity)

        bnv = findViewById(R.id.navigation) as BottomNavigationView
//        bnv.setOnNavigationItemSelectedListener()
        bnv.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val preferences = baseContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)

        val isLoggedIn = preferences.getBoolean("isLoggedIn", false)


        val index = intent.getIntExtra("index", 0)

        val menu = bnv.getMenu()


        if (index == 0){

        val selectedFragment = WeeklySpecialsView.newInstance()

        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()
            supportActionBar!!.title = "Main Store"

            val item = menu.getItem(0)
            item.setChecked(true)

                    //.selectedItemId(R.id.navigation_weeklyspecials)
       //     bnv.selectedItemId(R.id.navigation_weeklyspecials)
        }else if (index == 1){

            val selectedFragment = CategoryView.newInstance()

                    //MenuView.newInstance()

            supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()


            supportActionBar!!.title = "Main Store"

            val item = menu.getItem(1)
            item.setChecked(true)

        }else if (index == 2){

            if (isLoggedIn) {

                val selectedFragment = ProfileVIew.newInstance()

                supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

                supportActionBar!!.title = "Main Store"

                val item = menu.getItem(2)
                item.setChecked(true)

            }else{

                val selectedFragment = LoginView.newInstance()

                supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

                supportActionBar!!.title = "Main Store"

                val item = menu.getItem(2)
                item.setChecked(true)

            }



        }else if (index == 3){

            val selectedFragment = PromoView.newInstance()

            supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

            supportActionBar!!.title = "Main Store"

            val item = menu.getItem(3)
            item.setChecked(true)


        }else if (index == 4){

            val selectedFragment = RewardsView.newInstance()

            supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()


            supportActionBar!!.title = "Main Store"

            val item = menu.getItem(4)
            item.setChecked(true)

        }
    }

    fun createWeeklyFragment(){

        val selectedFragment = WeeklySpecialsView.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }

//    fun createMenuFragment(){
//
//        val selectedFragment = MenuView.newInstance()
//
//
//        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()
//
//    }

    fun createProfileFragment(){

        val selectedFragment = ProfileVIew.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }

    fun createLoginFragment(){

        val selectedFragment = LoginView.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }



    fun createPromoFragment(){

        val selectedFragment = PromoView.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }

    fun createRewardFragment(){

        val selectedFragment = RewardsView.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }

    fun createCategoryFragment(){

        val selectedFragment = CategoryView.newInstance()


        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()

    }

    fun logInNow(){

        val selectedFragment = ProfileVIew.newInstance()
        //supportFragmentManager.beginTransaction().add(R.id.frame_layout, ProfileVIew()).commit()

        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.frame_layout, selectedFragment, selectedFragment.getTag())
        transaction.commit()
    }

    fun logOutNow(){

        val selectedFragment = LoginView.newInstance()
        //supportFragmentManager.beginTransaction().add(R.id.frame_layout, ProfileVIew()).commit()

        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.frame_layout, selectedFragment, selectedFragment.getTag())
        transaction.commit()
    }

//    fun OnFragmentInteractionListener(uri: Uri) {
//        // TODO: Update argument type and name
//
//    }

}

