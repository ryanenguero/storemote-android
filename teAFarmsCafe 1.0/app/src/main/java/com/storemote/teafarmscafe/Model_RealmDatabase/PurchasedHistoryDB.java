package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class PurchasedHistoryDB extends RealmObject {

    public String amount_due;
    public String comment;
    public String comments;
    public String customer_name;
    public String email;
    public String invoice_number;
    public String qoute_number;
    public String sale_date;
    public String sale_id;
    public String sale_status;
    public String sale_time;
    public String user_id;

    public void setAmountDue(String amount_due) {

        this.amount_due = amount_due;
    }

    public String getAmountDue() {

        return amount_due;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public String getComment() {

        return comment;
    }

    public void setComments(String comments) {

        this.comments = comments;
    }

    public String getComments() {

        return comments;
    }

    public void setCustomerName(String customer_name) {

        this.customer_name = customer_name;
    }

    public String getCustomerName() {

        return customer_name;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getEmail() {

        return email;
    }

    public void setInvoiceNumber(String invoice_number) {

        this.invoice_number = invoice_number;
    }

    public String getInvoiceNumber() {

        return invoice_number;
    }

    public void setQouteNumber(String qoute_number) {

        this.qoute_number = qoute_number;
    }

    public String getQouteNumber() {

        return qoute_number;
    }

    public void setSalaDate(String sale_date) {

        this.sale_date = sale_date;
    }

    public String getSaleDate() {

        return sale_date;
    }

    public void setSaleId(String sale_id) {

        this.sale_id = sale_id;
    }

    public String getSaleId() {

        return sale_id;
    }

    public void setSaleStatus(String sale_status) {

        this.sale_status = sale_status;
    }

    public String getSaleStatus() {

        return sale_status;
    }

    public void setSaleTime(String sale_time) {

        this.sale_time = sale_time;
    }

    public String getSaleTime() {

        return sale_time;
    }

    public void setUserId(String user_id) {

        this.user_id = user_id;
    }

    public String getUserId() {

        return user_id;
    }
}
