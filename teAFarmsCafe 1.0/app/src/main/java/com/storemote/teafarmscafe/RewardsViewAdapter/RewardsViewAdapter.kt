package com.storemote.teafarmscafe.RewardsViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.Rewards
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import com.storemote.teafarmscafe.R

class RewardsViewAdapter (private val context: Context,
                          private val dataSource: ArrayList<Rewards>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.rewards_list_adapter, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.rewards_caption) as TextView

        val thumbnailImageView = rowView.findViewById(R.id.rewards_page_image) as ImageView

        val rewards = getItem(position) as Rewards

        nameTextView.text = rewards.name
//        addressTextView.text = weekly.menuProd.unitPrice as String
//        hoursTextView.text = "8am - 10pm"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${rewards.rewardBanner}").into(thumbnailImageView)

        return rowView
    }
}