package com.storemote.teafarmscafe


import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity

import android.os.Bundle
import android.os.PatternMatcher
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.storemote.teafarmscafe.Model_JSON.OrderListReponse
import com.storemote.teafarmscafe.Model_JSON.RegisterViewResponse

import com.storemote.teafarmscafe.Model_JSON.StoreListData
import com.storemote.teafarmscafe.Model_JSON.UserData
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.UserDataDB
import io.realm.Realm
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.login_view.*
import kotlinx.android.synthetic.main.user_registration.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.PasswordAuthentication
import java.util.HashMap
import java.util.regex.Pattern


class RegisterView : AppCompatActivity() {
    internal lateinit var apiInterface: APIInterface

//    private lateinit var listview: ListView

    private lateinit var realm: Realm
//    private lateinit var regview: RegisterViewResponse



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.user_registration)


        val txtEmail = findViewById(R.id.reg_email) as EditText
        val txtUsername = findViewById(R.id.reg_name) as EditText
        val txtPass = findViewById(R.id.reg_password) as EditText
        val txtCpass = findViewById(R.id.reg_confirm) as EditText
        val txtFname = findViewById(R.id.reg_fname) as EditText
        val txtMobile = findViewById(R.id.reg_mobno) as EditText
        val rBtn = findViewById(R.id.reg_submit) as Button
        val cBtn = findViewById(R.id.reg_cancel) as Button


        val builder = AlertDialog.Builder(this)

        Realm.init(this)
        realm = Realm.getDefaultInstance()

        rBtn.setOnClickListener {

            if (txtEmail.length() > 0 && txtUsername.length() > 0 && txtPass.length() > 0 && txtCpass.length() > 0 && txtFname.length() > 0 && txtMobile.length() > 0){


                if (isEmailValid(txtEmail.text)){

                    if (txtPass.text.toString() == txtCpass.text.toString()){

                        apiInterface = APIClient.getClient().create(APIInterface::class.java)


                      Log.d("MainActivity", "password matched")

                        val preferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                        val userPreferences = getSharedPreferences("userpreferences", Context.MODE_PRIVATE)

                        val map = HashMap<String, String>()

                         map["username"] = txtUsername.text.toString()
                        map["password"] = txtPass.text.toString()
                        map["email"] = txtEmail.text.toString()
                        map["full_name"] = txtFname.text.toString()
                        map["mobile_no"] = txtMobile.text.toString()
                        map["client_id"] = preferences.getString("clientId", "0").toString()

                        //
                        val call3 = apiInterface.doRegisterUser(map)
                        call3.enqueue(object : Callback<UserData> {

                            override fun onResponse(call: Call<UserData>, response: Response<UserData>) {
                                val userData = response.body()

                                val datumList = userData?.data

                                if (datumList != null) {

                                    builder.setTitle("Success")
                                    builder.setMessage("Registration successful")
                                    builder.setPositiveButton("Ok") { dialog, which ->

                                    }


                                    val dialog: AlertDialog = builder.create()
                                    dialog.show()


                                }

                            }

                            override fun onFailure(call: Call<UserData>, t: Throwable) {
                                call3.cancel()
                            }
                        })




                    }else{

//                        Log.d("MainActivity", "Password did not match")

                        builder.setTitle("Failed")
                        builder.setMessage("Password did not match")
                        builder.setPositiveButton("Ok") { dialog, which ->

                        }


                        val dialog: AlertDialog = builder.create()
                        dialog.show()


                    }

//                    Log.d("MainActivity", "email valid")

                }else{


//                    Log.d("MainActivity", "email invalid")

                    builder.setTitle("Failed")
                    builder.setMessage("Invalid email")
                    builder.setPositiveButton("Ok") { dialog, which ->

                    }


                    val dialog: AlertDialog = builder.create()
                    dialog.show()


                 }




            }else{

                Log.d("MainActivity", "Details Complete")

                builder.setTitle("Failed")
                builder.setMessage("Please complete the details")
                builder.setPositiveButton("Ok") { dialog, which ->

                }


                val dialog: AlertDialog = builder.create()
                dialog.show()
            }

        }
        cBtn.setOnClickListener {
            Log.d("MainActivity", "Cancel Register")
            finish()
        }


    }


        fun isEmailValid(email: CharSequence): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


}






















