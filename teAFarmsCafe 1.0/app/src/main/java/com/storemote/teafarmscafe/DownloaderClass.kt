package com.storemote.teafarmscafe

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle


import android.util.Log
import android.widget.*

//import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import com.storemote.teafarmscafe.Model_JSON.*
//import com.storemote.teafarmscafe.APIInterface

//import android.util.Log
//import java.util.ArrayList

import com.storemote.teafarmscafe.Model_RealmDatabase.*
import com.storemote.teafarmscafe.Model_RealmDatabase.Rewards
import com.storemote.teafarmscafe.Model_JSON.Promos
import com.storemote.teafarmscafe.StoreListAdapter.StoreListAdapt
//import com.storemote.teafarmscafe.APIClient.APIInterface
import io.realm.Realm
import io.realm.RealmResults
import java.lang.Error
import java.util.*
import kotlin.collections.ArrayList


class DownloaderClass {

    internal lateinit var apiInterface: APIInterface
//    internal lateinit var realm: Realm
    internal lateinit var landingData: RealmResults<LandingPageDB>
    internal lateinit var category: RealmResults<CategoryDB>
    internal lateinit var freeItems: RealmResults<FreeItemsDB>
    internal lateinit var menuProduct: RealmResults<MenuProductDB>
    internal lateinit var promoList: RealmResults<PromoListDB>
    internal lateinit var promos: RealmResults<PromosDB>
    internal lateinit var purchasedHistory: RealmResults<PurchasedHistoryDB>
    internal lateinit var rewards: RealmResults<Rewards>
    internal lateinit var rewardsCard: RealmResults<RewardsCardDB>
    internal lateinit var rewardsDetails: RealmResults<RewardsDetailsDB>
    internal lateinit var storeList: RealmResults<StoreListDB>
    internal lateinit var userData: RealmResults<UserDataDB>
    internal lateinit var weeklySpecials: RealmResults<WeeklySpecials>
    internal lateinit var registerToPusher: RealmResults<RegisterToPusherDB>






    fun downloadLandingPagedata(clientId: String, storeId: String, context: Context, realm: Realm, intent: Intent) {


//        realm.where(LandingPageDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(LandingPageDB::class.java)
        realm.commitTransaction();

        landingData = realm.where(LandingPageDB::class.java).findAllAsync()


       apiInterface = APIClient.getClient().create(APIInterface::class.java)

       val map = HashMap<String, String>()

     map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
     map["client_id"] = clientId
     map["store_id"] = storeId

     val call3 = apiInterface.doDownloadLandingPageData(map)
     call3.enqueue(
        object : Callback<LandingPage> {



        override fun onResponse(call: Call<LandingPage>, response: Response<LandingPage>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
            val landingPageData = response.body()

            val data = landingPageData!!.data

//            data.add(landingPageData)
                    //landingPageData

            if (data != null) {

//                for (da in 0 until datumList.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                Log.d("MainActivity", "ResponseLandingPageData =  ${data[0]!!.menu}");

                realm.executeTransaction { r ->
                    // Add a person.
                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                    val landingDataDb = r.createObject(LandingPageDB::class.java)

                    landingDataDb.clientId = data[0]!!.client_id
                    landingDataDb.menu = data[0]!!.menu
                    landingDataDb.storeId = data[0]!!.store_id
                    landingDataDb.promo = data[0]!!.promo
                    landingDataDb.rewards = data[0]!!.rewards
                    landingDataDb.weeklySpecial = data[0]!!.weekly_special

                }


//                StoreList().startLandingPage()
//                storeList.startLandingPage()
//                intent.putExtra("storeName", )
                context.startActivity(intent)



            }
        }



            override fun onFailure(call: Call<LandingPage>, t: Throwable) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                call3.cancel()
            }



        })



    }

    fun downloadProductdata(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(MenuProductDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(MenuProductDB::class.java)
        realm.delete(MenuProductExceptionDB::class.java)
        realm.commitTransaction();

        menuProduct = realm.where(MenuProductDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["store_id"] = storeId

        val call3 = apiInterface.doDownloadProductData(map)
        call3.enqueue(
                object : Callback<MenuProduct> {

                    override fun onResponse(call: Call<MenuProduct>, response: Response<MenuProduct>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val menuProdData = response.body()

                        val data = menuProdData!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseProductData =  ${data[da]!!.name}");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val menuProductDTB = r.createObject(MenuProductDB::class.java)

                                    menuProductDTB.category = data[da].category
                                    menuProductDTB.client_id = data[da].client_id
                                    menuProductDTB.costPrice = data[da].cost_price
                                    menuProductDTB.createdBy = data[da].created_by
                                    menuProductDTB.createdDate = data[da].created_date
                                    menuProductDTB.versionNo= data[da].version_no
                                    menuProductDTB.itemId = data[da].item_id
                                    menuProductDTB.itemNumber = data[da].item_number
                                    menuProductDTB.itemType = data[da].item_type
                                    menuProductDTB.name = data[da].name
                                    menuProductDTB.picFilename = data[da].pic_filename
                                    menuProductDTB.description = data[da].description;
                                    menuProductDTB.receivingQuantity = data[da].receiving_quantity
                                    menuProductDTB.reorderLevel = data[da].reorder_level
                                    menuProductDTB.stockType = data[da].stock_type
                                    menuProductDTB.storeId = data[da].store_id
                                    menuProductDTB.supplierId = data[da].supplier_id
                                    menuProductDTB.taxCategoryId = data[da].tax_category_id
                                    menuProductDTB.unitPrice = data[da].unit_price
                                    menuProductDTB.updatedBy = data[da].updated_by
                                    menuProductDTB.updatedDate = data[da].updated_date

                                    val menuExceptionDTB = r.createObject(MenuProductExceptionDB::class.java)

                                    menuExceptionDTB.menuProd =  menuProductDTB

                                    menuProductDTB.menuProductException = menuExceptionDTB



                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<MenuProduct>, t: Throwable) {
//                      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        Log.d("MainActivity", "Response Product Error =  $t ");
                        call3.cancel()
                    }


                })


        this.downloadCategoryData(clientId, storeId, context, realm)

    }


    fun downloadCategoryData(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(CategoryDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(CategoryDB::class.java)
        realm.commitTransaction();

        category = realm.where(CategoryDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["store_id"] = storeId

        val call3 = apiInterface.doDownloadCategoryData(map)
        call3.enqueue(
                object : Callback<Category> {



                    override fun onResponse(call: Call<Category>, response: Response<Category>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val categoryDat = response.body()

                        val data = categoryDat!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseCategory = $data");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val categoryDTB = r.createObject(CategoryDB::class.java)

                                    categoryDTB.categoryId = data[da].category_id
                                    categoryDTB.name = data[da].name




                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<Category>, t: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })

    }

    fun downloadWeeklySpecials(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(WeeklySpecials::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(WeeklySpecials::class.java)
        realm.commitTransaction();

        weeklySpecials = realm.where(WeeklySpecials::class.java).findAllAsync()

        menuProduct = realm.where(MenuProductDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["store_id"] = storeId

        val call3 = apiInterface.doDownloadWeeklySpecials(map)
        call3.enqueue(object : Callback<MenuProduct> {



                    override fun onResponse(call: Call<MenuProduct>, response: Response<MenuProduct>) {
                Log.d("MainActivity", "ResponseBodyError = " + response.isSuccessful);


                        val menuProdData = response.body()

                        val data = menuProdData!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseWeeklySpecials =  ${data[da]!!.name}");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val menuProductDTB = r.createObject(MenuProductDB::class.java)

                                    menuProductDTB.category = data[da].category
                                    menuProductDTB.client_id = data[da].client_id
                                    menuProductDTB.costPrice = data[da].cost_price
                                    menuProductDTB.createdBy = data[da].created_by
                                    menuProductDTB.createdDate = data[da].created_date
                                    menuProductDTB.versionNo= data[da].version_no
                                    menuProductDTB.itemId = data[da].item_id
                                    menuProductDTB.itemNumber = data[da].item_number
                                    menuProductDTB.itemType = data[da].item_type
                                    menuProductDTB.name = data[da].name
                                    menuProductDTB.picFilename = data[da].pic_filename
                                    menuProductDTB.description = data[da].description;
                                    menuProductDTB.receivingQuantity = data[da].receiving_quantity
                                    menuProductDTB.reorderLevel = data[da].reorder_level
                                    menuProductDTB.stockType = data[da].stock_type
                                    menuProductDTB.storeId = data[da].store_id
                                    menuProductDTB.supplierId = data[da].supplier_id
                                    menuProductDTB.taxCategoryId = data[da].tax_category_id
                                    menuProductDTB.unitPrice = data[da].unit_price
                                    menuProductDTB.updatedBy = data[da].updated_by
                                    menuProductDTB.updatedDate = data[da].updated_date

                                    val weeklySpecialsDTB = r.createObject(WeeklySpecials::class.java)

                                    weeklySpecialsDTB.menuProd =  menuProductDTB

                                    menuProductDTB.weeklySpecials = weeklySpecialsDTB
                                            //.setWeeklySpecials(weeklySpecialsDTB)


                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<MenuProduct>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        Log.d("MainActivity", "Weekly Error =  ${t.cause}");

                        call3.cancel()
                    }

                })

    }

    fun downloadPromos(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(PromosDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(PromosDB::class.java)
        realm.commitTransaction();

        promos = realm.where(PromosDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
                //clientId
        map["store_id"] = storeId

                //storeId

        val call3 = apiInterface.doDownloadPromos(map)
        call3.enqueue( object : Callback<Promos> {

//                    Log.d("MainActivity", "Promos");


                    override fun onResponse(call: Call<Promos>, response: Response<Promos>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val promosData = response.body()

                        Log.d("MainActivity", "promospromos =  ${promosData}");
                                //.body()

                        val data = promosData!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponsePromos =  ${data[da]!!.name}");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val promosDTB = r.createObject(PromosDB::class.java)

                                    promosDTB.discountPercent = data[da].discount_percent
                                    promosDTB.fromDate = data[da].from_date
//                                    promosDTB.itemFreeId = 0.toDouble()
                                            //data[da].item_free_id
                                    promosDTB.itemId = data[da].item_id
                                    promosDTB.itemName = data[da].name
                                    promosDTB.nooffreeitems = data[da].no_of_free_items
                                    promosDTB.promoId = data[da].promo_id
                                    promosDTB.promoTypeId = data[da].promo_type_id
                                    promosDTB.specificPrice = data[da].specific_price
                                    promosDTB.toDate = data[da].to_date



                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<Promos>, t: Throwable) {
//
                        call3.cancel()
                    }

                })



    }

    fun downloadPromosList(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(PromoListDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(PromoListDB::class.java)
        realm.commitTransaction();

        promoList = realm.where(PromoListDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["store_id"] = storeId

        val call3 = apiInterface.doDownloadPromosList(map)
        call3.enqueue(
                object : Callback<PromoList> {



                    override fun onResponse(call: Call<PromoList>, response: Response<PromoList>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val promosData = response.body()

                        val data = promosData!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponsePromoList =  ${data[da]!!.name}");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val promosListDTB = r.createObject(PromoListDB::class.java)

                                    promosListDTB.fromDate = data[da].from_date
                                    promosListDTB.promoId = data[da].promo_id
                                    promosListDTB.toDate = data[da].to_date
                                    promosListDTB.name = data[da].name
                                    promosListDTB.promoBanner = data[da].promo_banner



                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<PromoList>, t: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })

    }

    fun downloadRewards(clientId: String, storeId: String, context: Context, realm: Realm) {

//        realm.where(Rewards::class.java).findAll().deleteAllFromRealm()



        rewards = realm.where(Rewards::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["store_id"] = storeId

        val call3 = apiInterface.doDownloadRewards(map)
        call3.enqueue(
                object : Callback<com.storemote.teafarmscafe.Model_JSON.Rewards> {



                    override fun onResponse(call: Call<com.storemote.teafarmscafe.Model_JSON.Rewards>, response: Response<com.storemote.teafarmscafe.Model_JSON.Rewards>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val rewardsData = response.body()

                        val data = rewardsData!!.data

                        if (data != null) {

                            realm.beginTransaction()
                            realm.delete(Rewards::class.java)
                            realm.commitTransaction()

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseRewards =  ${data[da]!!.name}");

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val rewardsDTB = r.createObject(Rewards::class.java)

                                    rewardsDTB.fromDate = data[da].from_date
                                    rewardsDTB.toDate = data[da].to_date
                                    rewardsDTB.name = data[da].name
                                    rewardsDTB.discountPercent = data[da].discount_percent
                                    rewardsDTB.nooffreeitems = data[da].no_of_free_items
                                    rewardsDTB.price = data[da].price
                                    rewardsDTB.rewardBanner = data[da].reward_banner
                                    rewardsDTB.rewardId = data[da].reward_id
                                    rewardsDTB.specificPrice = data[da].specific_price




                                }

//                                downloadRewardsDetails(clientId, storeId, context, realm, data[da].reward_id)
                            }
                        }
                    }

                    override fun onFailure(call: Call<com.storemote.teafarmscafe.Model_JSON.Rewards>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })

    }

//    fun downloadRewardsDetails(clientId: String, storeId: String, context: Context, realm: Realm, rewardsId: String) {
//
////        realm.where(RewardsDetailsDB::class.java).findAll().deleteAllFromRealm()
//
//        realm.beginTransaction()
//        realm.delete(RewardsDetailsDB::class.java)
//        realm.commitTransaction()
//
//
//        rewardsDetails = realm.where(RewardsDetailsDB::class.java).findAllAsync()
//
//
//        apiInterface = APIClient.getClient().create(APIInterface::class.java)
//
//        val map = HashMap<String, String>()
//
////        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
//        map["client_id"] = clientId
//        map["store_id"] = storeId
//        map["reward_id"] = rewardsId
//
//        val call3 = apiInterface.doDownloadRewardsDetails(map)
//        call3.enqueue(
//                object : Callback<RewardsDetails> {
//
//
//
//                    override fun onResponse(call: Call<RewardsDetails>, response: Response<RewardsDetails>) {
////                Log.d("MainActivity", "ResponseBody = " + response.body());
//                        val rewardsDetailData = response.body()
//
//                        val data = rewardsDetailData!!.data
//
//                        if (data != null) {
//
//                            for (da in 0 until data.size) {
////                        Log.d("MainActivity", "ResponseData =  ${datum.name}");
//
//
//                                Log.d("MainActivity", "ResponseRewardDetails =  ${data[da]!!.item_id}");
//
//                                realm.executeTransaction { r ->
//                                    // Add a person.
//                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
////                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
//                                    val rewardsDTB = r.createObject(RewardsDetailsDB::class.java)
//
////                                    rewardsDTB.fromDate = data[da].from_date
////                                    rewardsDTB.toDate = data[da].to_date
//                                    rewardsDTB.discountPercent = data[da].discount_percent
//                                    rewardsDTB.nooffreeitems = data[da].no_of_free_items
//                                    rewardsDTB.rewardId = data[da].reward_id
//                                    rewardsDTB.specificPrice = data[da].specific_price
//                                    rewardsDTB.availableQuantity = data[da].available_quantity
//                                    rewardsDTB.itemId = data[da].item_id
//
//
//
//                                }
//                            }
//
////                            NotificationCenter.defaultCenter().postNotification("loadRewardDetails")
//                        }
//                    }
//
//                    override fun onFailure(call: Call<RewardsDetails>, t: Throwable) {
////                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                        call3.cancel()
//                    }
//
//                })
//    }

    fun downloadRewardsStamp(store_id: String, client_id: String, username: String, password: String, fb: String, google: String, context: Context, realm: Realm) {

//        realm.where(RewardsDetailsDB::class.java).findAll().deleteAllFromRealm()

        realm.beginTransaction()
        realm.delete(RewardsCardDB::class.java)
        realm.commitTransaction()


        rewardsCard = realm.where(RewardsCardDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)




        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["store_id"] = store_id
        map["client_id"] = client_id
        map["username"] = username
        map["password"] = password
        map["is_fb"] = fb
        map["is_google"] = google

        val call3 = apiInterface.doDownloadRewardsStamp(map)
        call3.enqueue(
                object : Callback<RewardsCard> {



                    override fun onResponse(call: Call<RewardsCard>, response: Response<RewardsCard>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val rewardsStampData = response.body()

                        val data = rewardsStampData!!.data

                        if (data != null) {
                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseRewardsStamp =  ${data[da]!!.card_points}")

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val rewardStampDTB = r.createObject(RewardsCardDB::class.java)

                                    rewardStampDTB.cardPoints = data[da].card_points
                                    rewardStampDTB.pointsToCollect = data[da].points_to_collect
                                    rewardStampDTB.rewardCardId = data[da].reward_card_id





                                }
//                                downloadRewardsStamp(store_id, client_id, username, password, fb, google, context, realm)
                            }
                        }
                    }

                    override fun onFailure(call: Call<RewardsCard>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })
    }


    fun registerApptoPusher(device_token: String, client_id: String, realm: Realm) {

        realm.beginTransaction()
        realm.delete(RegisterToPusherDB::class.java)
        realm.commitTransaction()

        registerToPusher = realm.where(RegisterToPusherDB::class.java).findAllAsync()

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()

//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = client_id
        map["device_token"] = device_token

        val call3 = apiInterface.doRegisterToPusher(map)
        call3.enqueue(
                object : Callback<RegisterToPusher> {
                    override fun onResponse(call: Call<RegisterToPusher>, response: Response<RegisterToPusher>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val rewardsStampData = response.body()

                        val data = rewardsStampData!!.data

                        if (data != null) {

                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "ResponseRewardsStamp =  ${data[da]!!.channel}")

                                realm.executeTransaction { r ->
                                    // Add a person.
                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                    val registerToPusherSave = r.createObject(RegisterToPusherDB::class.java)

                                    registerToPusherSave.setChannel(data[da].channel)
                                            //.channel = data[da].channel
                                    registerToPusherSave.event = data[da].event
                                    registerToPusherSave.key = data[da].key

                                    Log.d("MainActivity", "Edi wow")


                                }
//                                downloadRewardsStamp(store_id, client_id, username, password, fb, google, context, realm, rewards)
                            }



                                    //.postNotification("connectToPusher")

                        }

                        NotificationCenter.defaultCenter().postNotification("connectToPusher")

                    }

                    override fun onFailure(call: Call<RegisterToPusher>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }



                })



    }


    fun doDownloadPurchasedHistory(clientId: String, username: String, password: String, is_fb: String, is_google: String, realm: Realm){

        realm.beginTransaction()
        realm.delete(PurchasedHistoryDB::class.java)
        realm.commitTransaction()


        purchasedHistory = realm.where(PurchasedHistoryDB::class.java).findAllAsync()


        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()
//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["client_id"] = clientId
        map["username"] = username
        map["password"] = password
        map["is_fb"] = is_fb
        map["is_google"] = is_google

        val call3 = apiInterface.doDownloadPurchasedHistory(map)
        call3.enqueue(
                object : Callback<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory> {



                    override fun onResponse(call: Call<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>, response: Response<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                        val rewardsStampData = response.body()

                        val data = rewardsStampData!!.data

                        if (data != null) {
                            for (da in 0 until data.size) {
//                        Log.d("MainActivity", "ResponseData =  ${datum.name}");


                                Log.d("MainActivity", "purchased =  ${data[da]!!.amount_due}")

                                    realm.executeTransaction { r ->
                                        // Add a person.
                                        // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
//                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
                                        val purchased = r.createObject(PurchasedHistoryDB::class.java)

                                        purchased.amountDue = data[da]!!.amount_due
                                        purchased.comment = data[da]!!.comment
                                        purchased.comments = data[da]!!.comments
                                        purchased.customer_name = data[da]!!.customer_name
                                        purchased.email = data[da]!!.email
                                        purchased.invoice_number = data[da]!!.invoice_number
                                        purchased.qoute_number = data[da]!!.qoute_number
                                        purchased.sale_date = data[da]!!.sale_date
                                        purchased.sale_id = data[da]!!.sale_id
                                        purchased.sale_status = data[da]!!.sale_status
                                        purchased.sale_time = data[da]!!.sale_time
                                        purchased.user_id = data[da]!!.user_id
                                    }


//                                downloadRewardsStamp(store_id, client_id, username, password, fb, google, context, realm)
//                                NotificationCenter.defaultCenter().postNotification("refreshPurchasedHistory")
                            }
                        }
                    }

                    override fun onFailure(call: Call<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })


    }


//    fun doDownloadPurchasedHistory(clientId: String, username: String, password: String, is_fb: String, is_google: String, realm: Realm){
//
//        realm.beginTransaction()
//        realm.delete(PurchasedHistoryDB::class.java)
//        realm.commitTransaction()
//
//
//        purchasedHistory = realm.where(PurchasedHistoryDB::class.java).findAllAsync()
//
//
//        apiInterface = APIClient.getClient().create(APIInterface::class.java)
//
//        val map = HashMap<String, String>()
////        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
//        map["client_id"] = clientId
//        map["username"] = username
//        map["password"] = password
//        map["is_fb"] = is_fb
//        map["is_google"] = is_google
//
//        val call3 = apiInterface.doDownloadPurchasedHistory(map)
//        call3.enqueue(
//                object : Callback<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory> {
//
//
//
//                    override fun onResponse(call: Call<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>, response: Response<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>) {
////                Log.d("MainActivity", "ResponseBody = " + response.body());
//                        val rewardsStampData = response.body()
//
//                        val data = rewardsStampData!!.data
//
//                        if (data != null) {
//                            for (da in 0 until data.size) {
////                        Log.d("MainActivity", "ResponseData =  ${datum.name}");
//
//
//                                Log.d("MainActivity", "purchased =  ${data[da]!!.amount_due}")
//
//                                realm.executeTransaction { r ->
//                                    // Add a person.
//                                    // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.
////                            val storelistDat = r.createObject(StoreListDB::class.java, 1)
//                                    val purchased = r.createObject(PurchasedHistoryDB::class.java)
//
//                                    purchased.amountDue = data[da]!!.amount_due
//                                    purchased.comment = data[da]!!.comment
//                                    purchased.comments = data[da]!!.comments
//                                    purchased.customer_name = data[da]!!.customer_name
//                                    purchased.email = data[da]!!.email
//                                    purchased.invoice_number = data[da]!!.invoice_number
//                                    purchased.qoute_number = data[da]!!.qoute_number
//                                    purchased.sale_date = data[da]!!.sale_date
//                                    purchased.sale_id = data[da]!!.sale_id
//                                    purchased.sale_status = data[da]!!.sale_status
//                                    purchased.sale_time = data[da]!!.sale_time
//                                    purchased.user_id = data[da]!!.user_id
//                                }
//
//
////                                downloadRewardsStamp(store_id, client_id, username, password, fb, google, context, realm)
////                                NotificationCenter.defaultCenter().postNotification("refreshPurchasedHistory")
//                            }
//                        }
//                    }
//
//                    override fun onFailure(call: Call<com.storemote.teafarmscafe.Model_JSON.PurchasedHistory>, t: Throwable) {
////                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                        call3.cancel()
//                    }
//
//                })
//
//
//    }

    ///////
    fun doDownloadRewardsCardItems(storeId: String, token: String, realm: Realm){

        realm.beginTransaction()
        realm.delete(RewardsCardItemsDB::class.java)
        realm.commitTransaction()

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val map = HashMap<String, String>()
//        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
        map["store_id"] = storeId
        map["token"] = token

        val call3 = apiInterface.doDownloadRewardsCardItems(map)
        call3.enqueue(
                object : Callback<RewardsCardItems> {

                    override fun onResponse(call: Call<RewardsCardItems>, response: Response<RewardsCardItems>) {
                        val rewardsStampData = response.body()
                        val data = rewardsStampData!!.data
                        if (data != null) {
                            for (da in 0 until data.size) {
                                realm.executeTransaction { r ->
                                    val items = r.createObject(RewardsCardItemsDB::class.java)

                                    items.itemId = data[da].item_id
                                    items.name = data[da].name
                                    items.description = data[da].description
                                    items.picFilename = data[da].pic_filename
                                    items.storeId = data[da].store_id
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<RewardsCardItems>, t: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        call3.cancel()
                    }

                })


    }
}