package com.storemote.teafarmscafe

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.telecom.Call
import android.util.Log
import android.view.Gravity
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.storemote.teafarmscafe.Model_JSON.OrderListReponse
import com.storemote.teafarmscafe.Model_JSON.StoreListData
import com.storemote.teafarmscafe.Model_JSON.UserData
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.UserDataDB
import com.storemote.teafarmscafe.OrderListAdapter.OrderListAdapter
import io.realm.Realm
import retrofit2.Response
import java.util.*
import javax.security.auth.callback.Callback

class OrderListView : AppCompatActivity(), LoginView.OnFragmentInteractionListener {

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var realm: Realm
    private lateinit var listview: ListView
    var total : Float = 0.0f
    var totalPointSpent : Float = 0.0f

    var isGoogle : Boolean = false
    var isFB : Boolean = false
    var mode : Int = 1

    internal lateinit var apiInterface: APIInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.order_list_view)

        supportActionBar!!.title = "Order Cart"

        listview = findViewById<ListView>(R.id.orderListView)

        val builder = AlertDialog.Builder(this)
        val builder1 = AlertDialog.Builder(this)
        val builder2 = AlertDialog.Builder(this)

        val builderFailed = AlertDialog.Builder(this)


        Realm.init(this)

        realm = Realm.getDefaultInstance()

        val arrList = ArrayList<OrderListDB>(realm.where(OrderListDB::class.java).findAll())
        val userData = ArrayList<UserDataDB>(realm.where(UserDataDB::class.java).findAll())

        val adapt = OrderListAdapter(this, arrList)
        listview.adapter = adapt

            val orderData = ArrayList<Any>()

            for (i in 0 until arrList.size) {

                val orderList = arrList[i]

                val orderDict = HashMap<String, Any>()

                orderDict["quantity"] = orderList.quantity
                orderDict["description"] = orderList.specialInstruction
                orderDict["item_id"] = orderList.itemId
                orderDict["is_promo"] = orderList.isPromo
                orderDict["is_reward"] = orderList.isReward
                orderDict["unit_price"] = orderList.unitPrice

                if (orderList.promoId != null) {
                    orderDict["promo_id"] = orderList.promoId
                }

                if (orderList.rewardId != null) {
                    orderDict["reward_id"] = orderList.rewardId
                }

                orderData.add(orderDict)

            }




        for (i in 0 until arrList.size) {

            val orderList = arrList[i]

            if (orderList.isReward == "0") {


                val amount = orderList.payment.toFloat()

                total += amount

            }else {

               val amountPoints = orderList.totalPointSpent.toFloat()

               totalPointSpent += amountPoints
            }


        }

        val lblTotal = findViewById(R.id.lblTotal) as TextView
        val total1 = total + totalPointSpent
        val totalStr = "₱" + total1.toString()

        lblTotal.text = totalStr


        val payBtn = findViewById(R.id.payBtn) as Button
        val backBtn = findViewById(R.id.backBtn) as Button

        apiInterface = APIClient.getClient().create(APIInterface::class.java)

        payBtn.setOnClickListener {
            payBtn.isEnabled = false

            if (userData.size != 0) {

                if (arrList.size != 0) {

//                    val arr = arr
//                            //arrayOf{"Pay at cashier","Use credits","Cancel"}

                    builder.setTitle("Payment")
                    builder.setMessage("Please select payment type")
//                    builder.setItems
//                    builder.setPositiveButton("Ok") { dialog, which ->
//
//                    }

                    builder.setPositiveButton("Pay at cashier")  { dialog, which ->
                        dialog.dismiss()

                        val map = HashMap<String, Any>()

                        val orderList = arrList[0]
                        val user = userData[0]

                        val androidId = Settings.Secure.getString(contentResolver,
                                Settings.Secure.ANDROID_ID)

                        map["orders"] = orderData
                        map["username"] = user.username
                        map["password"] = user.password
                        map["client_id"] = orderList.clientId
                        map["store_id"] = orderList.storeId
                        map["is_fb"] = isFB
                        map["is_google"] = isGoogle
                        map["total_amount"] = total
                        map["total_point_spent"] = totalPointSpent
                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["device_token"] = androidId

                        Log.d("MainActivity", "Submit Order with Data: $map")

                        val call3 = apiInterface.doOrderListResponse(map)
                        call3.enqueue(object : retrofit2.Callback<OrderListReponse> {

                            override fun onResponse(call: retrofit2.Call<OrderListReponse>, response: Response<OrderListReponse>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                                val status = response.body()

                                Log.d("MainActivity", "Submit Order with status: ${status!!.status_message}")

                                builder1.setTitle("Success")
                                builder1.setMessage(status!!.status_message)
                                builder1.setPositiveButton("Ok") { dialog, which ->

                                    realm.beginTransaction()
                                    realm.delete(OrderListDB::class.java)
                                    realm.commitTransaction();

                                    listview.refreshDrawableState()

                                    payBtn.isEnabled = true

                                }


                                val dialog: AlertDialog = builder1.create()
                                dialog.show()

//                                listview.clear()

                                arrList.clear()
                                adapt.notifyDataSetChanged()

                            }

                            override fun onFailure(call: retrofit2.Call<OrderListReponse>, t: Throwable) {
                                call.cancel()
                                payBtn.isEnabled = true
                            }

                        })


                    }

                    builder.setNegativeButton("Use credits")  { dialog, which ->
                        dialog.dismiss()

                        val map = HashMap<String, Any>()

                        val orderList = arrList[0]
                        val user = userData[0]

                        val androidId = Settings.Secure.getString(contentResolver,
                                Settings.Secure.ANDROID_ID)

                        map["orders"] = orderData
                        map["username"] = user.username
                        map["password"] = user.password
                        map["client_id"] = orderList.clientId
                        map["store_id"] = orderList.storeId
                        map["is_fb"] = isFB
                        map["is_google"] = isGoogle
                        map["total_amount"] = total
                        map["total_point_spent"] = totalPointSpent
                        map["token"] = "e71b36f5516429b298ef4d490f96c47bec2d4dc559cc93d0668b3a109281e1c8691646486269026199ff8b8093832e11006f361722dbab488556e40bc213075aA9PUCTpcbw2hy9sxRx08uH5lQOCyDmvRK0Nu2b6mHgo="
                        map["device_token"] = androidId

                        Log.d("MainActivity", "Submit Order with Data: $map")

                        val call3 = apiInterface.doOrderListUseCreditsResponse(map)
                        call3.enqueue(object : retrofit2.Callback<OrderListReponse> {

                            override fun onResponse(call: retrofit2.Call<OrderListReponse>, response: Response<OrderListReponse>) {
//                Log.d("MainActivity", "ResponseBody = " + response.body());
                                val status = response.body()

                                Log.d("MainActivity", "Submit Order with status: ${status!!.status_message}")


                                builder2.setTitle("Success")
                                builder2.setMessage(status!!.status_message)
                                builder2.setPositiveButton("Ok") { dialog, which ->

                                    realm.beginTransaction()
                                    realm.delete(OrderListDB::class.java)
                                    realm.commitTransaction();

                                    listview.refreshDrawableState()

                                    payBtn.isEnabled = true
                                }


                                val dialog: AlertDialog = builder2.create()
                                dialog.show()

                                arrList.clear()
                                adapt.notifyDataSetChanged()


                            }

                            override fun onFailure(call: retrofit2.Call<OrderListReponse>, t: Throwable) {
                                call.cancel()
                                payBtn.isEnabled = true
                            }

                        })


                    }

                    builder.setNeutralButton("Cancel")  { dialog, which ->
                        dialog.dismiss()

                        payBtn.isEnabled = true
                    }

                    val dialog: AlertDialog = builder.create()

//                    val mw = dialog.findViewById<TextView>(android.R.id.message)
//                    mw!!.gravity = Gravity.CENTER

                    dialog.show()






//                    finish()

                }else{

                    //                val builder = AlertDialog.Builder(this)
                    builderFailed.setTitle("Failed")
                    builderFailed.setMessage("Please add items first")
                    builderFailed.setPositiveButton("OK") { dialog, which ->

                        payBtn.isEnabled = true
                    }

                    val dialog1: AlertDialog = builderFailed.create()
                    dialog1.show()
                }

            }else{

//                val builder = AlertDialog.Builder(this)
                builder.setTitle("Failed")
                builder.setMessage("Please log in first to continue this transaction")
                builder.setPositiveButton("OK") { dialog, which ->
                    //Toast.makeText(applicationContext,"continuar",Toast.LENGTH_SHORT).show()



 //                    val selectedFragment = LoginView.newInstance()
//
//                    val intentt = Intent(this, selectedFragment::class.java)
//
//                    startActivity(intentt)
                    payBtn.isEnabled = true


//                    supportFragmentManager.beginTransaction().replace(R.id.frame_layout, selectedFragment).commit()


                }

                val dialog: AlertDialog = builder.create()
                dialog.show()



            }

        }


        backBtn.setOnClickListener {

            Log.d("MainActivity", "Cancel Order");

            finish()
        }

    }


}