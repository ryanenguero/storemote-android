package com.storemote.teafarmscafe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_JSON.Rewards
import com.storemote.teafarmscafe.Model_RealmDatabase.MenuProductExceptionDB
import com.storemote.teafarmscafe.Model_RealmDatabase.RewardsDetailsDB
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import io.realm.Realm
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class RewardsLisAdapter  (private val context: Context,
                          private val dataSource: ArrayList<RewardsDetailsDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.weekly_specials_adapter, parent, false)

        // Get title element
        val nameTextView = rowView.findViewById(R.id.weekly_name) as TextView

// Get subtitle element
        val addressTextView = rowView.findViewById(R.id.weekly_price) as TextView

// Get detail element
//        val hoursTextView = rowView.findViewById(R.id.store_list_hours) as TextView

// Get thumbnail element
        val thumbnailImageView = rowView.findViewById(R.id.weekly_image) as ImageView

        val rewards = getItem(position) as RewardsDetailsDB


        Realm.init(this.context)
        // Create the Realm instance
        val realm = Realm.getDefaultInstance()

        val menu = ArrayList<MenuProductExceptionDB>(realm.where(MenuProductExceptionDB::class.java).findAll())

        for (da in 0 until menu.size) {
            if (rewards.itemId.toString() == menu[da].menuProd.itemId.toString()) {

                nameTextView.text = menu[da].menuProd.name
//                addressTextView.text = menu[da].menuProd.

                if (rewards.discountPercent != 0.0f && rewards.nooffreeitems == 0.toDouble() && rewards.specificPrice == 0.0f){

                    val price = (menu[da].menuProd.unitPrice.toDouble()-(menu[da].menuProd.unitPrice.toDouble()*rewards.discountPercent/100)).toString()

                    addressTextView.text = "₱" + price

                }else if (rewards.discountPercent == 0.0f && rewards.nooffreeitems != 0.toDouble() && rewards.specificPrice == 0.0f){

                    addressTextView.text = "₱" + rewards.specificPrice.toString()

                }else if (rewards.discountPercent == 0.0f && rewards.nooffreeitems == 0.toDouble() && rewards.specificPrice != 0.0f){


                    addressTextView.text = "₱" + rewards.specificPrice.toString()

                }else if (rewards.discountPercent != 0.0f && rewards.nooffreeitems != 0.toDouble() && rewards.specificPrice == 0.0f){

                    val price = (menu[da].menuProd.unitPrice.toDouble()-(menu[da].menuProd.unitPrice.toDouble()*rewards.discountPercent/100)).toString()

                    addressTextView.text = "₱" + price

                }else if (rewards.discountPercent == 0.0f && rewards.nooffreeitems != 0.toDouble() && rewards.specificPrice != 0.0f){

                    addressTextView.text = "₱" + rewards.specificPrice.toString()

                }else if (rewards.discountPercent != 0.0f && rewards.nooffreeitems == 0.toDouble() && rewards.specificPrice != 0.0f){

                    val price = (rewards.specificPrice-(rewards.specificPrice*rewards.discountPercent/100)).toString()

                    addressTextView.text = "₱" + price

                }else if (rewards.discountPercent != 0.0f && rewards.nooffreeitems != 0.toDouble() && rewards.specificPrice != 0.0f){


                    val price = (rewards.specificPrice-(rewards.specificPrice*rewards.discountPercent/100)).toString()

                    addressTextView.text = "₱" + price

                }


                Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${menu[da].menuProd.picFilename}")
                        .transform(CropCircleTransformation())
                        .into(thumbnailImageView)


            }

        }
//        nameTextView.text = rewards.
//        addressTextView.text ="₱" + weekly.price as String
//        hoursTextView.text = "8am - 10pm"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)


//        Picasso.with(this.context).load("http://staging.storemote.net/assets/uploads/item_pics/${weekly.menuProd.picFilename}").into(thumbnailImageView)



        return rowView
    }
}