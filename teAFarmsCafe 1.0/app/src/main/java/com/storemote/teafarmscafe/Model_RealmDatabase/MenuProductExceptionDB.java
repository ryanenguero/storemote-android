package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class MenuProductExceptionDB extends RealmObject  {


    private MenuProductDB menuProd;

    public void setMenuProd(MenuProductDB menuProd) {

        this.menuProd = menuProd;
    }

    public MenuProductDB getMenuProd() {

        return menuProd;
    }

}
