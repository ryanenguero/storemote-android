package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuProduct {

    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {
//        @SerializedName("category")
//        public String category;
//
//        @SerializedName("client_id")
//        public String client_id;
//
//        @SerializedName("cost_price")
//        public Float cost_price;
//
//        @SerializedName("created_by")
//        public String created_by;
//
//        @SerializedName("created_date")
//        public Date created_date;
//
//        @SerializedName("item_id")
//        public Integer item_id;
//
//        @SerializedName("item_number")
//        public String item_number;
//
//        @SerializedName("item_type")
//        public Integer item_type;
//
//        @SerializedName("name")
//        public String name;
//
//        @SerializedName("pic_filename")
//        public String pic_filename;
//
//        @SerializedName("description")
//        public String description;
//
//        @SerializedName("receiving_quantity")
//        public Float receiving_quantity;
//
//        @SerializedName("reorder_level")
//        public Float reorder_level;
//
//        @SerializedName("stock_type")
//        public Integer stock_type;
//
//        @SerializedName("store_id")
//        public String store_id;
//
//        @SerializedName("supplier_id")
//        public Integer supplier_id;
//
//        @SerializedName("tax_category_id")
//        public String tax_category_id;
//
//        @SerializedName("unit_price")
//        public Float unit_price;
//
//        @SerializedName("updated_by")
//        public String updated_by;
//
//        @SerializedName("updated_date")
//        public Date updated_date;
//
//        @SerializedName("version_no")
//        public Integer version_no;

        @SerializedName("category")
        public String category;

        @SerializedName("client_id")
        public String client_id;

        @SerializedName("cost_price")
        public String cost_price;

        @SerializedName("created_by")
        public String created_by;

        @SerializedName("created_date")
        public String created_date;

        @SerializedName("item_id")
        public String item_id;

        @SerializedName("item_number")
        public String item_number;

        @SerializedName("item_type")
        public String item_type;

        @SerializedName("name")
        public String name;

        @SerializedName("pic_filename")
        public String pic_filename;

        @SerializedName("description")
        public String description;

        @SerializedName("receiving_quantity")
        public String receiving_quantity;

        @SerializedName("reorder_level")
        public String reorder_level;

        @SerializedName("stock_type")
        public String stock_type;

        @SerializedName("store_id")
        public String store_id;

        @SerializedName("supplier_id")
        public String supplier_id;

        @SerializedName("tax_category_id")
        public String tax_category_id;

        @SerializedName("unit_price")
        public String unit_price;

        @SerializedName("updated_by")
        public String updated_by;

        @SerializedName("updated_date")
        public String updated_date;

        @SerializedName("version_no")
        public String version_no;

    }

}
