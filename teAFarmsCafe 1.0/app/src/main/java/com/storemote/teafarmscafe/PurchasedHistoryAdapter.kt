package com.storemote.teafarmscafe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.PurchasedHistoryDB
import com.storemote.teafarmscafe.Model_RealmDatabase.WeeklySpecials
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class PurchasedHistoryAdapter (private val context: Context,
                                 private val dataSource: ArrayList<PurchasedHistoryDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.purchased_history_adapter, parent, false)

        // Get title element
        val dateTextView = rowView.findViewById(R.id.txt_purchased_date) as TextView

// Get subtitle element
        val totalTextView = rowView.findViewById(R.id.txt_total_price_purchased) as TextView


        val purchasedHistory = getItem(position) as PurchasedHistoryDB

        dateTextView.text = purchasedHistory.saleDate
        totalTextView.text ="₱" + purchasedHistory.amountDue

        return rowView
    }
}