package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StoreListData {

    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {

        @SerializedName("client_id")
        public String client_id;
        @SerializedName("name")
        public String name;
        @SerializedName("store_id")
        public String store_id;
        @SerializedName("store_image")
        public String store_image;
        @SerializedName("store_address")
        public String store_address;

    }
}
