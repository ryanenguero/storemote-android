package com.storemote.teafarmscafe.Model_JSON;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LandingPage {


    @SerializedName("data")
    public List<Datum> data = new ArrayList<>();

    public class Datum {

        @SerializedName("client_id")
        public String client_id;
        @SerializedName("menu")
        public String menu;
        @SerializedName("promo")
        public String promo;
        @SerializedName("rewards")
        public String rewards;
        @SerializedName("store_id")
        public String store_id;
        @SerializedName("weekly_special")
        public String weekly_special;


    }
}
