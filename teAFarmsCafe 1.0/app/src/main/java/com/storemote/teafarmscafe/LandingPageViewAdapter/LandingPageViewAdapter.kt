package com.storemote.teafarmscafe.LandingPageViewAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.storemote.teafarmscafe.Model_RealmDatabase.LandingPageDB
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.R
import kotlinx.android.synthetic.main.list_store.view.*

class LandingPageViewAdapter (private val context: Context,
                              private val dataSource: ArrayList<LandingPageDB>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.landing_page_adapter, parent, false)

        // Get title element
        val weeklyTextView = rowView.findViewById(R.id.landing_page_weekly_caption) as TextView
        val menuTextView = rowView.findViewById(R.id.landing_page_menu_caption) as TextView
        val promoTextView = rowView.findViewById(R.id.landing_page_promo_caption) as TextView
        val rewardsTextView = rowView.findViewById(R.id.landing_page_rewards_caption) as TextView

        val weeklyImageView = rowView.findViewById(R.id.landing_page_weeklyspecials_image) as ImageView
        val menuImageView = rowView.findViewById(R.id.landing_page_menu_image) as ImageView
        val promoImageView = rowView.findViewById(R.id.landing_page_promo_image) as ImageView
        val rewardsImageView = rowView.findViewById(R.id.landing_page_rewards_image) as ImageView

        val landingPage = getItem(position) as LandingPageDB

        weeklyTextView.text = "Weekly Specials"
        menuTextView.text = "See Our Menu"
        promoTextView.text = "Promo"
        rewardsTextView.text = "Rewards"

//        Picasso.with(context).load("http://staging.storemote.net/Rest/${stores.storeImage}").placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${landingPage.weeklySpecial}").into(weeklyImageView)
        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${landingPage.menu}").into(menuImageView)
        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${landingPage.promo}").into(promoImageView)
        Picasso.with(this.context).load("http://storemote.net/assets/uploads/item_pics/${landingPage.rewards}").into(rewardsImageView)

        return rowView
    }
}