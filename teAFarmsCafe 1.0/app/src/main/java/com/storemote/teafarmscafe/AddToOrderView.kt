package com.storemote.teafarmscafe

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.storemote.teafarmscafe.Model_RealmDatabase.OrderListDB
import com.storemote.teafarmscafe.Model_RealmDatabase.StoreListDB
import io.realm.Realm
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class AddToOrderView : AppCompatActivity() {

    private lateinit var realm: Realm
    var strAmount : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.add_to_order_view)

        supportActionBar!!.title = "Add to Order"

        val txtProdName = findViewById(R.id.txtProdName) as TextView
        val txtProdQty = findViewById(R.id.txtProdQty) as TextView
        val txtSpecialInstruction = findViewById(R.id.textView3) as TextView
        val txtTotal = findViewById(R.id.textView4) as TextView

        txtProdName.text = intent.getStringExtra("name")

      txtProdQty.addTextChangedListener(object : TextWatcher{
          override fun afterTextChanged(s: Editable?) {


          }

          override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {


          }

          override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

              if (txtProdQty.text.toString().isNotEmpty() || txtProdQty.text.toString().isNotBlank()) {

                  val unitPrice = intent.getStringExtra("price").toFloat()
                  val qty = txtProdQty.text

                  val qtyStr = qty.toString()

                  val amount = (qtyStr.toFloat() * unitPrice).toBigDecimal()

                  txtTotal.text = "Amount: ₱" + amount.toString()

                  strAmount = "$amount"

              }else{
                  txtTotal.text = "Amount: ₱0.0"
              }
          }
      })

//        txtProdQty.setOnClickListener {
//            Log.d("MainActivity", "amount = " + txtProdQty.text as String)
//

//        }


        val submitBtn = findViewById(R.id.submitAddBtn) as Button
        val cancelBtn = findViewById(R.id.cancelAddBtn) as Button

        submitBtn.setOnClickListener {

            if (txtProdQty.text.toString().isNotEmpty() || txtProdQty.text.toString().isNotBlank()) {

            Log.d("MainActivity", "Submit Order");

            Realm.init(this)
            // Example of a call to a native method

//            Realm.deleteRealm(Realm.getDefaultConfiguration()!!)


            // Create the Realm instance
            realm = Realm.getDefaultInstance()

            // Asynchronous queries are evaluated on a background thread,
            // and passed to the registered change listener when it's done.
            // The change listener is also called on any future writes that change the result set.
//            storelist = realm.where(StoreListDB::class.java).findAllAsync()

            realm.executeTransaction { r ->
                // Add a person.
                // RealmObjects with primary keys created with `createObject()` must specify the primary key value as an argument.

                val orderListDat = r.createObject(OrderListDB::class.java)
                val prod = txtProdQty.text
                val specInst = txtSpecialInstruction.text
                val up = intent.getStringExtra("price").toFloat()

                orderListDat.name = intent.getStringExtra("name")
                orderListDat.itemId = intent.getStringExtra("itemId")
                orderListDat.quantity = prod.toString()
                orderListDat.specialInstruction = specInst.toString()
                if (intent.getStringExtra("is_rewards") == "0"){
                    orderListDat.payment = strAmount.toString()
                }else{
                    orderListDat.totalPointSpent = strAmount.toString()
                }




                orderListDat.clientId = intent.getStringExtra("clientId")
                orderListDat.storeId = intent.getStringExtra("storeId")
                orderListDat.isPromo = intent.getStringExtra("is_promo")
                orderListDat.isReward = intent.getStringExtra("is_rewards")
                orderListDat.unitPrice = up


                finish()

            }

            }else{

                val builder = AlertDialog.Builder(this)

                builder.setTitle("Failed")
                builder.setMessage("Quantity is empty")
                builder.setPositiveButton("Ok") { dialog, which ->

                }

                val dialog: AlertDialog = builder.create()
                dialog.show()


          }




        }


        cancelBtn.setOnClickListener {

            Log.d("MainActivity", "Cancel Order")

            finish()
        }

    }
}