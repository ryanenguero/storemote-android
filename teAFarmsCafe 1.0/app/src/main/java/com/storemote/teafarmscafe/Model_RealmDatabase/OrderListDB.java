package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmObject;

public class OrderListDB extends RealmObject {

    private String client_id;
    private String is_promo;
    private String is_reward;
    private String item_id;
    private String name;
    private String payment;
    private Integer promo_id;
    private String quantity;
    private Integer reward_id;
    private String special_instruction;
    private String store_id;
    private String total_point_spent;
    private Float unit_price;


    public void setClientId(String clientId) {

        this.client_id = clientId;
    }

    public String getClientId() {

        return client_id;
    }


    public void setIsPromo(String isPromo) {

        this.is_promo = isPromo;
    }

    public String getIsPromo() {

        return is_promo;
    }

    public void setIsReward(String isReward) {

        this.is_reward = isReward;
    }

    public String getIsReward() {

        return is_reward;
    }

    public void setItemId(String itemId) {

        this.item_id = itemId;
    }

    public String getItemId() {

        return item_id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setPayment(String payment) {

        this.payment = payment;
    }

    public String getPayment() {

        return payment;
    }

    public void setPromoid(Integer promoid) {

        this.promo_id = promoid;
    }

    public Integer getPromoId() {

        return promo_id;
    }

    public void setQuantity(String quantity) {

        this.quantity = quantity;
    }

    public String getQuantity() {

        return quantity;
    }

    public void setRewardId(Integer rewardId) {

        this.reward_id = rewardId;
    }

    public Integer getRewardId() {

        return reward_id;
    }

    public void setSpecialInstruction(String specialInstruction) {

        this.special_instruction = specialInstruction;
    }

    public String getSpecialInstruction() {

        return special_instruction;
    }

    public void setStoreId(String storeId) {

        this.store_id = storeId;
    }

    public String getStoreId() {

        return store_id;
    }

    public void setTotalPointSpent(String totalPointSpent) {

        this.total_point_spent = totalPointSpent;
    }

    public String getTotalPointSpent() {

        return total_point_spent;
    }

    public void setUnitPrice(Float unitPrice) {

        this.unit_price = unitPrice;
    }

    public Float getUnitPrice() {

        return unit_price;
    }

}
