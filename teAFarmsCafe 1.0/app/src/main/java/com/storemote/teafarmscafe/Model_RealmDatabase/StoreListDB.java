package com.storemote.teafarmscafe.Model_RealmDatabase;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
public class StoreListDB extends RealmObject {

//    @PrimaryKey
//    private int primaryKey;

//    @Index
    private String clientId;
    private String name;
    private String storeAddress;
    private String storeId;
    private String storeImage;

    public String getClientId() {

        return clientId;
    }

    public String getName() {

        return name;
    }

    public String getStoreAddress() {

        return storeAddress;
    }

    public String getStoreId() {

        return storeId;
    }

    public String getStoreImage() {

        return storeImage;
    }

    public void setClientId(String clientId) {

        this.clientId = clientId;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setStoreAddress(String storeAddress) {

        this.storeAddress = storeAddress;
    }

    public void setStoreId(String storeId) {

        this.storeId = storeId;
    }

    public void setStoreImage(String storeImage) {

        this.storeImage = storeImage;
    }
}
