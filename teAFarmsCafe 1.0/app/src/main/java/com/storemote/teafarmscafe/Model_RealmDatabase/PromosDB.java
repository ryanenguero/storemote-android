package com.storemote.teafarmscafe.Model_RealmDatabase;

import java.util.Date;

import io.realm.RealmObject;

public class PromosDB extends RealmObject {

    private Float discount_percent;
    private Date from_date;
    private Double item_free_id;
    private Double item_id;
    private String item_name;
    private Double no_of_free_items;
    private Double promo_id;
    private Double promo_type_id;
    private Float specific_price;
    private Date to_date;

    public void setDiscountPercent(Float discount_percent) {

        this.discount_percent = discount_percent;
    }

    public Float getDiscountPercent() {

        return discount_percent;
    }

    public void setFromDate(Date from_date) {

        this.from_date = from_date;
    }

    public Date getFromDate() {

        return from_date;
    }

    public void setItemFreeId(Double item_free_id) {

        this.item_free_id = item_free_id;
    }

    public Double getItemFreeId() {

        return item_free_id;
    }

    public void setItemId(Double item_id) {

        this.item_id = item_id;
    }

    public Double getItemId() {

        return item_id;
    }

    public void setItemName(String item_name) {

        this.item_name = item_name;
    }

    public String getItemName() {

        return item_name;
    }

    public void setNooffreeitems(Double no_of_free_items) {

        this.no_of_free_items = no_of_free_items;
    }

    public Double getNooffreeitems() {

        return no_of_free_items;
    }

    public void setPromoId(Double promo_id) {

        this.promo_id = promo_id;
    }

    public Double getPromoId() {

        return promo_id;
    }

    public void setPromoTypeId(Double promo_type_id) {

        this.promo_type_id = promo_type_id;
    }

    public Double getPromoTypeId() {

        return promo_type_id;
    }

    public void setSpecificPrice(Float specific_price) {

        this.specific_price = specific_price;
    }

    public Float getSpecificPrice() {

        return specific_price;
    }

    public void setToDate(Date to_date) {

        this.to_date = to_date;
    }

    public Date getToDate() {

        return to_date;
    }

}
